package astra.lang;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import astra.ast.core.ADTTokenizer;
import astra.ast.core.ASTRAParser;
import astra.ast.core.IEvent;
import astra.ast.core.IFormula;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.element.RuleElement;
import astra.ast.visitor.VariableTypeStack;
import astra.core.ActionParam;
import astra.core.Module;
import astra.core.Rule;
import astra.event.Event;
import astra.formula.Formula;
import astra.util.CodeVisitor;
import astra.util.VariableVisitor;

/**
 * Basic Compiler API.
 * 
 * <p>
 * This class is highly experimental and is designed to support the creation of rules from strings.
 * </p>
 * 
 * @author Rem Collier
 *
 */
public class Compiler extends Module {
	/**
	 * Internal method indicating that methods in this API do not need to be threaded.
	 */
	public boolean inline() {
		return true;
	}

	@ACTION
	public boolean compileRule(String source, ActionParam<Rule> rule) {
		try {
			ADTTokenizer tokenizer = new ADTTokenizer(new ByteArrayInputStream(source.getBytes()));
			ASTRAParser parser = new ASTRAParser(tokenizer);
			List<Token> list = new ArrayList<Token>();
			Token token = tokenizer.nextToken();
			while (token != Token.EOF_TOKEN) {
				list.add(token);
				token = tokenizer.nextToken();
			}
			RuleElement ruleElement = parser.createRule(list);
			ruleElement.accept(new VariableVisitor(), new VariableTypeStack());
			Rule myRule = (Rule) ruleElement.accept(new CodeVisitor(agent), null);
			rule.set(myRule);
		} catch (ParseException e) {
			java.lang.System.out.println("----------------------------------------------------------------------------------------------");
			java.lang.System.out.println("[" + agent.name() + "] SOURCE:");
			java.lang.System.out.println(source);
			java.lang.System.out.println("----------------------------------------------------------------------------------------------");
			e.printStackTrace();
			java.lang.System.out.println("----------------------------------------------------------------------------------------------");
		}
		return true;
	}

	@TERM
	public Rule compileRule(String source) {
		try {
			ADTTokenizer tokenizer = new ADTTokenizer(new ByteArrayInputStream(source.getBytes()));
			ASTRAParser parser = new ASTRAParser(tokenizer);
			List<Token> list = new ArrayList<Token>();
			Token token = tokenizer.nextToken();
			while (token != Token.EOF_TOKEN) {
				list.add(token);
				token = tokenizer.nextToken();
			}
			RuleElement ruleElement = parser.createRule(list);
			ruleElement.accept(new VariableVisitor(), new VariableTypeStack());
			Rule myRule = (Rule) ruleElement.accept(new CodeVisitor(agent), null);
			return myRule;
		} catch (ParseException e) {
			java.lang.System.out.println("----------------------------------------------------------------------------------------------");
			java.lang.System.out.println("[" + agent.name() + "] SOURCE:");
			java.lang.System.out.println(source);
			java.lang.System.out.println("----------------------------------------------------------------------------------------------");
			e.printStackTrace();
			java.lang.System.out.println("----------------------------------------------------------------------------------------------");
		}
		return null;
	}

   @TERM
   public String decompileRule(Rule rule) {
	   try {
		   return rule.toString() + " { " + rule.statement.toString() + "; }";
	   } catch (Exception e) {
		   java.lang.System.out.println("----------------------------------------------------------------------------------------------");
		   java.lang.System.out.println("Exception converting rule to string");
		   e.printStackTrace();
		   java.lang.System.out.println("----------------------------------------------------------------------------------------------");
	   }
	   return "";
   }

	@TERM
	public Event compileEvent(String source) {
		Event event = null;
		try {
			ADTTokenizer tokenizer = new ADTTokenizer(new ByteArrayInputStream(source.getBytes()));
			ASTRAParser parser = new ASTRAParser(tokenizer);
			List<Token> list = new ArrayList<Token>();
			Token token = tokenizer.nextToken();
			while (token != Token.EOF_TOKEN) {
				list.add(token);
				token = tokenizer.nextToken();
			}
			IEvent iEvent = parser.createEvent(list);
			
			event = (Event) iEvent.accept(new CodeVisitor(agent), null);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return event;
	}

	@TERM
	public Formula compileFormula(String source) {
		Formula formula = null;
		try {
			ADTTokenizer tokenizer = new ADTTokenizer(new ByteArrayInputStream(source.getBytes()));
			ASTRAParser parser = new ASTRAParser(tokenizer);
			List<Token> list = new ArrayList<Token>();
			Token token = tokenizer.nextToken();
			while (token != Token.EOF_TOKEN) {
				list.add(token);
				token = tokenizer.nextToken();
			}
			IFormula iFormula = parser.createFormula(list);
			
			formula = (Formula) iFormula.accept(new CodeVisitor(agent), null);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formula;
	}
}
