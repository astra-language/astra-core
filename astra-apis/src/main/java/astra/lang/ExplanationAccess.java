package astra.lang;

import astra.core.Module;
import astra.core.Rule;

import astra.explanation.Explanation;
import astra.explanation.store.ExplanationUnit;
import astra.formula.Formula;
import astra.formula.Predicate;

public class ExplanationAccess extends Module {


    /*
     * Operations to query the agent's explanation store
     */
    @TERM
	public Explanation retrieveAllExplanations(Rule r) {
		return agent.explanations().retrieveExplanation(r);
	}

    @TERM
	public Explanation retrieveAllExplanations(String tag) {
		return agent.explanations().retrieveExplanation(tag);
	}

	@TERM
	public Explanation latestExplanationUnitFor(Rule r) {
        return agent.explanations().retrieveLastExplanationFor(r);
    }

	@TERM
	public Explanation latestExplanationUnitFor(String tag){
        return agent.explanations().retrieveLastExplanationFor(tag);
    }

	@TERM
	public Explanation latestExplanationUnitFor(Rule r, String tag) {
        return agent.explanations().retrieveLastExplanationFor(r, tag);
    }

	@TERM
	public Explanation latestExplanationUnitFor(String tag, String value) {
        return agent.explanations().retrieveLastExplanationFor(tag, value);
    }

    @TERM
	public Explanation latestExplanationFor(String tag, String value) {
		return agent.explanations().retrieveLastFullExplanationFor(tag, value);
	}

    @TERM
	public Explanation latestExplanationFor(Rule r) {
		return agent.explanations().retrieveLastFullExplanationFor(r);
	}

    @TERM
    public Explanation fullMatchingExplanation(String tag, Explanation e) {
        return agent.explanations().retrieveExplanationFor(tag, getID(e)); //0 check?!
    }

    @TERM
    public int sizeOfExplanations() {
        return agent.explanations().size();
    }

    /*
     * Helper operation when generating operations.
     */
    @TERM
    public int newID() {
        return agent.explanations().newID();
    }

    /*
     * Operations to query an explanation object
     */
    @TERM
    public int getID(Explanation e) {
        if(e.getUnits().size() > 0){
            return e.getUnits().get(0).getID();
        }
        return -1;
    }

    @TERM
    public Rule getRule(Explanation e) {
        for (ExplanationUnit eu : e.getUnits()) {
            if (eu.getRule() != null) {
                return eu.getRule();
            }
        }
        return null;
    }

    @FORMULA
    public Formula hasRule(Explanation e) {
        for (ExplanationUnit eu : e.getUnits()) {
            if (eu.getRule() != null) {
                return Predicate.TRUE;
            }
        }
        return Predicate.FALSE;
    }

    @FORMULA
    public Formula isEmpty(Explanation e) {
        return e.getUnits().isEmpty() ? Predicate.TRUE : Predicate.FALSE;
    }

    @TERM 
    public String getDetail(Explanation e, String tag) {
        for (ExplanationUnit eu : e.getUnits()) {
            if (eu.getTag().equals(tag)) {
                if (eu.getDetail() != null) return eu.getDetail().value().toString();
            }
        }
        return "";
    }
}
