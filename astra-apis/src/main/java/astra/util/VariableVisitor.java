package astra.util;

import astra.ast.core.IStatement;
import astra.ast.core.ITerm;
import astra.ast.core.IType;
import astra.ast.core.ParseException;
import astra.ast.element.RuleElement;
import astra.ast.event.UpdateEvent;
import astra.ast.formula.GoalFormula;
import astra.ast.formula.NOTFormula;
import astra.ast.formula.PredicateFormula;
import astra.ast.statement.BlockStatement;
import astra.ast.statement.UpdateStatement;
import astra.ast.term.Function;
import astra.ast.term.InlineVariableDeclaration;
import astra.ast.term.ListTerm;
import astra.ast.term.VariableElement;
import astra.ast.visitor.AbstractVisitor;
import astra.ast.visitor.VariableTypeStack;

public class VariableVisitor extends AbstractVisitor {
	public Object visit(RuleElement element, Object data) throws ParseException {
		element.event().accept(this, data);
		element.context().accept(this, data);
		element.statement().accept(this, data);
        return null;
    }

	@Override
	public Object visit(UpdateEvent event, Object data) throws ParseException {
		event.content().accept(this, data);
		return null;
	}

   	@Override
	public Object visit(GoalFormula formula, Object data) throws ParseException {
		formula.predicate().accept(this, data);
		return null;
	}

	@Override
	public Object visit(PredicateFormula formula, Object data) throws ParseException {
		for (ITerm term : formula.terms()) {
			term.accept(this, data);
		}
		return null;
	}

	@Override
	public Object visit(Function function, Object data) throws ParseException {
		for (ITerm term : function.terms()) {
			term.accept(this, data);
		}
		return null;
	}

	@Override
	public Object visit(NOTFormula formula, Object data) throws ParseException {
		((VariableTypeStack) data).addScope();
		formula.formula().accept(this, data);
		((VariableTypeStack) data).removeScope();		
		return null;
	}
	
	@Override
	public Object visit(ListTerm term, Object data) throws ParseException {
		for (ITerm t : term.terms()) {
			t.accept(this, data);
		}
		return null;
	}

	@Override
	public Object visit(InlineVariableDeclaration term, Object data) throws ParseException {
		if (((VariableTypeStack) data).exists(term.identifier())) {
			throw new ParseException("Duplicate variable declaration: " + term.identifier(), term);
		}

		((VariableTypeStack) data).addVariable(term.identifier(), term.type());
		return null;
	}

	@Override
	public Object visit(VariableElement term, Object data) throws ParseException {
        java.lang.System.out.println("In visit(VariableElement)");
		IType type = ((VariableTypeStack) data).getType(term.identifier());

		if (type == null) {
			throw new ParseException("Undeclared variable: " + term.identifier(), term);
		}

		term.setType(type);
		return null;
	}

	@Override
	public Object visit(BlockStatement statement, Object data) throws ParseException {
		((VariableTypeStack) data).addScope();
		for (IStatement s: statement.statements()) {
			s.accept(this, data);
		}
		((VariableTypeStack) data).removeScope();
		return null;
	}

	@Override
	public Object visit(UpdateStatement statement, Object data)
			throws ParseException {
		statement.formula().accept(this, data);
		return null;
	}
}
