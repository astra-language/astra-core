package astra.util;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import astra.ast.core.IElement;
import astra.ast.core.ITerm;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.element.RuleElement;
import astra.ast.event.UpdateEvent;
import astra.ast.formula.GoalFormula;
import astra.ast.formula.PredicateFormula;
import astra.ast.statement.BlockStatement;
import astra.ast.statement.ModuleCallStatement;
import astra.ast.statement.UpdateStatement;
import astra.ast.term.InlineVariableDeclaration;
import astra.ast.term.Literal;
import astra.ast.term.VariableElement;
import astra.ast.visitor.AbstractVisitor;
import astra.core.Agent;
import astra.core.Intention;
import astra.core.Module;
import astra.core.Rule;
import astra.event.BeliefEvent;
import astra.event.Event;
import astra.event.GoalEvent;
import astra.formula.Formula;
import astra.formula.Goal;
import astra.formula.Predicate;
import astra.statement.BeliefUpdate;
import astra.statement.Block;
import astra.statement.DefaultModuleCallAdaptor;
import astra.statement.ModuleCall;
import astra.statement.Statement;
import astra.term.Primitive;
import astra.term.Term;
import astra.term.Variable;
import astra.type.Type;

public class CodeVisitor extends AbstractVisitor {
	private Agent agent;

	public CodeVisitor(Agent agent) {
		this.agent = agent;
	}

	private static Map<Integer, Type> types = new HashMap<>();
	static {
		types.put(Token.INTEGER, Type.INTEGER);
		types.put(Token.LONG, Type.LONG);
		types.put(Token.FLOAT, Type.FLOAT);
		types.put(Token.DOUBLE, Type.DOUBLE);
		types.put(Token.BOOLEAN, Type.BOOLEAN);
		types.put(Token.CHARACTER, Type.CHAR);
		types.put(Token.STRING, Type.STRING);
		types.put(Token.LIST, Type.LIST);
		types.put(Token.FORMULA, Type.FORMULA);
		types.put(Token.SPEECHACT, Type.PERFORMATIVE);
		types.put(Token.FUNCT, Type.FUNCTION);
	}

	public Object visit(RuleElement element, Object data) throws ParseException {
		Event event = (Event) element.event().accept(this, data);
		Statement statement = (Statement) element.statement().accept(this, data);
        Formula context = (Formula) element.context().accept(this, data);
        if (context != null) {
            return new Rule(event, context, statement);
        }
		return new Rule(event, statement);
	}

	public Object visit(UpdateEvent event, Object data) throws ParseException {
		UpdateEvent update = (UpdateEvent) event;
		char type = update.type().charAt(0);
		Formula formula = (Formula) update.content().accept(this, data);
		if (formula instanceof Predicate) {
			return new BeliefEvent(type, (Predicate) formula);
		} else if (formula instanceof Goal) {
			return new GoalEvent(type, (Goal) formula);
		} else {
			java.lang.System.out.println("formula: " + update.content());
			java.lang.System.out.println("formula: " + update.content().getClass());
		}
		return null;
	}

	public Object visit(GoalFormula formula, Object data) throws ParseException {
		return new Goal((Predicate) formula.predicate().accept(this, data));
	}

	public Object visit(PredicateFormula formula, Object data) throws ParseException {
		Term[] terms = new Term[formula.termCount()];
		int i=0;
		for (ITerm term : formula.terms()) {
			terms[i++] = (Term) term.accept(this, data);
		}
		return new Predicate(formula.predicate(), terms);
	}

	public Object visit(Literal term, Object data) throws ParseException {
		switch (term.type().type()) {
			case Token.STRING:
				String value = term.value();
				return Primitive.newPrimitive(value.substring(1, value.length()-1));
			case Token.CHARACTER:
				return Primitive.newPrimitive(term.value().charAt(1));
			case Token.INTEGER:
				return Primitive.newPrimitive(Integer.parseInt(term.value()));
			case Token.LONG:
				return Primitive.newPrimitive(Long.parseLong(term.value()));
			case Token.FLOAT:
				return Primitive.newPrimitive(Float.parseFloat(term.value()));
			case Token.DOUBLE:
				return Primitive.newPrimitive(Double.parseDouble(term.value()));
			case Token.BOOLEAN:
				return Primitive.newPrimitive(Boolean.parseBoolean(term.value()));

		}
		throw new ParseException("Could not convert literal: " + term, term);
	}

	public Object visit(VariableElement variable, Object data) throws ParseException {
		return new Variable(types.get(variable.type().type()), variable.identifier());
	}

	public Object visit(InlineVariableDeclaration variable, Object data) throws ParseException {
		return new Variable(types.get(variable.type().type()), variable.identifier());
	}

	public Object visit(BlockStatement statement, Object data) throws ParseException {
		Statement[] statements = new Statement[statement.statements().length];
        int i=0;
        for (IElement element : statement.statements()) {
			statements[i++] = (Statement) element.accept(this, data);
        }
        return new Block(statements);
    }

	public Object visit(UpdateStatement statement, Object data) throws ParseException {
		return new BeliefUpdate(statement.op().charAt(0), (Predicate) statement.formula().accept(this, data));
	}

	public Object visit(ModuleCallStatement statement, Object data) throws ParseException {
		// java.lang.System.out.println("HERE: " + statement);
		final Module module = agent.getModule(agent.getASTRAClass().getCanonicalName(), statement.module());
		if (module == null) {
			throw new ParseException("Could not find module: " + module, statement);
		}
		
		Predicate predicate = (Predicate) statement.method().accept(this,data);
		Method selectedMethod = null;
		for (Method method : module.getClass().getMethods()) {
			if (method.getName().equals(predicate.predicate())) {
				selectedMethod = method;
			}
		}
		java.lang.System.out.println("method: " + selectedMethod);
		return new ModuleCall(
			statement.module(),
			predicate,
			new DefaultModuleCallAdaptor() {
				@Override
				public boolean inline() {
					return true;
				}

				@Override
				public boolean invoke(Intention context, Predicate atom) {
					throw new UnsupportedOperationException("Unimplemented method 'invoke'");
				}
			}
		);
	}
}