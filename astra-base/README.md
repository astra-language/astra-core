#astra-base

This is a parent POM that can subclassed to to create ASTRA projects. 


To use this POM, create a child POM with the following code:

	<project xmlns="http://maven.apache.org/POM/4.0.0"
		 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
		<modelVersion>4.0.0</modelVersion>
		<groupId>{group-id}</groupId>
		<artifactId>{project-id}</artifactId>
		<version>{version-num}</version>

		<repositories>
			<repository>
				<id>astra-repo</id>
				<url>https://gitlab.com/astra-language/astra-mvn-repo/raw/master</url>
			</repository>
		</repositories>
    
		<parent>
			<groupId>astra</groupId>
		        <artifactId>astra-base</artifactId>
		        <version>0.1.0</version>
	    </parent>
    
	    <build>
	        <plugins>
	            <plugin>
	                <groupId>astra</groupId>
	                <artifactId>astra-maven-plugin</artifactId>
	            </plugin>
	        </plugins>
	    </build>
	</project>
