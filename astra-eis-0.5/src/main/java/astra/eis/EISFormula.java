package astra.eis;

import astra.formula.Formula;
import astra.formula.Predicate;
import astra.reasoner.util.LogicVisitor;
import astra.term.Term;

public class EISFormula implements Formula {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9137787685136946200L;
	private Term entity;
	private Predicate predicate;
	
	public EISFormula(Predicate predicate) {
		this.predicate = predicate;
	}

	public EISFormula(Term entity, Predicate predicate) {
		this.entity = entity;
		this.predicate = predicate;
	}

	public void reIndex() {
		entity.reIndex();
		predicate.reIndex();
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Formula formula) {
		if (formula instanceof EISFormula) {
			EISFormula f = (EISFormula) formula;
			return f.entity.matches(entity) && f.predicate.matches(predicate); 
		}
		return false;
	}

	public Predicate predicate() {
		return predicate;
	}
	
	public String toString() {
		return "EIS" + (entity == null ? "":"< " + entity + " >") + "." + predicate;
	}

	public Term entity() {
		return entity;
	}
}
