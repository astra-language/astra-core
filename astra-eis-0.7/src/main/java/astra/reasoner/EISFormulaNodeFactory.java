package astra.reasoner;

import java.util.Map;

import astra.eis.EISFormula;
import astra.reasoner.node.ReasonerNode;
import astra.reasoner.node.ReasonerNodeFactory;
import astra.term.Term;

public class EISFormulaNodeFactory implements ReasonerNodeFactory<EISFormula> {

    @Override
    public ReasonerNode create(EISFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new EISFormulaNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, EISFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new EISFormulaNode(parent, formula, bindings, singleResult);
    }
    
}
