package astra.eis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import astra.term.Funct;
import astra.term.Primitive;
import astra.term.Term;
import eis.AgentListener;
import eis.EILoader;
import eis.EnvironmentInterfaceStandard;
import eis.EnvironmentListener;
import eis.PerceptUpdate;
import eis.exceptions.ActException;
import eis.exceptions.AgentException;
import eis.exceptions.EntityException;
import eis.exceptions.ManagementException;
import eis.exceptions.NoEnvironmentException;
import eis.exceptions.PerceiveException;
import eis.exceptions.RelationException;
import eis.iilang.Action;
import eis.iilang.EnvironmentState;
import eis.iilang.Parameter;
import eis.iilang.Percept;

public class EISService {
	private static Logger logger = Logger.getLogger(EISService.class.getCanonicalName());
	static {
		logger.setLevel(Level.OFF);
	}
	
	private EnvironmentInterfaceStandard ei = null;
	private Map<String, EISAgent> agents = new HashMap<String, EISAgent>();
	private String jarFile;
	
	private class EISEnvironmentListener implements EnvironmentListener {
		public void handleNewEntity(String entity) {
			broadcastEvent(new EISEvent(EISEvent.ENVIRONMENT, new Funct("newEntity", new Term[] { Primitive.newPrimitive(entity) })));
		}

		public void handleDeletedEntity(String entity, Collection<String> EISAgents) {
			broadcastEvent(new EISEvent(EISEvent.ENVIRONMENT, new Funct("deletedEntity", new Term[] { Primitive.newPrimitive(entity) })));
		}

		public void handleFreeEntity(String entity, Collection<String> EISAgents) {
			broadcastEvent(new EISEvent(EISEvent.ENVIRONMENT, new Funct("freedEntity", new Term[] { Primitive.newPrimitive(entity) })));
		}

		public void handleStateChange(EnvironmentState newState) {
			broadcastEvent(new EISEvent(EISEvent.ENVIRONMENT, new Funct("state", new Term[] { Primitive.newPrimitive(newState.toString()) })));
		}
	}
	
	public EISService(String jar) {
		setup(jar);
	}

	private void broadcastEvent(EISEvent event) {
		List<EISAgent> list = new ArrayList<EISAgent>();
		list.addAll(agents.values());
		for (EISAgent agent : list) {
			agent.addEvent(event);
		}
	}

	public boolean eisStart() {
		try {
			ei.start();
		} catch (ManagementException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean reset(Map<String, Parameter> parameters) {
		// TODO: Uncomment for 0.5 support
//		try {
//			ei.reset(parameters);
//		} catch (ManagementException e) {
//			e.printStackTrace();
//			return false;
//		}
		return true;
	}
	
	public boolean isStartSupported() {
		return ei.isStartSupported();
	}
	
	public String getEnvironmentState() {
		EnvironmentState state = ei.getState();
		if (state.equals(EnvironmentState.PAUSED)) {
			return "paused";
		} else if (state.equals(EnvironmentState.INITIALIZING)) {
			return "initializing";
		} else if (state.equals(EnvironmentState.RUNNING)) {
			return "running";
		} else if (state.equals(EnvironmentState.KILLED)) {
			return "killed";
		}
		return "unknown";
	}
	
    public boolean setup(String jarFile) {
		this.jarFile = jarFile;
		try {
			ei = EILoader.fromJarFile(new File(jarFile));
			EISEnvironmentListener listener = new EISEnvironmentListener();
			ei.attachEnvironmentListener(listener);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
    }

	public boolean init(Map<String, Parameter> params) {
		try {
				ei.init(params);
		} catch (ManagementException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public String getJarFile() {
		return jarFile;
	}
    
    public void registerAgent(final EISAgent agent) {
    	try {
			ei.registerAgent(agent.name());
			agents.put(agent.name(), agent);
			ei.attachAgentListener(agent.name(), new AgentListener() {
				public void handlePercept(String arg0, Percept arg1) {
					System.out.println("agent: " +arg0 + " / percept: " +arg1.toProlog());
					 
				}
				
			});
		} catch (AgentException e) {
			e.printStackTrace();
		}
    }

    public Map<String, PerceptUpdate> collectBeliefs(EISAgent agent) {
		try {
			if (ei.getState().equals(EnvironmentState.RUNNING)) {
				try {
					
					Collection<String> entities = ei.getAssociatedEntities(agent.name());
					if (!entities.isEmpty()) {
						String[] entitesStrArray = entities.toArray(new String[entities.size()]);
						Map<String, PerceptUpdate> updates = ei.getPercepts(agent.name(), entitesStrArray);
						for (PerceptUpdate pu: updates.values()) {
							if (!pu.getAddList().isEmpty()) {
								for (Percept p : pu.getAddList()) {
									if (p.getName().equals("holding")) {
										System.out.println("Collect beliefs");
										System.out.println(p.toProlog());
									}
								}
							}
						}
				    	return updates;
					}
				} catch (AgentException e) {
					logger.log(Level.WARNING, "[" + agent.name() + "] Agent Problem: " + e.getMessage(), e);
				}
			}
		} catch (PerceiveException e) {
			logger.log(Level.WARNING, "[" + agent.name() + "] Perception Problem: " + e.getMessage(), e);
		} catch (NoEnvironmentException e) {
			logger.log(Level.WARNING, "[" + agent.name() + "] Environment Problem: " + e.getMessage(), e);
		}
		return new HashMap<String, PerceptUpdate>();
    }

    public void unregisterAgent(EISAgent EISAgent) {
    	try {
			ei.unregisterAgent(EISAgent.name());
		} catch (AgentException e) {
			e.printStackTrace();
		}
    }
    
    public EISAgent get(String name) {
    	return agents.get(name);
    }
    
    public Collection<String> getAssociatedEntities(String name) throws AgentException {
    	return ei.getAssociatedEntities(name);
    }

    public Collection<String> getAssociatedAgents(String name) throws EntityException {
    	return ei.getAssociatedAgents(name);
    }

    public Collection<String> getFreeEntities() {
    	return ei.getFreeEntities();
    }
        
    public Collection<String> getAllEntities() {
    	return ei.getEntities();
    }
        
    public boolean associateEntity(String name, String entity) {
    	try {
			ei.associateEntity(name, entity);
		} catch (RelationException e) {
			e.printStackTrace();
			return false;
		}
		return true;
    }

    public String queryEntityType(String entity) {
    	try {
			return ei.getType(entity);
		} catch (EntityException e) {
		}
		return null;
    }

    public void performAction(String agent, String entity, Action act) throws ActException, NoEnvironmentException {
        //KB This used to return percepts but no longer does in later versions of EIS
		ei.performAction(agent, act, entity);
    }

	public boolean hasAssociatedEntity(String name, String entity) {
		try {
			return ei.getAssociatedEntities(name).contains(entity);
		} catch (AgentException e) {
			e.printStackTrace();
			return false;
		}
	}
}
