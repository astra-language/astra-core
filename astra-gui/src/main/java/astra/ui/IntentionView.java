package astra.ui;

import java.awt.BorderLayout;
import java.awt.event.MouseListener;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import astra.core.Agent;
import astra.core.Intention;

public class IntentionView implements AgentView {
    private JList<Intention> intentions;
    private JTextArea details;

    public String title() {
        return "Intentions";
    }

    public JPanel setup(Agent agent) {
        JPanel panel = new JPanel();
        panel.setLayout(new java.awt.BorderLayout());
       
        JScrollPane scrollPane = new JScrollPane(details = new JTextArea());
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        details.setText("No intention Selected");
        panel.add(scrollPane, BorderLayout.CENTER);
        
        intentions = new JList<Intention>();
        intentions.setVisibleRowCount(4);
        intentions.addMouseListener(new MouseListener() {
            @Override
            @SuppressWarnings("unchecked")
            public void mouseClicked(java.awt.event.MouseEvent e) {
                Intention intention =(Intention) ((JList<Intention>)e.getSource()).getSelectedValue();
                if (intention == null) {
                    details.setText("No intention Selected");
                } else {
                    details.setText(intention.generateIntentionTree());
                }
            }

            @Override public void mousePressed(java.awt.event.MouseEvent e) {}
            @Override public void mouseReleased(java.awt.event.MouseEvent e) {}
            @Override public void mouseEntered(java.awt.event.MouseEvent e) {}
            @Override public void mouseExited(java.awt.event.MouseEvent e) {}

        });
        scrollPane = new JScrollPane(intentions);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(scrollPane, BorderLayout.NORTH);

        return panel;
    }

    public void update(Agent agent) {
        intentions.setModel(new ListModel<Intention>() {
            @Override
            public void addListDataListener(ListDataListener l) {}

            @Override
            public Intention getElementAt(int index) {
                return agent.intentions().get(index);
            }

            @Override
            public int getSize() {
                return agent.intentions().size();
            }

            @Override
            public void removeListDataListener(ListDataListener l) {}
        });
    }
    
}
