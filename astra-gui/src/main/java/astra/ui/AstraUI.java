package astra.ui;

import java.awt.BorderLayout;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import astra.core.Agent;
import astra.gui.AstraEventListener;
import astra.gui.AstraGui;

public class AstraUI extends JFrame implements AstraGui, Agent.AgentRegistryListener {
    /**
     *
     */
    private static final long serialVersionUID = 3620558537654297825L;
    // private static final Object[] EMPTY_ARRAY = new Object[0];
    private JDesktopPane desktop;
    private JPanel control;
    private JList<String> agentList;

    // private AstraEventListener listener;

    public AstraUI() {
        Agent.addRegistryListener(this);

        desktop = new JDesktopPane();
        control = new JPanel();
        control.setLayout(new java.awt.BorderLayout());
        control.add(new JLabel("Agents:"), BorderLayout.NORTH);

        
        agentList = new JList<>();
        agentList.addMouseListener(new MouseListener() {
            @Override
            @SuppressWarnings("unchecked")
            public void mouseClicked(java.awt.event.MouseEvent e) {
                if (e.getClickCount() == 2) {
                    AgentUI aui = new AgentUI(Agent.getAgent(
                        (String) ((JList<String>)e.getSource()).getSelectedValue()
                    ));
                    aui.setVisible(true);
                    desktop.add(aui);
                }
            }

            @Override public void mousePressed(java.awt.event.MouseEvent e) {}
            @Override public void mouseReleased(java.awt.event.MouseEvent e) {}
            @Override public void mouseEntered(java.awt.event.MouseEvent e) {}
            @Override public void mouseExited(java.awt.event.MouseEvent e) {}
        });
        JScrollPane scrollPane = new JScrollPane(agentList);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        control.add(BorderLayout.CENTER, scrollPane);

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setLeftComponent(control);
        splitPane.setRightComponent(desktop);
        setContentPane(splitPane);
        pack();
    }

    public void refreshAgentList() {
        agentList.setModel(new ListModel<String>() {
            String[] agentNames;
            {
                agentNames = Agent.agentNames().toArray(new String[Agent.agentNames().size()]);
            }

            @Override
            public void addListDataListener(ListDataListener l) {}

            @Override
            public String getElementAt(int index) {
                return agentNames[index];
            }

            @Override
            public int getSize() {
                return agentNames.length;
            }

            @Override
            public void removeListDataListener(ListDataListener l) {}
        });
    }

    @Override
    public boolean receive(String type, List<?> args) {
        System.out.println("type: " + type + " / args: " + args);

        return true;
    }

    @Override
    public void launch(AstraEventListener listener) {
        // this.listener = listener;

        refreshAgentList();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 600);
        setVisible(true);
    }

    @Override
    public void receive(Agent agent) {
        refreshAgentList();
    }
}