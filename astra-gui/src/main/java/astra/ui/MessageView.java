package astra.ui;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import astra.core.Agent;
import astra.core.AgentMessageListener;
import astra.messaging.AstraMessage;

public class MessageView implements AgentView,AgentMessageListener {
    private DefaultListModel<AstraMessage> model = new DefaultListModel<AstraMessage>();
    
    @Override
    public String title() {
        return "Messages";
    }

    @Override
    public JPanel setup(Agent agent) {
        JPanel panel = new JPanel(new java.awt.BorderLayout());
        JScrollPane scrollPane = new JScrollPane(new JList<AstraMessage>(model));
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        panel.add(scrollPane, java.awt.BorderLayout.CENTER);
        agent.addAgentMessageListener(this);
        return panel;
        }

    @Override
    public void update(Agent agent) {
    }

    @Override
    public void receive(AstraMessage message) {
        model.addElement(message);
    }    
}
