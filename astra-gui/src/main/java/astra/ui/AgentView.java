package astra.ui;

import javax.swing.JPanel;

import astra.core.Agent;

public interface AgentView {
    String title();
    JPanel setup(Agent agent);
    void update(Agent agent);
    
}
