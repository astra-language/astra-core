package astra.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import astra.core.Agent;
import astra.core.Scheduler;

public class AgentUI extends JInternalFrame implements Observer {
    public static List<Class<? extends AgentView>> viewTemplates = new LinkedList<>();
    static {
        viewTemplates.add(BeliefView.class);
        viewTemplates.add(IntentionView.class);
        viewTemplates.add(MessageView.class);
    }
    /**
     *
     */
    private static final long serialVersionUID = 7907644278114616870L;

    private List<AgentView> views = new LinkedList<>();
    private Agent agent;
    private JTextField status;
    private JTextField iteration;
    private JTabbedPane tabbedPane;
    
    public AgentUI(Agent agent) {
        this.agent = agent;
        setTitle("Agent: " + this.agent.name());
        setResizable(true);
        setMaximizable(true);
        setClosable(true);
        setLayout(new BorderLayout());
        
        tabbedPane = new JTabbedPane();
        for (Class<? extends AgentView> template : viewTemplates) {
            try {
                AgentView view = template.newInstance();
                tabbedPane.addTab(view.title(), null, view.setup(agent), "Does nothing");
                view.update(agent);
                views.add(view);
                } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            }
        }
        add(tabbedPane, BorderLayout.CENTER);

        // Infobar
        JPanel infoBar = new JPanel(new BorderLayout());
        
        JPanel statusBar = new JPanel(new java.awt.FlowLayout());
        statusBar.add(new JLabel("Iteration:"));
        iteration = new JTextField(""+agent.getIteration(), 7);
        statusBar.add(iteration);
        statusBar.add(new JLabel("Status:"));
        status = new JTextField((agent.isActive() ? "ACTIVE":"INACTIVE"), 7);
        status.setAlignmentY(RIGHT_ALIGNMENT);
        statusBar.add(status);

        JButton refresh = new JButton("REFRESH");
        refresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                update(null, null);
            }
        });
        infoBar.add(refresh);
        infoBar.add(statusBar, BorderLayout.EAST);

        JPanel controlBar = new JPanel(new java.awt.FlowLayout());
        JButton stop = new JButton("Stop");
        stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agent.setState(Agent.STEP);
                System.out.println("STOPPED");
            }
        });
        controlBar.add(stop);
        JButton play = new JButton("Play");
        play.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agent.setState(Agent.INACTIVE);
                agent.lazyActivation();
                Scheduler.schedule(agent);
                System.out.println("PLAYED");
            }
        });
        controlBar.add(play);
        JButton step = new JButton("Step");
        step.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (agent.isStep()) {
                    agent.execute();
                    update(null, null);
                }
                System.out.println("STEPPED");
            }
        });
        controlBar.add(step);
        infoBar.add(controlBar, BorderLayout.WEST);
        add(infoBar, BorderLayout.NORTH);
        pack();
        agent.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        status.setText(agent.isActive() ? "ACTIVE":"INACTIVE");
        iteration.setText(""+agent.getIteration());
        for (AgentView view : views) {
            view.update(agent);
        }
    }
    
}
