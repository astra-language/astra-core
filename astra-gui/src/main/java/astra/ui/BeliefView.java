package astra.ui;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import astra.core.Agent;
import astra.formula.Formula;

public class BeliefView implements AgentView {
    private JList<Formula> beliefs;

    public String title() {
        return "Beliefs";
    }

    public JPanel setup(Agent agent) {
        JPanel panel = new JPanel(new java.awt.BorderLayout());
        JScrollPane scrollPane = new JScrollPane(beliefs = new JList<Formula>());
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        panel.add(scrollPane, java.awt.BorderLayout.CENTER);
        return panel;
    }

    public void update(Agent agent) {
        beliefs.setModel(new ListModel<Formula>() {
            @Override
            public void addListDataListener(ListDataListener l) {}

            @Override
            public Formula getElementAt(int index) {
                return agent.beliefs().beliefs().get(index);
            }

            @Override
            public int getSize() {
                return agent.beliefs().beliefs().size();
            }

            @Override
            public void removeListDataListener(ListDataListener l) {}
        });
    }
    
}
