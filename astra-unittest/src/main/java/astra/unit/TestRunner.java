package astra.unit;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;

import astra.core.ASTRAClass;

public class TestRunner {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        List<String> tests = new LinkedList<String>();
        Reflections reflections = new Reflections();
        Set<Class<? extends ASTRAClass>> javaClasses = reflections.getSubTypesOf(ASTRAClass.class);
        for (Class<? extends ASTRAClass> javaClass : javaClasses) {
            ASTRAClass mainClass = javaClass.newInstance();
            for (Class<ASTRAClass> parent : mainClass.getParents()) {
                if (parent.getCanonicalName().equals("astra.unit.ASTRAUnitTest")) {
                    tests.add(javaClass.getCanonicalName());
                }
            }
        }
        TestSuite suite = new TestSuite(tests.toArray(new String[tests.size()]));
        suite.execute();
        TestSuite.displayResults(suite);
        System.out.println("COMPLETED");
    }
}