package astra.unit;
import java.util.Arrays;

import astra.core.Agent;
import astra.core.Module;
import astra.formula.Predicate;
import astra.term.Funct;

public class UnitTest extends Module {
	public static final String SUCCESS_MESSAGE = "Success";
	@ACTION
	public boolean assertEquals(TestSuite suite, int first, int second) {
		if (first == second) {
			suite.testResult(first + " == " + second, true);
			return true;
		} else {
			suite.testResult(first + " != " + second, false);
			return false;
		}
	}

	@ACTION
	public boolean assertEquals(TestSuite suite, long first, long second) {
		if (first == second) {
			suite.testResult(first + " == " + second, true);
			return true;
		} else {
			suite.testResult(first + " != " + second, false);
			return false;
		}
	}

	@ACTION
	public boolean assertEquals(TestSuite suite, String first, String second) {
		if (first.equals(second)) {
			suite.testResult(first + " == " + second, true);
			return true;
		} else {
			suite.testResult(first + " != " + second, false);
			return false;
		}
	}

	@ACTION
	public boolean assertArrayEquals (TestSuite suite, int[] first, int[] second) {

		if (Arrays.equals(first, second)) {
			suite.testResult("The two Arrays are equal", true);
			return true;
		} else {
			suite.testResult("The two Arrays are not equal", false);
			return false;
		}
	}

	@ACTION
	public boolean assertArrayEquals (TestSuite suite, char[] first, char[] second) {

		if (Arrays.equals(first, second)) {
			suite.testResult("The two Arrays are equal", true);
			return true;
		} else {
			suite.testResult("The two Arrays are not equal", false);
			return false;
		}
	}

	@ACTION
	public boolean assertTrue (TestSuite suite, String msg, boolean condition) {

		if (!condition) {
			fail (suite, msg);
		}

		return true;
	}

	@ACTION
	public boolean assertTrue (TestSuite suite, boolean condition) {

		return assertTrue(suite, null, condition);
	}

	@ACTION
	public boolean injectBelief(TestSuite suite, String name, Funct belief) {
		Agent.getAgent(name).beliefs().addBelief(new Predicate(belief.functor(), belief.terms()));
		return true;
	}

	@ACTION
	public boolean extractBelief(TestSuite suite, String name, Funct belief) {
		Agent.getAgent(name).beliefs().dropBelief(new Predicate(belief.functor(), belief.terms()));
		return true;
	}

	@ACTION
	public boolean assertBelief(TestSuite suite, String name, Funct belief) {
		if (Agent.getAgent(name).queryAll(new Predicate(belief.functor(), belief.terms())) != null) {
			suite.testResult("Success", true);
			return true;
		} else {
			suite.testResult("Belief: " + belief + " NOT present", false);
			return false;
		}
	}

	@ACTION
	public boolean fail(TestSuite suite, String msg) {

		if (msg == null) {
			suite.testResult(null, false);
		} else {
			suite.testResult(msg, false);
		}

		return true;
	}

	@ACTION
	public boolean fail(TestSuite suite) {

		return fail(suite, null);
	}

	@ACTION
	public boolean success(TestSuite suite) {
		suite.testResult(SUCCESS_MESSAGE, true);
		return true;
	}
}