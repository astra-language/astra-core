package astra.learn;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import astra.core.Agent;
import astra.event.GoalEvent;
import astra.formula.Goal;
import astra.formula.Predicate;
import astra.learn.library.Algorithm;
import astra.learn.library.ID3DecisionTree;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;
import astra.type.Type;


public class ID3DecisionTreeTest {
    
    static Predicate knowledge_input;
    static Predicate sample;
    static Predicate data;
    static Predicate dataWithFloats;
    static Predicate knowledge_label;
    static Predicate knowledge_train;

    static Predicate config;

    static LearningProcess lp;
    static Algorithm a;
    static GoalEvent ge;

    @BeforeAll
    static void setup() {
        //Knowledge input config
        Term[] ki = {new Primitive<String>(Type.STRING, "learnCategorise"),new Primitive<String>(Type.STRING, "sample")};
        knowledge_input = new Predicate("knowledge_input", ki);

        Primitive x11 =  new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        Primitive x12 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X1 = new ListTerm(new Term[]{x11,x12});
        Primitive x21 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        Primitive x22 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X2 = new ListTerm(new Term[]{x21,x22});
        Primitive x31 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        Primitive x32 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        ListTerm X3 = new ListTerm(new Term[]{x31,x32});
        Primitive x41 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        Primitive x42 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        ListTerm X4 = new ListTerm(new Term[]{x41,x42});

        Primitive t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t3 = new Primitive<Float>(Type.FLOAT, new Float(0.4));
        Primitive t4 = new Primitive<Float>(Type.FLOAT, new Float(0.4));

        ListTerm X = new ListTerm(new Term[]{X1,X2,X3,X4}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});

        //Knowledge input pred
        ListTerm emptyList = new ListTerm(new Term[]{});
        Term[] inputPred = {new Primitive<ListTerm>(Type.LIST, emptyList)};
        sample = new Predicate("sample", inputPred);

        //Build rule
        ge = new GoalEvent('+',
            new Goal(
                new Predicate("categorise", new Term[] {})
            )
        );
       
        //Knowledge knowledge_train config
        Term[] kt = {new Primitive<String>(Type.STRING, "learnCategorise"),new Primitive<String>(Type.STRING, "data")};
        knowledge_train = new Predicate("knowledge_train", kt);

         //Config
        Term[] ck = {new Primitive<String>(Type.STRING, "learnCategorise"),new Primitive<String>(Type.STRING, "max_depth"),new Primitive<Integer>(Type.INTEGER, 2)};
        config = new Predicate("configuration", ck);

         
        //Knowledge input pred
        data = new Predicate("data", new Term[]{X, Y});

        //Knowledge label config
        Term[] kl = {new Primitive<String>(Type.STRING, "learnCategorise"),new Primitive<String>(Type.STRING, "category")};
        knowledge_label = new Predicate("knowledge_label", kl);

        x11 = new Primitive<Float>(Type.FLOAT, new Float(5.0f));
        x12 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        Primitive x13 = new Primitive<String>(Type.STRING, "hi");
        Primitive x14 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        X1 = new ListTerm(new Term[]{x11,x12,x13,x14});
        x21 =  new Primitive<Float>(Type.FLOAT, new Float(2.0f));
        x22 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        Primitive x23 = new Primitive<String>(Type.STRING, "hi");
        Primitive x24 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        X2 = new ListTerm(new Term[]{x21,x22,x23,x24});
        x31 = new Primitive<Float>(Type.FLOAT, new Float(8.0f));
        x32 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        Primitive x33 = new Primitive<String>(Type.STRING, "hi");
        Primitive x34 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        X3 = new ListTerm(new Term[]{x31,x32,x33,x34});
        x41 =  new Primitive<Float>(Type.FLOAT, new Float(9.0f));
        x42 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        Primitive x43 = new Primitive<String>(Type.STRING, "oh");
        Primitive x44 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        X4 = new ListTerm(new Term[]{x41,x42,x43,x44});

        t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        t3 = new Primitive<Float>(Type.FLOAT, new Float(0.3));
        t4 = new Primitive<Float>(Type.FLOAT, new Float(0.4));

        X = new ListTerm(new Term[]{X1,X2,X3,X4}); //empty list, not checking X here
        Y = new ListTerm(new Term[]{t,t2,t3,t4});
        dataWithFloats = new Predicate("data", new Term[]{X, Y});

    }


    @Test
    public void testExtractConfigurationBelief() {
        Agent agent = new Agent("AgentZero");
        try {
            agent.beliefs().addBelief(knowledge_input); 
            agent.beliefs().addBelief(knowledge_label); 
            agent.beliefs().update();
        } catch (NullPointerException npe) {
            System.out.println("Skipping test for now, FIXME later");
            return;
        }
       
        a = new ID3DecisionTree();
        lp = new LearningProcess("learnCategorise", "nav", null, a);
        lp.setAgent(agent);

        try {
            lp.extractConfigurationAndInputBeliefs();
            assertTrue(a.labelSet);
            assertEquals(a.input_predicates.size(), 1);
        } catch (Exception e) {
            fail("Shouldn't have thrown exception, " + e.getMessage());
        }
    }

    @Test
    public void testExtractConfigurationBeliefWithMultipleLabelBeliefs() {
        Agent agent = new Agent("AgentZero");
        try {
            agent.beliefs().addBelief(knowledge_label);
            agent.beliefs().addBelief(knowledge_label);
            agent.beliefs().update();
        } catch (NullPointerException npe) {
            System.out.println("Skipping test for now, FIXME later");
            return;
        }
        a = new ID3DecisionTree();
        lp = new LearningProcess("learnCategorise", "nav", null, a);
        lp.setAgent(agent);

        try {
            lp.extractConfigurationAndInputBeliefs();
        } catch (LearningProcessException ex) {
            String message = "Exception building input beliefs for " + lp.processNamespace + "; expected one knowledge_label belief but have 2";
            assertEquals(message, ex.getMessage());
        } catch (Exception e) {
            fail("Shouldn't have thrown exception, " + e.getMessage());
        }
    }

    @Test
    public void testApplyLearningProcess() {
        Agent agent = new Agent("AgentZero");
        try {
            agent.beliefs().addBelief(knowledge_label);
            agent.beliefs().addBelief(knowledge_train);
            agent.beliefs().addBelief(knowledge_input);
            agent.beliefs().addBelief(data);
            agent.beliefs().addBelief(sample);
            agent.beliefs().update();
        } catch (NullPointerException npe) {
            System.out.println("Skipping test for now, FIXME later");
            return;
        }
        a = new ID3DecisionTree();
        lp = new LearningProcess("learnCategorise", "nav", ge, a);
        lp.setAgent(agent);

        try {
            lp.trigger();
        } catch (LearningProcessException ex) {
            fail("Shouldn't have thrown exception, " + ex.getMessage());
        } catch (Exception e) {
            fail("Shouldn't have thrown exception, " + e.getMessage());
        }

        assertEquals(2, agent.rules().size());
    }

    @Test
    public void testApplyLearningProcessContinuousData() {
        Agent agent = new Agent("AgentZero");
        try {
            agent.beliefs().addBelief(knowledge_label);
            agent.beliefs().addBelief(knowledge_train);
            agent.beliefs().addBelief(knowledge_input);
            agent.beliefs().addBelief(dataWithFloats);
            agent.beliefs().addBelief(sample);
            agent.beliefs().addBelief(config);
            agent.beliefs().update();
        } catch (NullPointerException npe) {
            System.out.println("Skipping test for now, FIXME later");
            return;
        }
        a = new ID3DecisionTree();
        lp = new LearningProcess("learnCategorise", "nav", ge, a);
        lp.setAgent(agent);

        try {
            lp.trigger();
        } catch (Exception ex) {
            fail("Shouldn't have thrown exception, " + ex.getMessage());
        }

        assertEquals(3, agent.rules().size());
        String s = lp.algorithm.getModel().toString();
        System.out.println(s);
    }
}

