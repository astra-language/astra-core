package astra.learn.RLModel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import astra.learn.library.RLModel.RLDataModel;
import astra.learn.library.RLModel.RLDataObject;
import astra.learn.library.RLModel.StateActionValue;
import astra.term.Primitive;
import astra.term.Term;
import astra.type.Type;

/*
 * For Q Learning, represents Q(s,a)
 */
public class StateActionValueTest {
    
    static StateActionValue sav;

    
    @BeforeAll
    static void setup() {
    }


    @Test
    public void testGetHighestValue() throws Exception {
        RLDataModel dm = new RLDataModel();
        Term[] beliefTerms11 = {new Primitive<Integer>(Type.INTEGER, 1),new Primitive<Integer>(Type.INTEGER, 1)};
        RLDataObject output = dm.parseBeliefTerms(beliefTerms11);

        Term[] beliefTerms12 = {new Primitive<Integer>(Type.INTEGER, 1),new Primitive<Integer>(Type.INTEGER, 2)};
        RLDataObject output2 = dm.parseBeliefTerms(beliefTerms12);

        Term[] beliefTerms21 = {new Primitive<Integer>(Type.INTEGER, 2),new Primitive<Integer>(Type.INTEGER, 1)};
        RLDataObject output3 = dm.parseBeliefTerms(beliefTerms21);

        Term[] beliefTerms22 = {new Primitive<Integer>(Type.INTEGER, 2),new Primitive<Integer>(Type.INTEGER, 2)};
        RLDataObject output4 = dm.parseBeliefTerms(beliefTerms22);
       
        sav = new StateActionValue();
        sav.put(output, 0.0);
        sav.put(output2, 0.0);
        sav.put(output3, 0.0);
        sav.put(output4, 0.0);

        RLDataObject retval = sav.getHighestValuedMatchingAction(Arrays.asList(output, output2, output3, output4));
        assertNull(retval);

        sav.put(output4, 1.0);
        retval = sav.getHighestValuedMatchingAction(Arrays.asList(output, output2, output3, output4));
        assertEquals(retval, output4);

        sav.put(output3, 1.0);
        retval = sav.getHighestValuedMatchingAction(Arrays.asList(output, output2, output3, output4));
        assertNull(retval);

        //public RLDataObject getHighestValuedMatchingAction(List<RLDataObject> nextPossibleActions) {
    }

}
