package astra.learn.RLModel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import astra.learn.library.RLModel.RLDataModel;
import astra.learn.library.RLModel.RLDataObject;
import astra.term.Primitive;
import astra.term.Term;
import astra.type.Type;

/*
 * For Q Learning, represents Q(s,a)
 */
public class RLDataModelTest {
    
    static RLDataModel dm;

    
    @BeforeAll
    static void setup() {
    }


    @Test
    public void testParseBeliefTerms() throws Exception {
        Term[] beliefTerms12 = {new Primitive<Integer>(Type.INTEGER, 1),new Primitive<Integer>(Type.INTEGER, 2)};
        Term[] beliefTerms11 = {new Primitive<Integer>(Type.INTEGER, 1),new Primitive<Integer>(Type.INTEGER, 1)};
        Term[] beliefTerms21 = {new Primitive<Integer>(Type.INTEGER, 2),new Primitive<Integer>(Type.INTEGER, 1)};
        Term[] beliefTerms22 = {new Primitive<Integer>(Type.INTEGER, 2),new Primitive<Integer>(Type.INTEGER, 2)};
        dm = new RLDataModel();
        RLDataObject output = dm.parseBeliefTerms(beliefTerms11);
        assertEquals(output.getTerms(), beliefTerms11);
    }

    @Test
    public void testQ() throws Exception {
        Term[] beliefTerms12 = {new Primitive<Integer>(Type.INTEGER, 1),new Primitive<Integer>(Type.INTEGER, 2)};
        Term[] beliefTerms11 = {new Primitive<Integer>(Type.INTEGER, 1),new Primitive<Integer>(Type.INTEGER, 1)};
        Term[] beliefTerms21 = {new Primitive<Integer>(Type.INTEGER, 2),new Primitive<Integer>(Type.INTEGER, 1)};
        Term[] beliefTerms22 = {new Primitive<Integer>(Type.INTEGER, 2),new Primitive<Integer>(Type.INTEGER, 2)};
        dm = new RLDataModel();
        RLDataObject output = dm.parseBeliefTerms(beliefTerms11);

        
        assertEquals(output.getTerms(), beliefTerms11);
    }

    @Test
    public void testHash() throws Exception {
        dm = new RLDataModel();
        Term[] beliefTerms11 = {new Primitive<Integer>(Type.INTEGER, 1),new Primitive<Integer>(Type.INTEGER, 1)};
        RLDataObject output = dm.parseBeliefTerms(beliefTerms11);

        Term[] beliefTerms12 = {new Primitive<Integer>(Type.INTEGER, 1),new Primitive<Integer>(Type.INTEGER, 2)};
        RLDataObject output2 = dm.parseBeliefTerms(beliefTerms12);

        Term[] beliefTerms21 = {new Primitive<Integer>(Type.INTEGER, 2),new Primitive<Integer>(Type.INTEGER, 1)};
        RLDataObject output3 = dm.parseBeliefTerms(beliefTerms21);

        Term[] beliefTerms22 = {new Primitive<Integer>(Type.INTEGER, 2),new Primitive<Integer>(Type.INTEGER, 2)};
        RLDataObject output4 = dm.parseBeliefTerms(beliefTerms22);
       
        assertNotEquals(output.hashCode(), output2.hashCode());
        assertNotEquals(output.hashCode(), output3.hashCode());
        assertNotEquals(output.hashCode(), output4.hashCode());

        assertNotEquals(output2.hashCode(), output3.hashCode());
        assertNotEquals(output2.hashCode(), output4.hashCode());

        assertNotEquals(output4.hashCode(), output3.hashCode());

        assertEquals(output.getTerms(), beliefTerms11);
    }

}
