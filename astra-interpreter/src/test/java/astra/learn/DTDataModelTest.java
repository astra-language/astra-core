package astra.learn;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import astra.core.Agent;
import astra.learn.library.DTModel.DTDataModel;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;
import astra.type.Type;


public class DTDataModelTest {
    
    @Test
    public void checkEntropyStringPrimitiveAllSameTest() {
        Primitive t = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_setosa");
        Primitive t2 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_setosa");
        Primitive t3 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_setosa");
        Primitive t4 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_setosa");
        Primitive t5 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_setosa");

        ListTerm X = new ListTerm(new Term[]{}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4,t5});
        DTDataModel dt = new DTDataModel();

        assertEquals(0.0, dt.entropy(X, Y));
        
    }
    
    @Test
    public void checkEntropyStringPrimitiveEqualSplitTest() {
        Primitive t = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_setosa");
        Primitive t2 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_setosa");
        Primitive t3 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_v");
        Primitive t4 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_v");

        ListTerm X = new ListTerm(new Term[]{}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});
        DTDataModel dt = new DTDataModel();

        assertEquals(1.0, dt.entropy(X, Y));
        
    }

    @Test
    public void checkEntropyStringPrimitiveEqualSplitFourTest() {
        Primitive t = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/a");
        Primitive t2 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/b");
        Primitive t3 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/c");
        Primitive t4 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/d");

        ListTerm X = new ListTerm(new Term[]{}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});
        DTDataModel dt = new DTDataModel();

        assertEquals(2.0, dt.entropy(X, Y));
        
    }

    @Test
    public void checkEntropyStringPrimitiveUnEqualSplitTest() {
        Primitive t = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_setosa");
        Primitive t2 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_setosa");
        Primitive t3 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_v");
        Primitive t4 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_v");
        Primitive t5 = new Primitive<String>(Type.STRING, "http://dbpedia.org/resource/Iris_p");

        ListTerm X = new ListTerm(new Term[]{}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4,t5});
        DTDataModel dt = new DTDataModel();

        assertEquals(1.5219280948873621, dt.entropy(X, Y));
        
    }

    @Test
    public void checkEntropyFloatPrimitiveTest() {
        Primitive t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t3 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t4 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t5 = new Primitive<Float>(Type.FLOAT, new Float(0.2));

        ListTerm X = new ListTerm(new Term[]{}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4,t5});
        DTDataModel dt = new DTDataModel();

        assertEquals(0.0, dt.entropy(X, Y));
        
    }

    @Test
    public void checkEntropyFloatPrimitivequalSplitTest() {
        Primitive t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t3 = new Primitive<Float>(Type.FLOAT, new Float(0.4));
        Primitive t4 = new Primitive<Float>(Type.FLOAT, new Float(0.4));

        ListTerm X = new ListTerm(new Term[]{}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});
        DTDataModel dt = new DTDataModel();

        assertEquals(1.0, dt.entropy(X, Y));
        
    }

    @Test
    public void entropyAfterSplitCategoryPerfect() {
        //Split on second index, shoudl be good
        Primitive x11 =  new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        Primitive x12 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X1 = new ListTerm(new Term[]{x11,x12});
        Primitive x21 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        Primitive x22 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X2 = new ListTerm(new Term[]{x21,x22});
        Primitive x31 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        Primitive x32 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        ListTerm X3 = new ListTerm(new Term[]{x31,x32});
        Primitive x41 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        Primitive x42 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        ListTerm X4 = new ListTerm(new Term[]{x41,x42});

        Primitive t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t3 = new Primitive<Float>(Type.FLOAT, new Float(0.4));
        Primitive t4 = new Primitive<Float>(Type.FLOAT, new Float(0.4));

        ListTerm X = new ListTerm(new Term[]{X1,X2,X3,X4}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});
        DTDataModel dt = new DTDataModel();

        //Entropy group one + entropy group two
        // Which is going to be 0 + 0
        // But then weighted in proportion to size
        assertEquals(0.0, dt.entropyAfterSplitCategory(X, Y, 1));
        // This is going to be 1 + 1
         // But then weighted in proportion to size
        assertEquals(1.0, dt.entropyAfterSplitCategory(X, Y, 0));
        
    }

    @Test
    public void entropyAfterSplitContinuousPerfect() {
        //Split on second index, shoudl be good
        Primitive x11 = Primitive.newPrimitive(0.2);
        Primitive x12 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X1 = new ListTerm(new Term[]{x11,x12});
        Primitive x21 = Primitive.newPrimitive(0.3);
        Primitive x22 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X2 = new ListTerm(new Term[]{x21,x22});
        Primitive x31 = Primitive.newPrimitive(0.7);
        Primitive x32 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X3 = new ListTerm(new Term[]{x31,x32});
        Primitive x41 = Primitive.newPrimitive(0.8); 
        Primitive x42 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        ListTerm X4 = new ListTerm(new Term[]{x41,x42});
        //Expect midway point to be 0.5
        Primitive t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t3 = new Primitive<Float>(Type.FLOAT, new Float(0.4));
        Primitive t4 = new Primitive<Float>(Type.FLOAT, new Float(0.4));

        ListTerm X = new ListTerm(new Term[]{X1,X2,X3,X4}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});
        DTDataModel dt = new DTDataModel();

        //Expect this to be perfect, 0, because splitting on 0.2,0.3 in one group, which have the same 
        // output, and 0.7,0.8 in the other
        assertEquals(0.0, dt.entropyAfterBestSplitContinousDouble(X, Y, 0)); 
    }

    @Test
    public void informationGainAfterSplitContinuousPerfect() {
        //Split on second index, shoudl be good
        Primitive x11 = Primitive.newPrimitive(0.2);
        Primitive x12 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X1 = new ListTerm(new Term[]{x11,x12});
        Primitive x21 = Primitive.newPrimitive(0.3);
        Primitive x22 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X2 = new ListTerm(new Term[]{x21,x22});
        Primitive x31 = Primitive.newPrimitive(0.7);
        Primitive x32 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X3 = new ListTerm(new Term[]{x31,x32});
        Primitive x41 = Primitive.newPrimitive(0.8); 
        Primitive x42 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        ListTerm X4 = new ListTerm(new Term[]{x41,x42});
        //Expect midway point to be 0.5
        Primitive t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t3 = new Primitive<Float>(Type.FLOAT, new Float(0.4));
        Primitive t4 = new Primitive<Float>(Type.FLOAT, new Float(0.4));

        ListTerm X = new ListTerm(new Term[]{X1,X2,X3,X4}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});
        DTDataModel dt = new DTDataModel();

        //Expect this to be perfect, 0, because splitting on 0.2,0.3 in one group, which have the same 
        // output, and 0.7,0.8 in the other
        assertEquals(1.0, dt.informationGain(X, Y, 0)); 
    }

    @Test
    public void informationGainAfterSplitCateogryPerfect() {
        //Split on second index, shoudl be good
        Primitive x11 =  new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        Primitive x12 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X1 = new ListTerm(new Term[]{x11,x12});
        Primitive x21 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        Primitive x22 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X2 = new ListTerm(new Term[]{x21,x22});
        Primitive x31 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        Primitive x32 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        ListTerm X3 = new ListTerm(new Term[]{x31,x32});
        Primitive x41 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        Primitive x42 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        ListTerm X4 = new ListTerm(new Term[]{x41,x42});

        Primitive t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t3 = new Primitive<Float>(Type.FLOAT, new Float(0.4));
        Primitive t4 = new Primitive<Float>(Type.FLOAT, new Float(0.4));

        ListTerm X = new ListTerm(new Term[]{X1,X2,X3,X4}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});
        DTDataModel dt = new DTDataModel();

        //IG:
        // Current entropy (1), - entropy after split, which is 0
        // But then weighted in proportion to size
        assertEquals(1.0, dt.informationGain(X, Y, 1));
         // Current entropy (1), - entropy after split, which is 1
        assertEquals(0.0, dt.informationGain(X, Y, 0));
    }
    
    @Test
    public void informationGainAfterSplitStringPerfect() {
        //Split on second index, shoudl be good
        Primitive x11 =  new Primitive<String>(Type.STRING, "blue");
        Primitive x12 = new Primitive<String>(Type.STRING, "purple");
        ListTerm X1 = new ListTerm(new Term[]{x11,x12});
        Primitive x21 = new Primitive<String>(Type.STRING, "yellow");
        Primitive x22 = new Primitive<String>(Type.STRING,"purple");
        ListTerm X2 = new ListTerm(new Term[]{x21,x22});
        Primitive x31 = new Primitive<String>(Type.STRING,"blue");
        Primitive x32 = new Primitive<String>(Type.STRING, "orange");
        ListTerm X3 = new ListTerm(new Term[]{x31,x32});
        Primitive x41 = new Primitive<String>(Type.STRING,"yellow");
        Primitive x42 = new Primitive<String>(Type.STRING,"orange");
        ListTerm X4 = new ListTerm(new Term[]{x41,x42});

        Primitive t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t3 = new Primitive<Float>(Type.FLOAT, new Float(0.4));
        Primitive t4 = new Primitive<Float>(Type.FLOAT, new Float(0.4));

        ListTerm X = new ListTerm(new Term[]{X1,X2,X3,X4}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});
        DTDataModel dt = new DTDataModel();

        //IG:
        // Current entropy (1), - entropy after split, which is 0
        // But then weighted in proportion to size
        assertEquals(1.0, dt.informationGain(X, Y, 1));
         // Current entropy (1), - entropy after split, which is 1
        assertEquals(0.0, dt.informationGain(X, Y, 0));
    }

    @Test
    public void informationGainAfterSplitCharPerfect() {
        //Split on second index, shoudl be good
        Primitive x11 =  new Primitive<Character>(Type.CHAR, 'a');
        Primitive x12 =  new Primitive<Character>(Type.CHAR, '&');
        ListTerm X1 = new ListTerm(new Term[]{x11,x12});
        Primitive x21 = new Primitive<Character>(Type.CHAR, '5');
        Primitive x22 = new Primitive<Character>(Type.CHAR, '&');
        ListTerm X2 = new ListTerm(new Term[]{x21,x22});
        Primitive x31 =  new Primitive<Character>(Type.CHAR, 'a');
        Primitive x32 =  new Primitive<Character>(Type.CHAR, '*');
        ListTerm X3 = new ListTerm(new Term[]{x31,x32});
        Primitive x41 =  new Primitive<Character>(Type.CHAR, '5');
        Primitive x42 =new Primitive<Character>(Type.CHAR, '*');
        ListTerm X4 = new ListTerm(new Term[]{x41,x42});

        Primitive t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t3 = new Primitive<Float>(Type.FLOAT, new Float(0.4));
        Primitive t4 = new Primitive<Float>(Type.FLOAT, new Float(0.4));

        ListTerm X = new ListTerm(new Term[]{X1,X2,X3,X4}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});
        DTDataModel dt = new DTDataModel();

        //IG:
        // Current entropy (1), - entropy after split, which is 0
        // But then weighted in proportion to size
        assertEquals(1.0, dt.informationGain(X, Y, 1));
         // Current entropy (1), - entropy after split, which is 1
        assertEquals(0.0, dt.informationGain(X, Y, 0));
    }
    
    @Test
    public void informationGainAfterSplitCharAndStringMixPerfect() {
        //Split on second index, shoudl be good
        Primitive x11 =  new Primitive<Character>(Type.CHAR, 'a');
        Primitive x12 =  new Primitive<Character>(Type.CHAR, '&');
        ListTerm X1 = new ListTerm(new Term[]{x11,x12});
        Primitive x21 = new Primitive<Character>(Type.CHAR, '5');
        Primitive x22 = new Primitive<String>(Type.STRING, "&");
        ListTerm X2 = new ListTerm(new Term[]{x21,x22});
        Primitive x31 =  new Primitive<Character>(Type.CHAR, 'a');
        Primitive x32 =  new Primitive<Character>(Type.CHAR, '*');
        ListTerm X3 = new ListTerm(new Term[]{x31,x32});
        Primitive x41 =  new Primitive<Character>(Type.CHAR, '5');
        Primitive x42 =new Primitive<Character>(Type.CHAR, '*');
        ListTerm X4 = new ListTerm(new Term[]{x41,x42});

        Primitive t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t3 = new Primitive<Float>(Type.FLOAT, new Float(0.4));
        Primitive t4 = new Primitive<Float>(Type.FLOAT, new Float(0.4));

        ListTerm X = new ListTerm(new Term[]{X1,X2,X3,X4}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});
        DTDataModel dt = new DTDataModel();

        //IG:
        // Current entropy (1), - entropy after split, which is 0
        // But then weighted in proportion to size
        assertEquals(1.0, dt.informationGain(X, Y, 1));
         // Current entropy (1), - entropy after split, which is 1
        assertEquals(0.0, dt.informationGain(X, Y, 0));
    }

    @Test
    public void buildTreeContinuousPerfect() {
        //Split on second index, shoudl be good
        Primitive x11 = Primitive.newPrimitive(0.2);
        Primitive x12 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X1 = new ListTerm(new Term[]{x11,x12});
        Primitive x21 = Primitive.newPrimitive(0.3);
        Primitive x22 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X2 = new ListTerm(new Term[]{x21,x22});
        Primitive x31 = Primitive.newPrimitive(0.7);
        Primitive x32 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(true));
        ListTerm X3 = new ListTerm(new Term[]{x31,x32});
        Primitive x41 = Primitive.newPrimitive(0.8); 
        Primitive x42 = new Primitive<Boolean>(Type.BOOLEAN, new Boolean(false));
        ListTerm X4 = new ListTerm(new Term[]{x41,x42});
        //Expect midway point to be 0.5
        Primitive t = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t2 = new Primitive<Float>(Type.FLOAT, new Float(0.2));
        Primitive t3 = new Primitive<Float>(Type.FLOAT, new Float(0.4));
        Primitive t4 = new Primitive<Float>(Type.FLOAT, new Float(0.4));

        ListTerm X = new ListTerm(new Term[]{X1,X2,X3,X4}); //empty list, not checking X here
        ListTerm Y = new ListTerm(new Term[]{t,t2,t3,t4});
        DTDataModel dt = new DTDataModel();
        Agent a = new Agent("Learny");
        dt.setAgent(new Agent("Learny"));
        dt.buildTree(X, Y, null, 5);
        
        assertEquals(2, dt.getLeafNodes().size());
    }
}

