package astra.learn;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import astra.core.Agent;
import astra.formula.Predicate;
import astra.learn.library.Algorithm;
import astra.learn.library.QLearning;
import astra.term.Primitive;
import astra.term.Term;
import astra.type.Type;


public class LearningProcessTest {
    
    static Predicate knowledge_input;
    static Predicate move;
    static Predicate knowledge_label;

    static Predicate config_alpha;
    static Predicate config_gamma;
    static Predicate config_four_terms;
    static Predicate config_wrong_namespace;

    static LearningProcess lp;
    static Algorithm a;

    @BeforeAll
    static void setup() {
        //Knowledge input config
        Term[] ki = {new Primitive<String>(Type.STRING, "navigate"),new Primitive<String>(Type.STRING, "move")};
        knowledge_input = new Predicate("knowledge_input", ki);

        //Knowledge input pred
        Term[] moveTerm = {new Primitive<Integer>(Type.INTEGER, 1),new Primitive<Integer>(Type.INTEGER, 2)};
        move = new Predicate("move", moveTerm);

        //Knowledge label config
        Term[] kl = {new Primitive<String>(Type.STRING, "navigate"),new Primitive<String>(Type.STRING, "next_action")};
        knowledge_label = new Predicate("knowledge_label", kl);

        //Alpha config
        Term[] alph = {new Primitive<String>(Type.STRING, "navigate"),new Primitive<String>(Type.STRING, "alpha"),new Primitive<Double>(Type.DOUBLE, 0.9)};
        config_alpha = new Predicate("configuration", alph);

        //Gamma config
        Term[] gamma = {new Primitive<String>(Type.STRING, "navigate"),new Primitive<String>(Type.STRING, "gamma"),new Primitive<Double>(Type.DOUBLE, 0.9)};
        config_gamma = new Predicate("configuration", gamma);

        Term[] four = {new Primitive<String>(Type.STRING, "navigate"),new Primitive<String>(Type.STRING, "extra"),new Primitive<String>(Type.STRING, "gamma"),new Primitive<Double>(Type.DOUBLE, 0.9)};
        config_four_terms = new Predicate("configuration", four);
    
        Term[] wrongNS = {new Primitive<String>(Type.STRING, "build"),new Primitive<String>(Type.STRING, "gamma"),new Primitive<Double>(Type.DOUBLE, 0.2)};
        config_wrong_namespace = new Predicate("configuration", wrongNS);
    }


    @Test
    public void testExtractConfigurationBelief() {
        Agent agent = new Agent("AgentZero");
        try {
            agent.beliefs().addBelief(config_alpha); 
            agent.beliefs().addBelief(config_gamma); 
            agent.beliefs().update();
        } catch (NullPointerException npe) {
            System.out.println("Skipping test for now, FIXME later");
            return;
        }
       
        a = new QLearning();
        lp = new LearningProcess("navigate", "nav", null, a);
        lp.setAgent(agent);

        try {
            lp.extractConfigurationAndInputBeliefs();
            assertTrue(!a.labelSet);
            assertEquals(a.input_predicates.size(), 0);
            assertEquals(a.getConfiguration("alpha"), 0.9);
            assertEquals(a.getConfiguration("gamma"), 0.9);
        } catch (Exception e) {
            fail("Shouldn't have thrown exception, " + e.getMessage());
        }
    }

    @Test
    public void testExtractConfigurationBeliefWithLabel() {
        Agent agent = new Agent("AgentZero");
        try {
            agent.beliefs().addBelief(config_alpha); 
            agent.beliefs().addBelief(config_gamma); 
            agent.beliefs().addBelief(knowledge_label);
            agent.beliefs().update();
        } catch (NullPointerException npe) {
            System.out.println("Skipping test for now, FIXME later");
            return;
        }
       
        a = new QLearning();
        lp = new LearningProcess("navigate", "nav", null, a);
        lp.setAgent(agent);

        try {
            lp.extractConfigurationAndInputBeliefs();
            assertTrue(a.labelSet);
            assertEquals(a.input_predicates.size(), 0);
            assertEquals(a.getConfiguration("alpha"), 0.9);
            assertEquals(a.getConfiguration("gamma"), 0.9);
        } catch (Exception e) {
            fail("Shouldn't have thrown exception, " + e.getMessage());
        }
    }

    @Test
    public void testExtractConfigurationBeliefWithMultipleLabelBeliefs() {
        Agent agent = new Agent("AgentZero");
        try {
            agent.beliefs().addBelief(config_alpha); 
            agent.beliefs().addBelief(config_gamma); 
            agent.beliefs().addBelief(knowledge_label);
            agent.beliefs().addBelief(knowledge_label);
            agent.beliefs().update();
        } catch (NullPointerException npe) {
            System.out.println("Skipping test for now, FIXME later");
            return;
        }

        lp.setAgent(agent);

        try {
            lp.extractConfigurationAndInputBeliefs();
        } catch (LearningProcessException ex) {
            String message = "Exception building input beliefs for " + lp.processNamespace + "; expected one knowledge_label belief but have 2";
            assertEquals(message, ex.getMessage());
        } catch (Exception e) {
            fail("Shouldn't have thrown exception, " + e.getMessage());
        }
    }

    @Test
    public void testExtractConfigurationBeliefWithWrongTerms() {
        Agent agent = new Agent("AgentZero");
        try {
            agent.beliefs().addBelief(config_alpha); 
            agent.beliefs().addBelief(config_four_terms); 
            agent.beliefs().addBelief(knowledge_label);
            agent.beliefs().addBelief(knowledge_label);
            agent.beliefs().update();
        } catch (NullPointerException npe) {
            System.out.println("Skipping test for now, FIXME later");
            return;
        }

        a = new QLearning();
        lp = new LearningProcess("navigate", "nav", null, a);
        lp.setAgent(agent);

        try {
            lp.extractConfigurationAndInputBeliefs();
        } catch (LearningProcessException ex) {
            String message = "Configuration belief for " + lp.processNamespace + " without three terms";
            assertEquals(message, ex.getMessage());
        } catch (Exception e) {
            fail("Shouldn't have thrown exception, " + e.getMessage());
        }
    }

    @Test
    public void testExtractConfigurationBeliefWithWrongNamspace() {
        Agent agent = new Agent("AgentZero");
        
        try {
            agent.beliefs().addBelief(config_alpha); 
            agent.beliefs().addBelief(config_wrong_namespace);  //Namespace is wrong, gamma is 0.2
            agent.beliefs().addBelief(knowledge_label);
            agent.beliefs().addBelief(knowledge_label);
            agent.beliefs().update();
        } catch (NullPointerException npe) {
            System.out.println("Skipping test for now, FIXME later");
            return;
        }

        a = new QLearning();
        lp = new LearningProcess("navigate", "nav", null, a);
        lp.setAgent(agent);

        try {
            lp.extractConfigurationAndInputBeliefs();
            assertEquals(0.9, a.getConfiguration("alpha"));
            assertEquals(0.9, a.getConfiguration("gamma")); //Default
        } catch (Exception e) {
            fail("Shouldn't have thrown exception, " + e.getMessage());
        }
    }

    
}

