package astra.learn;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import astra.core.Agent;
import astra.formula.Predicate;
import astra.learn.library.Algorithm;
import astra.learn.library.QLearning;

public class QLearningTest {

    static Predicate knowledge_input;
    static Predicate move;
    static Predicate knowledge_label;

    static Predicate config_alpha;
    static Predicate config_gamma;
    static Predicate config_four_terms;
    static Predicate config_wrong_namespace;

    static LearningProcess lp;
    static Algorithm a;

    static Agent agent;
    
    @BeforeAll
    static void setup() {
    }


    @Test
    public void testSetConfiguration() throws Exception {
        QLearning qLearning = new QLearning();
        qLearning.setConfiguration("alpha", "0.9");
        assertEquals(0.9, qLearning.getConfiguration("alpha"));

        qLearning.setConfiguration("gamma", "0.8");
        assertEquals(0.8, qLearning.getConfiguration("gamma"));

        try {
            //No such configuration
            qLearning.setConfiguration("beta", "0.9");
        } catch (LearningProcessException ex) {
            assertEquals("Could not set Q-Learning configuration for beta", ex.getMessage());

        }
    }

    
}

