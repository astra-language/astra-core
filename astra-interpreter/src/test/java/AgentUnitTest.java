
import org.junit.jupiter.api.Test;

import astra.core.Agent;
import astra.core.Rule;
import astra.event.GoalEvent;
import astra.formula.Goal;
import astra.formula.Predicate;
import astra.statement.CustomStatement;
import astra.term.Term;

public class AgentUnitTest {
    @Test
    public void createAgentTest() {
        Agent agent = new Agent("rem");
        Goal goal =new Goal(new Predicate("happy", new Term[] {}));
        agent.addRule(
            new Rule(new GoalEvent(GoalEvent.ADDITION, goal),
                    new CustomStatement()
            )
        );
        agent.initialize(goal);
        agent.execute();
    }
}
