package astra.reasoner;

import java.util.Map;

import astra.core.Agent;
import astra.formula.Formula;
import astra.term.Term;

public interface FormulaUnifier {
    boolean isApplicable(Formula source, Formula target);
    Map<Integer, Term> unify(Formula source, Formula target, Map<Integer, Term> bindings, Agent agent);
}
