package astra.reasoner.unifier;

import java.util.Map;

import astra.core.Agent;
import astra.event.TestGoalEvent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class TestGoalEventUnifier implements EventUnifier<TestGoalEvent> {
	@Override
	public Map<Integer, Term> unify(TestGoalEvent source, TestGoalEvent target, Agent agent) {
		return (source.type() == target.type()) ? Unifier.unify(source.goal, target.goal, agent):null;
	}

}
