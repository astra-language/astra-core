package astra.reasoner.unifier;

import java.util.Map;
import astra.core.Agent;
import astra.formula.BracketFormula;
import astra.formula.Formula;
import astra.reasoner.FormulaUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class BracketUnifier implements FormulaUnifier {

	@Override
	public boolean isApplicable(Formula source, Formula target) {
        return (source instanceof BracketFormula) && (target instanceof BracketFormula);
    }

	@Override
	public Map<Integer, Term> unify(Formula source, Formula target, Map<Integer, Term> bindings, Agent agent) {
        BracketFormula s = (BracketFormula) source;
        BracketFormula t = (BracketFormula) target;

        Map<Integer, Term> temp = Unifier.unify(s.formula(), t.formula(), bindings, agent);
        if (temp == null)
            return null;

        
        return temp;
	}
    
}
