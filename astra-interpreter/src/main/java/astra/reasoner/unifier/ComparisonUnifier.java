package astra.reasoner.unifier;

import java.util.Map;
import astra.core.Agent;
import astra.formula.BracketFormula;
import astra.formula.Comparison;
import astra.formula.Formula;
import astra.reasoner.FormulaUnifier;
import astra.reasoner.Unifier;
import astra.reasoner.util.Utilities;
import astra.term.Term;

public class ComparisonUnifier implements FormulaUnifier {

	@Override
	public boolean isApplicable(Formula source, Formula target) {
        return (source instanceof Comparison) && (target instanceof Comparison);
    }

	@Override
	public Map<Integer, Term> unify(Formula source, Formula target, Map<Integer, Term> bindings, Agent agent) {
        Comparison s = (Comparison) source;
        Comparison t = (Comparison) target;

        if (!s.operator().equals(t.operator())) {
            //Operators don't match
            return null;
        }

        Map<Integer, Term> temp = Unifier.unify(new Term[]{s.left()}, new Term[]{t.left()}, bindings, agent);
        if (temp == null)
            return null;

        temp = Unifier.unify(new Term[]{s.right()}, new Term[]{t.right()}, Utilities.merge(temp, bindings), agent);
        if (temp == null)
            return null;

        
        return temp;
        
	}
    
}
