package astra.reasoner.unifier;

import java.util.Map;

import astra.core.Agent;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.reasoner.FormulaUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class PredicateUnifier implements FormulaUnifier  {

	@Override
	public boolean isApplicable(Formula source, Formula target) {
		return (source instanceof Predicate) && (target instanceof Predicate);
	}

	@Override
	public Map<Integer, Term> unify(Formula source, Formula target, Map<Integer, Term> bindings, Agent agent) {
		Predicate s = (Predicate) source;
		Predicate t = (Predicate) target;

		if (s.id() == t.id() && s.size() == t.size()) {
			return Unifier.unify(s.terms(), t.terms(), bindings, agent);
		}
		return null;
	}
    
}
