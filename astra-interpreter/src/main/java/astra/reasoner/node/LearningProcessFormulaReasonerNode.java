package astra.reasoner.node;

import java.util.Map;
import java.util.Stack;

import astra.formula.Formula;
import astra.formula.LearningProcessFormula;
import astra.reasoner.NewReasoner;
import astra.reasoner.Reasoner;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Term;

public class LearningProcessFormulaReasonerNode extends ReasonerNode {
    LearningProcessFormula formula;
    int state = 0;
    ReasonerNode child;

    public LearningProcessFormulaReasonerNode(ReasonerNode parent, LearningProcessFormula formula, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.formula = formula;
        this.initial = initial;
    }

    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        switch (state) {
            case 0:
            // System.out.println("------------- IN MFRN [0]");
            Formula f = formula.adaptor().invoke(
                    new BindingsEvaluateVisitor(initial, reasoner.agent()), 
                    formula.predicate());
                child = ((NewReasoner) reasoner).createReasonerNode(this, f, initial, singleResult);
                stack.push(child);
                state++;
                break;
            case 1:
                // System.out.println("------------- IN MFRN [1]");
                // System.out.println("Child: "+ child);
                // System.out.println("Child.solutions: "+ child.solutions);
                if (child.isFailed()) {
                    finished = true;
                    failed = true;
                    return false;
                }

                solutions = child.solutions;
                finished = true;
                return true;            
        }       

        return true;
    }
}
