package astra.reasoner.node;

import java.util.Map;

import astra.formula.ModuleFormula;
import astra.term.Term;

public class ModuleFormulaReasonerNodeFactory implements ReasonerNodeFactory<ModuleFormula> {

    @Override
    public ReasonerNode create(ModuleFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new ModuleFormulaReasonerNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, ModuleFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new ModuleFormulaReasonerNode(parent, formula, bindings, singleResult);
    }
    
}
