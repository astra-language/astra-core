package astra.reasoner.node;

import java.util.Map;

import astra.formula.LearningProcessFormula;
import astra.formula.ModuleFormula;
import astra.term.Term;

public class LearningProcessFormulaReasonerNodeFactory implements ReasonerNodeFactory<LearningProcessFormula> {

    @Override
    public ReasonerNode create(LearningProcessFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new LearningProcessFormulaReasonerNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, LearningProcessFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new LearningProcessFormulaReasonerNode(parent, formula, bindings, singleResult);
    }
    
}
