package astra.reasoner.node;

import java.util.Map;
import java.util.Stack;

import astra.formula.BooleanTermFormula;
import astra.reasoner.Reasoner;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Primitive;
import astra.term.Term;

public class BooleanTermFormulaNode extends ReasonerNode {
    ReasonerNode node;
    BooleanTermFormula formula;
    int state = 0;

    public BooleanTermFormulaNode(ReasonerNode parent, BooleanTermFormula formula, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.formula = formula;
        this.initial = initial;
    }

    @Override
    public ReasonerNode initialize(Reasoner reasoner) {
        visitor = new BindingsEvaluateVisitor(initial, reasoner.agent());
        formula = (BooleanTermFormula) formula.accept(visitor);
        // System.out.println("[Formula] " + formula);
        return super.initialize(reasoner);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        // System.out.println("[BTFN] solve called: "  + formula);
        // System.out.println("[BTFN] value: " + ((Primitive<Boolean>) formula.term()).value());
        finished = true;

        failed = !((Primitive<Boolean>) formula.term()).value();
        solutions.add(initial);
        return !failed;
    }

    public String toString() {
        return formula +  " = " + solutions();
    }
    
}
