package astra.reasoner.node;

import java.util.Map;

import astra.formula.Predicate;
import astra.term.Term;

public class PredicateReasonerNodeFactory implements ReasonerNodeFactory<Predicate> {

    @Override
    public ReasonerNode create(Predicate formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new PredicateReasonerNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, Predicate formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new PredicateReasonerNode(parent, formula, bindings, singleResult);
    }
    
}
