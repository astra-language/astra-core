package astra.reasoner.node;

import java.util.Map;

import astra.formula.Comparison;
import astra.term.Term;

public class ComparisonReasonerNodeFactory implements ReasonerNodeFactory<Comparison> {

    @Override
    public ReasonerNode create(Comparison formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new ComparisonReasonerNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, Comparison formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new ComparisonReasonerNode(parent, formula, bindings, singleResult);
    }
    
}
