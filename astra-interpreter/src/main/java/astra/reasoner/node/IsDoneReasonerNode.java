package astra.reasoner.node;

import java.util.Map;
import java.util.Stack;

import astra.formula.IsDone;
import astra.reasoner.Reasoner;
import astra.term.Term;

public class IsDoneReasonerNode extends ReasonerNode {
    IsDone isDone;

    public IsDoneReasonerNode(ReasonerNode parent, IsDone isDone, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.isDone = isDone;
        this.initial = initial;
    }

    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        if (reasoner.agent().intention().isGoalCompleted()) {
            solutions.add(initial);
            finished = true;
			return true;
		}             
        finished = true;
        failed=true;
        return false;
    }
}
