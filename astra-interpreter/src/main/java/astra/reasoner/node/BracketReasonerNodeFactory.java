package astra.reasoner.node;

import java.util.Map;

import astra.formula.BracketFormula;
import astra.term.Term;

public class BracketReasonerNodeFactory implements ReasonerNodeFactory<BracketFormula> {

    @Override
    public ReasonerNode create(BracketFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new BracketReasonerNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, BracketFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new BracketReasonerNode(parent, formula, bindings, singleResult);
    }
    
}
