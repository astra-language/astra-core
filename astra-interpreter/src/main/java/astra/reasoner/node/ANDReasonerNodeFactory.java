package astra.reasoner.node;

import java.util.Map;

import astra.formula.AND;
import astra.term.Term;

public class ANDReasonerNodeFactory implements ReasonerNodeFactory<AND> {

    @Override
    public ReasonerNode create(AND formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new ANDReasonerNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, AND formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new ANDReasonerNode(parent, formula, bindings, singleResult);
    }
    
}
