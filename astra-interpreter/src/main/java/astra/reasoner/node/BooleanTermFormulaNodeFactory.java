package astra.reasoner.node;

import java.util.Map;

import astra.formula.BooleanTermFormula;
import astra.term.Term;

public class BooleanTermFormulaNodeFactory implements ReasonerNodeFactory<BooleanTermFormula> {

    @Override
    public ReasonerNode create(BooleanTermFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new BooleanTermFormulaNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, BooleanTermFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new BooleanTermFormulaNode(parent, formula, bindings, singleResult);
    }
    
}
