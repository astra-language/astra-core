package astra.reasoner.node;

import java.util.Map;

import astra.formula.IsDone;
import astra.term.Term;

public class IsDoneReasonerNodeFactory implements ReasonerNodeFactory<IsDone> {

    @Override
    public ReasonerNode create(IsDone formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new IsDoneReasonerNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, IsDone formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new IsDoneReasonerNode(parent, formula, bindings, singleResult);
    }
    
}
