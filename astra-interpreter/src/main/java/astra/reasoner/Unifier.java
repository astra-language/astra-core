package astra.reasoner;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.script.Bindings;

import astra.core.Agent;
import astra.event.BeliefEvent;
import astra.event.Event;
import astra.event.GoalEvent;
import astra.event.TestGoalEvent;
import astra.formula.Formula;
import astra.formula.Goal;
import astra.formula.Predicate;
import astra.formula.TestGoal;
import astra.messaging.MessageEvent;
import astra.reasoner.unifier.ANDUnifier;
import astra.reasoner.unifier.AcreFormulaUnifier;
import astra.reasoner.unifier.BeliefEventUnifier;
import astra.reasoner.unifier.BracketUnifier;
import astra.reasoner.unifier.ComparisonUnifier;
import astra.reasoner.unifier.FormulaVariableUnifier;
import astra.reasoner.unifier.GoalEventUnifier;
import astra.reasoner.unifier.MessageEventUnifier;
import astra.reasoner.unifier.PredicateUnifier;
import astra.reasoner.unifier.TestGoalEventUnifier;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Funct;
import astra.term.LearningProcessTerm;
import astra.term.ListSplitter;
import astra.term.ListTerm;
import astra.term.ModuleTerm;
import astra.term.Primitive;
import astra.term.Term;
import astra.term.Variable;

/**
 * This class performs unification of logical formulae and events. Events can be
 * associated with this class through the following code:
 *
 * <code>
 * Unifier.eventFactory.put(EISEvent.class, new EISEventUnifier());
 * </code>
 * 
 * @author rem
 *
 */
public class Unifier {
	public static Map<Class<?>, EventUnifier<?>> eventFactory = new HashMap<>();
	public static List<FormulaUnifier> formulaUnifiers = new LinkedList<>();
	
	static {
		eventFactory.put(BeliefEvent.class, new BeliefEventUnifier());
		eventFactory.put(GoalEvent.class, new GoalEventUnifier());
		eventFactory.put(TestGoalEvent.class, new TestGoalEventUnifier());
		eventFactory.put(MessageEvent.class, new MessageEventUnifier());

		formulaUnifiers.add(new FormulaVariableUnifier());
		formulaUnifiers.add(new PredicateUnifier());
		formulaUnifiers.add(new AcreFormulaUnifier());
		formulaUnifiers.add(new ANDUnifier());
		formulaUnifiers.add(new BracketUnifier());
		formulaUnifiers.add(new ComparisonUnifier());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map<Integer, Term> unify(Event source, Event target, Agent agent) {
		EventUnifier unifier = eventFactory.get(source.getClass());
		// System.out.println("unifier:  " + source.getClass());
		return (unifier != null) ? unifier.unify(source, target, agent) : null;
	}

	/**
	 * Generate variable bindings for two predicates or return null if there is no
	 * binding...
	 * 
	 * @param source first predicate
	 * @param target second predicate
	 * @param agent  the agent for which the unification is being performed
	 * @return a {@link Bindings} object or null
	 */
	public static Map<Integer, Term> unify(Predicate source, Predicate target, Agent agent) {
		return unify(source, target, new HashMap<Integer, Term>(), agent);
	}

	/**
	 * Generate variable bindings for two achievement goals or return null if there
	 * is no binding...
	 * 
	 * @param source first goal
	 * @param target second goal
	 * @param agent  the agent for which the unification is being performed
	 * @return a {@link Bindings} object or null
	 */
	public static Map<Integer, Term> unify(Goal source, Goal target, Agent agent) {
		return unify(source.formula(), target.formula(), agent);
	}

	/**
	 * Generate variable bindings for two achievement goals or return null if there
	 * is no binding...
	 * 
	 * @param source first goal
	 * @param target second goal
	 * @param agent  the agent for which the unification is being performed
	 * @return a {@link Bindings} object or null
	 */
	public static Map<Integer, Term> unify(TestGoal source, TestGoal target, Agent agent) {
		return unify(source.formula(), target.formula(), agent);
	}

	public static Map<Integer, Term> unify(Term[] source, Term[] target, Map<Integer, Term> bindings, Agent agent) {
		BindingsEvaluateVisitor visitor = new BindingsEvaluateVisitor(bindings, agent);
		for (int i = 0; i < source.length; i++) {
			Term sourceTerm = source[i];
			Term targetTerm = target[i];
			if (sourceTerm instanceof ModuleTerm) {
				sourceTerm = Primitive.newPrimitive(((ModuleTerm) sourceTerm).evaluate(visitor));
			}
			if (targetTerm instanceof ModuleTerm) {
				targetTerm = Primitive.newPrimitive(((ModuleTerm) targetTerm).evaluate(visitor));
			}

			if (sourceTerm instanceof LearningProcessTerm) {
				sourceTerm = Primitive.newPrimitive(((LearningProcessTerm) sourceTerm).evaluate(visitor));
			}
			if (targetTerm instanceof LearningProcessTerm) {
				targetTerm = Primitive.newPrimitive(((LearningProcessTerm) targetTerm).evaluate(visitor));
			}

			// Do the actual unification of the terms...
			if (Variable.class.isInstance(sourceTerm)) {
				Variable var = (Variable) sourceTerm;

				Term term = bindings.get(var.id());
				if (term == null) {
					if (!var.type().equals(targetTerm.type())) {
						return null;
					}
					bindings.put(var.id(), targetTerm);
				} else {
					if (!term.equals(targetTerm)) {
						return null;
					}
				}
			} else if (Variable.class.isInstance(targetTerm)) {
				Variable var = (Variable) targetTerm;
				Term term = bindings.get(var.id());
				if (term == null) {
					if (!var.type().equals(sourceTerm.type())) {
						return null;
					}
					bindings.put(var.id(), sourceTerm);
				} else {
					if (!term.equals(sourceTerm)) {
						return null;
					}
				}
			} else if (ListTerm.class.isInstance(sourceTerm)) {
				if (ListTerm.class.isInstance(targetTerm)) {
					if (((ListTerm) sourceTerm).size() != ((ListTerm) targetTerm).size())
						return null;
					if (unify(((ListTerm) sourceTerm).terms(), ((ListTerm) targetTerm).terms(), bindings,
							agent) == null)
						return null;
				} else if (ListSplitter.class.isInstance(targetTerm)) {
					ListTerm list = (ListTerm) sourceTerm;
					if (list.size() < 1)
						return null;
					ListTerm tail = new ListTerm(list.subList(1, list.size()).toArray(new Term[list.size() - 1]));
					if (unify(new Term[] { ((ListTerm) sourceTerm).get(0), tail },
							new Term[] { ((ListSplitter) targetTerm).head(), ((ListSplitter) targetTerm).tail() },
							bindings, agent) == null)
						return null;
				} else
					return null;
			} else if (ListTerm.class.isInstance(targetTerm)) {
				if (ListSplitter.class.isInstance(sourceTerm)) {
					ListTerm list = (ListTerm) targetTerm;
					if (list.size() < 1)
						return null;

					ListTerm tail = new ListTerm(list.subList(1, list.size()).toArray(new Term[list.size() - 1]));
					if (unify(new Term[] { ((ListTerm) targetTerm).get(0), tail },
							new Term[] { ((ListSplitter) sourceTerm).head(), ((ListSplitter) sourceTerm).tail() },
							bindings, agent) == null)
						return null;
				} else
					return null;
			} else if (Funct.class.isInstance(sourceTerm)) {
				if (!Funct.class.isInstance(targetTerm))
					throw new RuntimeException("Could not match: " + sourceTerm + " to: " + targetTerm
							+ "\nTarget is not a funct, but is: " + target.getClass().getCanonicalName());
				Funct sf = (Funct) sourceTerm;
				Funct tf = (Funct) targetTerm;
				if (sf.id() != tf.id() || sf.size() != tf.size())
					return null;
				// System.out.println("in funct comparison: " + sourceTerm + " / " +
				// targetTerm);
				// System.out.println("\tWE HAVE AN ID MATCH");
				if (unify(((Funct) sourceTerm).terms(), ((Funct) targetTerm).terms(), bindings, agent) == null)
					return null;
				// System.out.println("\tWE HAVE A TERM MATCH");
			} else if (!sourceTerm.equals(targetTerm)) {
				// System.out.println("terms are not equal: " + sourceTerm + " / " +
				// targetTerm);
				return null;
			}

		}

		return bindings;
	}

	public static Map<Integer, Term> unify(Formula source, Formula target, Map<Integer, Term> bindings, Agent agent) {
		for (FormulaUnifier unifier : formulaUnifiers) {
			if (unifier.isApplicable(source, target)) {
				return unifier.unify(source, target, bindings, agent);
			}
		}

		return null;
	}
}
