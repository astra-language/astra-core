package astra.reasoner.util;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class StringMapper {
	private Map<String, Integer> stringMap = new HashMap<String, Integer>();
	private Map<Integer, String> idMap = new TreeMap<Integer, String>();

	public synchronized int toId(String predicate) {
		Integer id = stringMap.get(predicate);
		if (id == null) {
			id = predicate.hashCode();
			idMap.put(id, predicate);
			stringMap.put(predicate, id);
		}
		return id;
	}
	
	public String fromId(int id) {
		return idMap.get(id);
	}

}
