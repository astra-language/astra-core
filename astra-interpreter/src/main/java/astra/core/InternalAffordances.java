package astra.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class InternalAffordances {

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface ACTION {
		boolean inline() default true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface TERM {}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface FORMULA {
		String[] types() default {};
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface SUPPRESS_NOTIFICATIONS {}
	
	protected Agent agent;
	
	public void setAgent(Agent agent) {
		this.agent = agent;
	}
	
	public boolean inline() {
		return false;
	}

}
