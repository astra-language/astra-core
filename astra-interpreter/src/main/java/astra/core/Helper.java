package astra.core;

import java.util.List;
import java.util.Map;

import astra.debugger.Breakpoints;
import astra.event.Event;
import astra.event.ModuleEvent;
import astra.reasoner.Unifier;
import astra.term.Term;

public class Helper {
	public static Event resolveEvent(Event event, Agent agent) {
		if (event instanceof ModuleEvent) {
			event = ((ModuleEvent) event).adaptor().generate(agent,((ModuleEvent) event).event());
		}
		return event;
	}

	public static Map<Integer, Term> evaluateRule(Agent agent, Rule rule, Event event) {
		if (agent.trace()) System.out.println("["+agent.name()+"] \tEvent:  " + event);
		Event triggeringEvent = resolveEvent(rule.event, agent);
		if (agent.trace()) System.out.println("["+agent.name()+"] \tTriggering Event:  " + triggeringEvent);
		Map<Integer, Term> bindings = Unifier.unify(triggeringEvent, event, agent);
		if (agent.trace()) System.out.println("["+agent.name()+"] \tBindings:  " + bindings);

		Breakpoints.getInstance().check(agent, rule, triggeringEvent, bindings);

		if (bindings != null) {
			if (agent.trace()) System.out.println("["+agent.name()+"] \tRule.context: " + rule.context);
			List<Map<Integer, Term>> results = agent.query(rule.context, bindings);
			if (agent.trace()) System.out.println("["+agent.name()+"]>>>>>>>>>>>> results: " + results);
			Breakpoints.getInstance().check(agent, rule, rule.context, results);
			
			if (results != null) {
				if (!results.isEmpty()) {
					bindings.putAll(results.get(0));
				}
				return bindings;
			}
		}

		return null;
	}
}
