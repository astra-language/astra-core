package astra.learn;

public class LearningProcessException extends RuntimeException {
    
    public LearningProcessException(String msg) {
        super(msg);
    }
}
