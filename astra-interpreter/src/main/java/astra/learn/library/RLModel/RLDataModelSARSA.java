package astra.learn.library.RLModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import astra.formula.Formula;
import astra.learn.library.QLearning;

public class RLDataModelSARSA extends RLDataModel {

    public RLDataModelSARSA() {
        Q = new HashMap<>();
        actions = new ArrayList<>();
       // trace = true;
    }

    /*
     * Where Q(S,A) <- Q(S,A) + alpha[R + (gamma * Q(S',A') - Q(S,A)] happens
     * 
     * Q(S,A) - the value for taking action A FROM state S
     * max Q(S',a) - the maximum possible value from any action taken in the NEXT state (the state gained from taking
     * action A FROM state S)
     * 
     * BUT - what we have available is S' (S current), and we cache information about the previous state S (S previous)
     * 
     * We compare S previous (S) and S current (S') to infer the Action A that was taken
     * 
     * So we are looking at the value for the move (A) from S, previous, to S', current.
     * 
     * max Q(S',a) is the maximum score for moving from the current location, to another one.
     */ 
    public Map<String, Double> update(List<Formula> new_inputs, List<Formula> new_actions, List<Formula> new_reward, double gamma, double alpha, Map<String, Double> metrics, boolean evaluate) {
        //The reward we get in the environment is because we have moved into the current state S', by taking a previous action, A
        //So Q(S,A) is how good was it for me to take this action A, from the previous state S, which got me here? 
        if (previous_state == null && previous_action == null) {
            //This is the first iteration of the learning process
            previous_action = action;
            previous_state = state;
            return metrics;
        }

        if (trace) System.out.println("S, previous state " + previous_state.toString());
        if (trace) System.out.println("A, previous action " + previous_action.toString());
        // Q(S,A)
        StateActionValue sav = Q.get(previous_state);
        if (sav == null) {
            sav = new StateActionValue();
        }

        Double currentValue = sav.getValueForAction(previous_action);
        if (currentValue == null) {
            currentValue = 0.0;
        }
        if (trace) System.out.println("Q(S,A) is " + currentValue);
        
        //Q(S',A') is the maximum score for moving from the current location, to another one.
        StateActionValue sav_current = Q.get(state);
        if (sav_current == null) {
            sav_current = new StateActionValue();
            Q.put(state, sav_current);
        }
        if (trace) System.out.println("S', current state " + state.toString());
        if (trace) System.out.println("A', next action " + action.toString());
        // Q(S',A')
        Double q_s_n_a = sav_current.getValueForAction(action);
        if (q_s_n_a == null) {
            q_s_n_a = 0.0;
        }
        if (trace) System.out.println("Q(S',A') " + q_s_n_a);
        double addition = (alpha * (reward + (gamma * q_s_n_a.doubleValue()) - currentValue.doubleValue()));
        double newValue = currentValue.doubleValue() + addition;
        
        //Update metrics
        for (String key: metrics.keySet()) {
            if (key.equals(QLearning.TOTAL_REWARD)) {
                double totalDiscountdb = metrics.get(key) + reward;
                metrics.put(key, totalDiscountdb);
            }
            if (key.equals(QLearning.TOTAL_DISCOUNTED_REWARD)) {
                double totalDiscountdb = metrics.get(key) + addition;
                metrics.put(key, totalDiscountdb);
            }
        }

        // Update Q(S,A) - UNLESS IN EVALUATION MODE
        if (trace) System.out.println("After update, new value is " + newValue);
        if (!evaluate) {
            if (trace) System.out.println("Updated Q(S, A) for state(" + previous_state.toString() + "), action(" + action.toString() + ")");
            sav.put(previous_action, newValue);
            Q.put(previous_state, sav);
        } else {
            if (trace) System.out.println("Evaluation mode: Did not update Q(S, A) for state(" + previous_state.toString() + "), action(" + action.toString() + ")");
        }
        
        for (RLDataObject key : Q.keySet()) {
            if (trace) System.out.println("For Q(S, ...), S= " + key.toString() + ", Q(S,A) is ");
            if (trace) System.out.println(Q.get(key).toString());
        }

        previous_action = action;
        previous_state = state;
        return metrics;
    }

    @Override
    public String print() {
        StringBuffer sb = new StringBuffer();
        sb.append("RLDataModelSARSA ");
        for (RLDataObject key: Q.keySet()) {
            sb.append("\nKey: ");
            sb.append(key.toString());
            StateActionValue sav = Q.get(key);
            sb.append("\n");
            sb.append(sav.toString());
        }
        sb.append("\nEnd Model");
        return sb.toString();
    }
}
