package astra.learn.library.RLModel;

import astra.statement.BeliefUpdate;

public class BeliefUpdateScoreTuple {
    
    private double score;
    private BeliefUpdate bu;

    public BeliefUpdateScoreTuple(BeliefUpdate bu, double score) {
        this.bu = bu;
        this.score = score;
    }

    public double getScore() {
        return score;
    }

    public BeliefUpdate getBeliefUpdate() {
        return bu;
    }
}
