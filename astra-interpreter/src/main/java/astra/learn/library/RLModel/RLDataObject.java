package astra.learn.library.RLModel;


import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;

public class RLDataObject {
    
    private Term[] terms;

    public RLDataObject() {

    }

    public RLDataObject(Term[] terms) {
        this.terms = terms;
    }

    public Term[] getTerms() {
        return terms;
    }

    /*
     * Order matters
     */
    @Override
    public int hashCode() {
        int multiplier = 1;
        int hashcode = 0;
        for (Term t: this.terms) {
            int pcode = t.toString().hashCode();
            hashcode += (pcode * multiplier);
            multiplier++;
        }
        return hashcode;
    }

    @Override
    public boolean equals(Object o) {

        if (!(o instanceof RLDataObject)) {
            return false;
        }
        
        RLDataObject otherState = (RLDataObject)o;
        Term[] otherTerms = otherState.getTerms();

        //Arrays.equals won't work - this compares the orders of elements, here order doesn't matter
        if (otherTerms.length != this.terms.length) {
            return false;
        }

        //Otherwise, for each element in OUR terms, is that in ther otherTerms array? - order matters
        for (int i=0; i<this.terms.length; i++) {
            try {
                if (this.terms[i] instanceof Primitive && otherTerms[i] instanceof Primitive) {
                    Primitive pthis = (Primitive)this.terms[i];
                    Primitive pOther = (Primitive)otherTerms[i];
                    if (!pthis.equals(pOther)) {
                        return false;
                    }
                } else if (this.terms[i] instanceof ListTerm && otherTerms[i] instanceof ListTerm) {
                    ListTerm pthis = (ListTerm)this.terms[i];
                    ListTerm pOther = (ListTerm)otherTerms[i];
                    if (!pthis.equals(pOther)) {
                        return false;
                    }
                 else {
                    return false;
                 }}
                
            } catch (Exception e) {
                System.out.println("Exception comparing terms : " + e.getMessage());
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        String retval = "";
        boolean first = true;
        for (Term p : this.terms) {
            if (!first) {
                retval += ", ";
            }
            retval += p.toString();
            first = false;
        }
        return retval;
    }
}
