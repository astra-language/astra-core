package astra.learn.library.RLModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class StateActionValue {

    private HashMap<RLDataObject, Double> actionValueMap;

    public StateActionValue() {
        this.actionValueMap = new HashMap<>();
    }

    public Double getValueForAction(RLDataObject action) {
        return actionValueMap.get(action);
    }

    public void put(RLDataObject action, Double value) {
        actionValueMap.put(action, value);
    }

    public Double getHighestValue() {
        //Start low!
        Double highest = null;
        try {
            for (RLDataObject action: actionValueMap.keySet()) {
                Double value = actionValueMap.get(action);
                if (value != null && highest == null || value >= highest) {
                    highest = value;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        return highest;
    }

    public HashMap<RLDataObject, Double> getActionValueMap() {
        return actionValueMap;
    }

    public RLDataObject getHighestValuedMatchingAction(List<RLDataObject> nextPossibleActions) {
        //Start on the assumption that there is no action, and no highest value!
        //BUT - if all actions are equally bad, or good, return null because want random behaviour.
        Double highest = null;
        int size = nextPossibleActions.size();
        Double[] values = new Double[nextPossibleActions.size()];
        int highestValuePosition = 0;
        for (int i = 0; i<size; i++) {
            Double value = actionValueMap.get(nextPossibleActions.get(i));
            if (value != null) {
                values[i] = value;
                //Have to do >= because it may have equally valued actions
                if (highest == null || (value >= highest)) {
                    highest = value;
                    highestValuePosition = i;
                }
            }
        }
        if (highest == null) {
            return null;
        }
        //CHECK - for highest value, is there another equal one?
        for (int i = 0; i<size; i++) {
            if (i != highestValuePosition) {
                Double otherValue = values[i];
                if ((otherValue != null) && (Double.compare(highest, values[i]) == 0)) {
                        //Equal
                        return null;
                } 
            }  
        }
        return nextPossibleActions.get(highestValuePosition);
    }
    
    @Override
    public String toString() {
        String retval = "";
        boolean first = true;
        for (Entry<RLDataObject,Double> entry: actionValueMap.entrySet()) {
            if (first) {
                first = false;
            } else {
                retval += "\n";
            }
            retval += entry.getKey().toString() + " : " +  entry.getValue().toString();
        }
        return retval;
    }

    /*
     * Order matters
     */
    @Override
    public int hashCode() {
        int hashcode = 0;
        for (RLDataObject p: actionValueMap.keySet()) {
            int pcode = p.toString().hashCode();
            hashcode += pcode;
        }
        return hashcode;
    }

}
