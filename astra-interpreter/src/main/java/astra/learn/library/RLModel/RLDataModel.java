package astra.learn.library.RLModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import astra.formula.Formula;
import astra.formula.Predicate;
import astra.learn.LearningProcessException;
import astra.learn.library.DataModel;
import astra.learn.library.QLearning;
import astra.statement.BeliefUpdate;
import astra.term.Term;
import astra.type.Type;

public class RLDataModel extends DataModel {

    public Random generator = new Random();
    
    protected RLDataObject state; //S_t
    protected List<RLDataObject> actions; // set A | S_t
    protected RLDataObject action; //A_t, when chosen
    protected String next_action; //Predicate to set A_t
    
    protected RLDataObject previous_state; // S_(t-1)
    protected RLDataObject previous_action; // A_(t-1)
    protected double reward; // R_(t-1)
    protected boolean trace = false;

    protected HashMap<RLDataObject, StateActionValue> Q;
    
    public RLDataModel() {
        Q = new HashMap<>();
        actions = new ArrayList<>();
    }

    public RLDataObject parseBeliefTerms(Term[] terms) {
        //Convert to RLDataModel
        return new RLDataObject(terms);
    }

    public void setOutputLabel(Term[] terms) {
       if (terms.length == 1) {
            Term t = terms[0];
            if (t.type().equals(Type.STRING))  {
                next_action = Type.stringValue(t);
            }
        } else {
            throw new LearningProcessException("Given input for the knowledge_label belief in RL process is incorrect: it should have one term, of type string");
        }
    }

    public double parseReward(Term[] terms) {
        if (terms.length == 1) {
            Term t = terms[0];
            if (t.type().equals(Type.FLOAT) || t.type().equals(Type.INTEGER) || t.type().equals(Type.DOUBLE) || t.type().equals(Type.LONG))  {
                return Type.doubleValue(t);
            }
        }
        throw new LearningProcessException("Given input for the knowledge_reward belief in RL process is incorrect: it should have one term, of type float, int, double or long");
    }

    public BeliefUpdateScoreTuple nextActionRandom() {
        if (trace) System.out.println("Exploring");
        //Get from actions
        if (trace) System.out.println("Actions size is " + actions.size());
        if (actions.size() < 1) {
            return null;
        }
        int random_action = (int)(Math.random() * actions.size());
        //int random_action = generator.nextInt(actions.size());
        //Collections.shuffle(actions); //Help the randomness
        //Set the action
        action = actions.get(random_action);
        if (trace) System.out.println("Randomly selected action is " + action.toString());
        //Return a belief update for that action
        StateActionValue sav = Q.get(state);
        Double score = null;
        if (sav != null) {
            score = sav.getValueForAction(action);
        }
        return createBeliefUpdateForAction(score);
    }

    private BeliefUpdateScoreTuple createBeliefUpdateForAction(Double score) {
        Term[] newTerms = action.getTerms().clone();
        Predicate p = new Predicate(next_action, newTerms);
        if (score == null) {
            score = new Double(0.0);
        }
        return new BeliefUpdateScoreTuple(new BeliefUpdate('+', p), score);
    }
    
    public BeliefUpdateScoreTuple nextActionHighestValue() {
        if (trace) System.out.println("Exploiting");
        StateActionValue sav = Q.get(state);
        if (sav != null) {
           
            RLDataObject highestValueAction = sav.getHighestValuedMatchingAction(actions);
            if (highestValueAction == null) {
                return nextActionRandom();
            }
            
            if (highestValueAction != null) {
                action = highestValueAction;
                Double score = sav.getValueForAction(action);
                return createBeliefUpdateForAction(score);
            }
            //Else there is no highest value, so default to random
        } //Else there is no prior information about this state so default to random
        
        return nextActionRandom();
    }

    public void clearActions() {
        actions = new ArrayList<>(); //Clear actions at end
    }

    /*
     * Where Q(S,A) <- Q(S,A) + alpha[R + (gamma * max Q(S',a) - Q(S,A)] happens
     * 
     * Q(S,A) - the value for taking action A FROM state S
     * max Q(S',a) - the maximum possible value from any action taken in the NEXT state (the state gained from taking
     * action A FROM state S)
     * 
     * BUT - what we have available is S' (S current), and we cache information about the previous state S (S previous)
     * 
     * We compare S previous (S) and S current (S') to infer the Action A that was taken
     * 
     * So we are looking at the value for the move (A) from S, previous, to S', current.
     * 
     * max Q(S',a) is the maximum score for moving from the current location, to another one.
     */ 
    public Map<String, Double> update(List<Formula> new_inputs, List<Formula> new_actions, List<Formula> new_reward, 
                        double gamma, double alpha, Map<String, Double> metrics, boolean evaluate) {
        parseActions(new_actions);
        parseState(new_inputs);
        parseReward(new_reward);
        //The reward we get in the environment is because we have moved into the current state S', by taking a previous action, A
        //So Q(S,A) is how good was it for me to take this action A, from the previous state S, which got me here? 
        if (previous_state == null && previous_action == null) {
            //This is the first iteration of the learning process
            previous_action = action;
            previous_state = state;
            return metrics;
        }
        if (trace) System.out.println("Updating for previous state " + previous_state.toString());
        if (trace) System.out.println("and action " + action.toString());
        // Q(S,A)
        StateActionValue sav = Q.get(previous_state);
        if (sav == null) {
            sav = new StateActionValue();
        }

        Double currentValue = sav.getValueForAction(action);
        if (currentValue == null) {
            currentValue = 0.0;
        }
        if (trace) System.out.println("Current value is " + currentValue);
        //max Q(S',a) is the maximum score for moving from the current location, to another one.
        StateActionValue sav_current = Q.get(state);
        if (sav_current == null) {
            sav_current = new StateActionValue();
            Q.put(state, sav_current);
        }
        if (trace) System.out.println("Now get nax value for state and any subsequent action " + state.toString());
        //max Q(S',a)
        Double q_s_n_a = sav_current.getHighestValue();
        if (q_s_n_a == null) {
            q_s_n_a = 0.0;
        }
        if (trace) System.out.println("Max value from current state, " + state.toString() + " is " + q_s_n_a);
        double addition = (alpha * (reward + (gamma * q_s_n_a.doubleValue()) - currentValue.doubleValue()));
        double newValue = currentValue.doubleValue() + addition;
        
        //Update metrics
        for (String key: metrics.keySet()) {
            if (key.equals(QLearning.TOTAL_REWARD)) {
                double totalDiscountdb = metrics.get(key) + reward;
                metrics.put(key, totalDiscountdb);
            }
            if (key.equals(QLearning.TOTAL_DISCOUNTED_REWARD)) {
                double totalDiscountdb = metrics.get(key) + addition;
                metrics.put(key, totalDiscountdb);
            }
        }

        // Update Q(S,A) - UNLESS IN EVALUATION MODE
        if (trace) System.out.println("After update, new value is " + newValue);
        if (!evaluate) {
            if (trace) System.out.println("Updated Q(S, A) for state(" + previous_state.toString() + "), action(" + action.toString() + ")");
            sav.put(action, newValue);
            Q.put(previous_state, sav);
        } else {
            if (trace) System.out.println("Evaluation mode: Did not update Q(S, A) for state(" + previous_state.toString() + "), action(" + action.toString() + ")");
        }
        
        for (RLDataObject key : Q.keySet()) {
            if (trace) System.out.println("For Q(S, ...), S= " + key.toString() + ", Q(S,A) is ");
            if (trace) System.out.println(Q.get(key).toString());
        }

        previous_action = action;
        previous_state = state;
        return metrics;
    }

    public void parseActions(List<Formula> new_actions) {
        actions = new ArrayList<>(); //Clear down previous
        for (Formula f: new_actions) {
            Predicate p = (Predicate)f;
            RLDataObject action = parseBeliefTerms(p.terms());
            actions.add(action);
        }
    }

    public void parseState(List<Formula> new_inputs) {
        if (new_inputs.size() > 1) {
            throw new LearningProcessException("Q-Learning Only handles one input belief at the moment");
        }
        for (Formula f: new_inputs) {
            Predicate p = (Predicate)f;
            state = parseBeliefTerms(p.terms());
        }
    }

    public void parseReward(List<Formula> new_reward) {
        if (new_reward.size() > 1) {
            throw new LearningProcessException("Q-Learning Only handles one reward belief at the moment");
        }
        for (Formula f: new_reward) {
            Predicate p = (Predicate)f;
            reward = parseReward(p.terms());
        }
    }

    @Override
    public boolean mergeModel(DataModel other) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'mergeModel'");
    }

    @Override
    public String print() {
        StringBuffer sb = new StringBuffer();
        sb.append("RLDataModel ");
        for (RLDataObject key: Q.keySet()) {
            sb.append("\nKey: ");
            sb.append(key.toString());
            StateActionValue sav = Q.get(key);
            sb.append("\n");
            sb.append(sav.toString());
        }
        sb.append("\nEnd Model");
        return sb.toString();
    }
}
