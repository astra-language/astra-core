package astra.learn.library;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import astra.core.Rule;
import astra.explanation.ExplanationEngine;
import astra.formula.AND;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.learn.LearningProcessException;
import astra.learn.LearningProtectedPredicates;
import astra.learn.library.RLModel.BeliefUpdateScoreTuple;
import astra.learn.library.RLModel.RLDataModel;
import astra.statement.BeliefUpdate;
import astra.term.Term;

public class QLearning extends Algorithm {

    final String EPSILON = "epsilon";
    final String ALPHA = "alpha";
    final String GAMMA = "gamma";
    protected double epsilon = 0.1; //Initialise to sensible values
    protected double alpha = 0.9;
    protected double gamma = 0.9;

    RLDataModel model;  //State:Action - value map, <Q(s,a), v>

    //Public for testing...
    public List<Formula> inputs = new ArrayList<Formula>(); // s
    public List<Formula> actions = new ArrayList<Formula>(); // A | s
    public List<Formula> reward = new ArrayList<Formula>(); // r
    
    //Algorithm-specific config
    final String PRUNE_CONTEXT = "pruneContext";
    //Controls
    public boolean pruneContext = false;

    //Algorithm-specific metrics...
    public final static String TOTAL_REWARD = "totalReward";
    public final static String TOTAL_DISCOUNTED_REWARD = "totalDiscountedReward";
    public Double totalDiscountedReward = new Double(0); //Tracks discounted reward across X total steps (a)
    public Double totalUndiscountedReward = new Double(0);  //Tracks undiscounted reward across X total steps (b)

    public Random generator = new Random(12341); //This is the random number generator for the explore/ exploit decision

    public QLearning() {
        model = new RLDataModel();
        metrics.put(TOTAL_REWARD, totalUndiscountedReward);
        metrics.put(TOTAL_DISCOUNTED_REWARD, totalDiscountedReward);
    }

    public void setConfiguration(String term, String value) throws Exception {
        if (debug) System.out.println("Setting parameter configuration for " + term);
        switch (term) {
            case PAUSE:
                pause = Boolean.parseBoolean(value);
                break;
            case EVALUATE:
                boolean newValue = Boolean.parseBoolean(value);
                if (!evaluate && newValue) { //Was false, now true
                    metrics.put(TOTAL_REWARD, 0.0);
                    metrics.put(TOTAL_DISCOUNTED_REWARD, 0.0);
                }
                evaluate = newValue;
                break;
            case LEARNING_COMPLETE:
                learningComplete = Boolean.parseBoolean(value);
                break;
            case EXPLAIN:
                explain = Boolean.parseBoolean(value);
                break;
            case DEBUG:
                debug = Boolean.parseBoolean(value);
                break;
            case PRUNE_CONTEXT:
                pruneContext = Boolean.parseBoolean(value);
                break;
            case EPSILON:
                epsilon = Double.parseDouble(value);
                break;
            case ALPHA:
                alpha = Double.parseDouble(value);
                break;
            case GAMMA:
                gamma = Double.parseDouble(value);
                break;
            default:
                throw new LearningProcessException("Could not set Q-Learning configuration for " + term);
        }
    }

    public Double getConfiguration(String term) throws Exception {
        switch (term) {
            case EPSILON:
                return epsilon;
            case ALPHA:
                return alpha;
            case GAMMA:
                return gamma;
            default:
                throw new LearningProcessException("Could not get Q-Learning configuration for " + term);
        }
    }

    public void setInputBelief(String term, List<Formula> values) throws Exception {
        if (debug) System.out.println("Setting input configuration for " + term);
        if (values.isEmpty()) {
            if (debug) System.out.println("No matching beliefs for " + term);
            return;
        }
        //Set this on the model
        switch (term) {
            case LearningProtectedPredicates.KNOWLEDGE_INPUT:
                inputs.addAll(values);
            
                if (!input_predicates.containsKey(LearningProtectedPredicates.KNOWLEDGE_INPUT)) {
                    Predicate p = (Predicate)values.get(0);
                    input_predicates.put(LearningProtectedPredicates.KNOWLEDGE_INPUT, p.predicate());
                }
                
                break;
            case LearningProtectedPredicates.KNOWLEDGE_ACTION:
                actions.addAll(values);
                if (!input_predicates.containsKey(LearningProtectedPredicates.KNOWLEDGE_ACTION)) {
                    Predicate p = (Predicate)values.get(0);
                    input_predicates.put(LearningProtectedPredicates.KNOWLEDGE_ACTION, p.predicate());
                }

                break;
            case LearningProtectedPredicates.KNOWLEDGE_REWARD:
                
                reward.addAll(values);

                if (!input_predicates.containsKey(LearningProtectedPredicates.KNOWLEDGE_REWARD)) {
                    Predicate p = (Predicate)values.get(0);
                    input_predicates.put(LearningProtectedPredicates.KNOWLEDGE_REWARD, p.predicate());
                }

                break;
            default:
                throw new LearningProcessException("Could not set Q-Learning intput belief for " + term);
        }
       
    }

    @Override
    public void setLabelBelief(String knowledgeLabel, Term[] t) {
        model.setOutputLabel(t);
        labelSet = true;
    }

    public Formula generateContext(BeliefUpdate bu) {
        Formula retval = Predicate.TRUE;
        boolean first = true;
        List<Formula> contexts = new ArrayList<>();
        contexts.addAll(inputs);
        if (pruneContext) 
            removeActionsNotInBeliefUpdate(bu);
        
        contexts.addAll(actions);
        for (Formula c : contexts) {
            if (first) {
                first = false;
                retval = c;
            } else {
                retval = new AND(retval, c);
            }
        }
        return retval;
    }

    private void removeActionsNotInBeliefUpdate(BeliefUpdate bu) {
        List<Formula> new_actions = new ArrayList<>();
        System.out.println("BU " + bu);
        Predicate buP = bu.getPredicate();
        System.out.println("BUP " + buP);
        for (Formula a: actions) {
            //Match terms
            boolean termsMatch = true;
            Predicate ap = (Predicate)a;
            System.out.println("Pred " + ap);
            for (int i=0; i<ap.terms().length ; i++) {
                if (!buP.termAt(i).equals(ap.termAt(i))) {
                    termsMatch = false;
                }
            }
            if (termsMatch) {
                new_actions.add(a);
            }
        }
        actions = new_actions;
    }

    @Override
    public DataModel getModel() {
        return model;
    }

    @Override
    public void applyLearningFunction() throws Exception {
        if (pause || learningComplete) {
            if (debug) System.out.println("Learning is paused / complete");
            return;
        }

        if (debug) System.out.println("Updating model for action just taken, to get me into this state");
        // Have reward, for S_(t-1), A_(t-1) (which led to S_t)
        model.update(inputs, actions, reward, gamma, alpha, metrics, evaluate);
       
        if (actions.isEmpty()) {
            if (debug) System.out.println("No actions.");
            inputs = new ArrayList<>();
            actions = new ArrayList<>();
            reward = new ArrayList<>();
            return;
        }
        //Have S_t
        // Set of possible A - get A_t
        if (debug) System.out.println("Getting next action for current state");
        double randomN = Math.random();
        //double randomN = generator.nextDouble();   
        BeliefUpdateScoreTuple o_t_score;
        if ((randomN <= epsilon) && !evaluate) {
            //Picks random location (explore)
            o_t_score = model.nextActionRandom();
        } else {
            //Exploit
            //Of the AVAILABLE actions, which (if any) has the highest value?
            o_t_score = model.nextActionHighestValue();
        }

        if (o_t_score == null) {
            //This is fine
            if (debug) System.out.println("No rule to add ");
            inputs = new ArrayList<>();
            actions = new ArrayList<>();
            reward = new ArrayList<>();
            return;
        }

        Formula context = generateContext(o_t_score.getBeliefUpdate());
        if (debug) System.out.println("BU Context: " + context.toString());
        
        //Clear down state, action, reward info
        inputs = new ArrayList<>();
        actions = new ArrayList<>();
        reward = new ArrayList<>();

        //Build rule
        Rule r = new Rule(event, context, o_t_score.getBeliefUpdate());
        if (debug) System.out.println("Adding rule: " + r.toString());
        agent.addOrReplaceRule(r);

        if (explain) {
            ExplanationEngine exEngine = agent.explanations();
            exEngine.addExplanations(exEngine.unitBuilder().build(r, learningProcessNamespace, o_t_score.getScore()));
        }
    }

}
