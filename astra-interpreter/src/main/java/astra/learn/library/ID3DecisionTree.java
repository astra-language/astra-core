package astra.learn.library;

import java.util.ArrayList;
import java.util.List;

import astra.core.Rule;
import astra.explanation.ExplanationEngine;
import astra.formula.AND;
import astra.formula.Comparison;
import astra.formula.Formula;
import astra.formula.LearningProcessFormula;
import astra.formula.LearningProcessFormulaAdaptor;
import astra.formula.Predicate;
import astra.learn.LearningProcessException;
import astra.learn.LearningProtectedPredicates;
import astra.learn.library.DTModel.DTDataModel;
import astra.learn.library.DTModel.TreeNode;
import astra.learn.library.RLModel.BeliefUpdateScoreTuple;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.statement.BeliefUpdate;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;
import astra.term.Variable;
import astra.type.Type;

/*
 * Actually a bit of an ID3-C4.5 hybrid, as it handles continuous numerical data AS INPUTS (ok, so maybe a grandiose claim)
 *  However it doesn't handle missing data, or do any pruning
 * 
 * ID3:
 * 
 * Input training examples X, output values Y, attributes A 
 * Create a Root node for the tree
 *   If all Y ∈ Ci, return single-node tree Root with label Ci 
 *   Else:
 *       A ← attribute from A with highest Information Gain 
 * 
 *      For each possible value vi of A:
 *           Add a new tree branch below Root for A = vi
 *           Let Xvi be the subset of X with value vi for A, and let Yvi be the corresponding subset of Y
 *           Build subtree by applying ID3 ( Xvi, Yvi)
 *
 */
public class ID3DecisionTree extends Algorithm {

    DTDataModel model;  
    ListTerm X;
    ListTerm Y;
    int numberAttributes;
    String label; 
    Type labelType;
    Formula inputKnowledge;

    final String MAX_DEPTH = "max_depth";
    int max_depth = 0;

    public ID3DecisionTree() {
        model = new DTDataModel();
    }

    /*
     * Config might be max depth, or max impurity of leaf nodes 
     * Need a way of preventing this from trying to find "pure" trees which may overfit
     */
    @Override
    public void setConfiguration(String term, String value) throws Exception {
        if (debug) System.out.println("[ID3DecisionTree] Setting config: " + term);
        switch (term) {
            case MAX_DEPTH:
                try {
                    max_depth = Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    throw new LearningProcessException("Could not set " + term + " for  ID3 Decision Tree configuration value " + value);
                }
                break;
            case PAUSE:
                pause = Boolean.parseBoolean(value);
                break;
            case EVALUATE:
                evaluate = Boolean.parseBoolean(value);
                break;
            case EXPLAIN:
                explain = Boolean.parseBoolean(value);
                break;
            case DEBUG:
                debug = Boolean.parseBoolean(value);
                break;
            case LEARNING_COMPLETE:
                learningComplete = Boolean.parseBoolean(value);
                break;
            default:
                throw new LearningProcessException("Unhandled configuration. Could not set " + term + " for  ID3 Decision Tree configuration value " + value);
            }
    }

    public Integer getConfiguration(String term) throws Exception {
        switch (term) {
            case MAX_DEPTH:
                return max_depth;
            default:
                throw new LearningProcessException("Could not get ID3 Decision Tree configuration for " + term);
        }
    }
    
    @Override
    public void setInputBelief(String term, List<Formula> values) throws Exception {
        if (debug) System.out.println("[ID3DecisionTree] Set input belief for term " + term);
        switch (term) {
            case LearningProtectedPredicates.KNOWLEDGE_TRAIN:
                for (Formula f: values) {
                    Predicate p = (Predicate)f;
                    if (!input_predicates.containsKey(LearningProtectedPredicates.KNOWLEDGE_TRAIN)) {
                        input_predicates.put(LearningProtectedPredicates.KNOWLEDGE_TRAIN, p.predicate());
                    }
                    parseInputData(p.terms()); 
                }
                
                break;
            case LearningProtectedPredicates.KNOWLEDGE_INPUT:
                //Set something that says - we're not training. But need to know the input to build the 
                // contexts for rules...
                if (values.size() > 1) {
                    throw new LearningProcessException("More than one input for ID3DecisionTree, only supports one at a time for now " + term);
                }
                if (values.size() == 0) {
                    //So get the label
                    try {
                        List<Formula> listF = agent.beliefs().list(LearningProtectedPredicates.getID(term));
                        Formula f = listF.get(0);
                        Predicate p = (Predicate)f;
                        Primitive t = (Primitive)p.getTerm(1);
                        String knowledgeInputPred = (String)t.value();
                        input_predicates.put(LearningProtectedPredicates.KNOWLEDGE_INPUT, knowledgeInputPred);
                        inputKnowledge = new Predicate(knowledgeInputPred, new Term[] {new ListTerm(new Term[]{})});
                    } catch (Exception e) {
                        throw new LearningProcessException("No input belief for ID3DecisionTree with term " + term);
                    }
                } else {
                    Formula v = values.get(0);
                    Predicate p = (Predicate)v;
                    if (!input_predicates.containsKey(LearningProtectedPredicates.KNOWLEDGE_INPUT)) {
                        input_predicates.put(LearningProtectedPredicates.KNOWLEDGE_INPUT, p.predicate());
                    }
                    if (p.terms().length != 1) {
                        throw new LearningProcessException("More than one term for input knowledge for ID3DecisionTree, must be one, a list of primitives" + term);
                    }
                    inputKnowledge = values.get(0);  //Expect this to be like sample(list, int)
                    
                }

                break;
            default:
                throw new LearningProcessException("Could not set ID3DecisionTree configuration for " + term);
        }
    }

    public void parseInputData(Term[] trainingData) {
        //Initial checks
        try {
            X = (ListTerm)trainingData[0];
            Y = (ListTerm)trainingData[1];
        } catch(Exception e){
            throw new LearningProcessException("Could not parse training data, " + e.getMessage());
        }
        //Check X
        try {
            ListTerm singleRow = (ListTerm)X.get(0);
            numberAttributes = singleRow.size();
            for (int i=0; i<numberAttributes; i++) {
                Term singleExample = singleRow.get(i);
                if (!Primitive.class.isInstance(singleExample)) {
                    throw new LearningProcessException("Expected the X dataset for the DT to be a list of a list of primitives (i.e. char/strings, boolean or numerical), like [ [1,'hi'], [2,'low'] ]");
                }
            }
        } catch (ClassCastException cce) {
            throw new LearningProcessException("Expected the X dataset for the DT to be a list of a list, like [ [1,'hi'], [2,'low'] ]");
        }
        //Check Y
        Term example = Y.get(0);
        if (!Primitive.class.isInstance(example)) {
            throw new LearningProcessException("Expected the Y dataset for the DT to be a list of primitives (i.e. strings, chars, booleans, or int/double/float/long (but it doesn't handle continous output data))");
        }
    }

    @Override
    public void setLabelBelief(String knowledgeLabel, Term[] terms) {
        if (debug) System.out.println("[ID3DecisionTree] Setting label belief  " + knowledgeLabel);
        if (terms.length == 1) {
            Term t = terms[0];
            if (t.type().equals(Type.STRING))  {
                label = Type.stringValue(t);
                labelSet = true;
            }
        }
    }

    @Override
    public DataModel getModel() {
        return model;
    }

    @Override
    public void applyLearningFunction() throws Exception {
        //1. If no KNOWLEDGE_TRAIN - just single input (KNOWLEDGE_INPUT), do nothing
        if (X == null || Y == null) {
            if (debug) System.out.println("[ID3DecisionTree] No input data, stopping applylearningfunction");
            return;
        }
        if (pause || learningComplete) {
            if (debug) System.out.println("[ID3DecisionTree] Learning process is paused / complete, not applying learning function");
            return;
        }

        if (debug) System.out.println("[ID3DecisionTree] Applying learning function");
        
        //2. Recursive tree build
        model.setAgent(agent); //Need agent reasoner
        if (debug) System.out.println("[ID3DecisionTree] Starting to build tree, max depth " + max_depth);
        try {
            model.buildTree(X,Y, null, max_depth); 
        } catch (Exception e) {
            throw new LearningProcessException("Error applying learning function: " + e.getMessage());
        }
       
        // Not sure need the if finished...
        //3. Build plans - from max depth leaf node first
        List<TreeNode> leaves = model.getLeafNodes();
        if (debug) System.out.println("[ID3DecisionTree Model] Leaf nodes: " + leaves.size()); 
        for (TreeNode leaf: leaves) {
            BeliefUpdateScoreTuple o_t_score = createBeliefUpdate(leaf);
            if (debug) System.out.println("[ID3DecisionTree Model] Belief update: " + o_t_score.getBeliefUpdate()); 
            if (debug) System.out.println("[ID3DecisionTree Model] Belief update score: " + o_t_score.getScore()); 
            Formula context = createContext(leaf);
            
            if(debug) System.out.println("[ID3DecisionTree Model] Context: " + context); 
            Rule r = new Rule(event, context, o_t_score.getBeliefUpdate());
            if (debug) System.out.println("Adding rule: " + r.toString());
            agent.addRule(r);
            
            if (explain) {
                ExplanationEngine exEngine = agent.explanations();
                exEngine.addExplanations(exEngine.unitBuilder().build(r, learningProcessNamespace, o_t_score.getScore()));
            }
        }
    }

    private BeliefUpdateScoreTuple createBeliefUpdate(TreeNode node) {
        Term[] newTerms = {node.label().clone()};
        Predicate p = new Predicate(label, newTerms);
        
        return new BeliefUpdateScoreTuple(new BeliefUpdate('+', p), node.entropy());
    }

    private Formula createContext(TreeNode treeNode) {
        List<Formula> contexts = new ArrayList<>();

        Formula retval = convertInputKnowledgeToContext();
        
        contexts = getContext(treeNode, retval, contexts);
        
        for (Formula c : contexts) {
            retval = new AND(retval, c);
        }
        return retval;
    }

    private Predicate convertInputKnowledgeToContext() {
        //This is going to be something like "sample(list, int)"
        Predicate p = (Predicate)inputKnowledge;
        Term t0 = p.getTerm(0);
        
        Predicate f = new Predicate(p.predicate(), new Term[] {
            new Variable(t0.type(), "X",false)
        });
        return f;
    }

    private List<Formula> getContext(TreeNode parent, Formula listTerm, List<Formula> contexts) {
        if (parent.formula == null) {
            return contexts;
        }
        Comparison cf = (Comparison)parent.formula;
        
        int index = parent.index();

        //What is the type?
        Term rightTerm = cf.right();
        Type rightTermType = rightTerm.type(); //E.g. right term is float or double

        String predicate = getPredicateForType(rightTermType);
        
        Predicate method = new Predicate(predicate, new Term[] {
            new Variable(Type.LIST, "X", false),
            Primitive.newPrimitive(index),
            Primitive.newPrimitive(cf.operator()),
            rightTerm
        });
        
        LearningProcessFormulaAdaptor lpta = buildFormulaAdaptor(rightTermType);
        Formula formulaTerm = new LearningProcessFormula(learningProcessNamespace, method, lpta);
        contexts.add(formulaTerm);

        TreeNode grandparent = parent.getParent();
        if (grandparent != null) {
            return getContext(grandparent, listTerm, contexts);
        }
        return contexts;
    }

    private String getPredicateForType(Type rightTermType) {
        if (rightTermType.equals(Type.BOOLEAN)) {
            return "valueAsBoolean";
        }
        if (rightTermType.equals(Type.STRING)) {
            return "valueAsString";
        }
        if (rightTermType.equals(Type.FLOAT)) {
            return "evaluateListTerm";
        }
        if (rightTermType.equals(Type.DOUBLE)) {
            return "valueAsDouble";
        }
        if (rightTermType.equals(Type.INTEGER)) {
            return "valueAsInt";
        }

        throw new UnsupportedOperationException("Unimplemented method 'getPredicateForType'");
    }

    private LearningProcessFormulaAdaptor buildFormulaAdaptor(Type rightTermType) {
        
        if (rightTermType.equals(Type.FLOAT)) {
            return new LearningProcessFormulaAdaptor() {
                public Formula invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
                    return ((astra.learn.LearningProcess) visitor.agent().getLearningProcess(learningProcessNamespace)).evaluateListTerm(
                        (astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0)),
                        (int) visitor.evaluate(predicate.getTerm(1)),
                        (java.lang.String) visitor.evaluate(predicate.getTerm(2)),
                        (float) visitor.evaluate(predicate.getTerm(3))
                );
            };
        };} 
        else if (rightTermType.equals(Type.STRING)) {
            return new LearningProcessFormulaAdaptor() {
                public Formula invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
                    return ((astra.learn.LearningProcess) visitor.agent().getLearningProcess(learningProcessNamespace)).evaluateListTerm(
                        (astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0)),
                        (int) visitor.evaluate(predicate.getTerm(1)),
                        (java.lang.String) visitor.evaluate(predicate.getTerm(2)),
                        (java.lang.String) visitor.evaluate(predicate.getTerm(3))
                );
            };
        };}
        else if (rightTermType.equals(Type.DOUBLE)) {
            return new LearningProcessFormulaAdaptor() {
                public Formula invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
                    return ((astra.learn.LearningProcess) visitor.agent().getLearningProcess(learningProcessNamespace)).evaluateListTerm(
                        (astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0)),
                        (int) visitor.evaluate(predicate.getTerm(1)),
                        (java.lang.String) visitor.evaluate(predicate.getTerm(2)),
                        (double) visitor.evaluate(predicate.getTerm(3))
                );
            };
        };}
        else if (rightTermType.equals(Type.INTEGER)) {
            return new LearningProcessFormulaAdaptor() {
                public Formula invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
                    return ((astra.learn.LearningProcess) visitor.agent().getLearningProcess(learningProcessNamespace)).evaluateListTerm(
                        (astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0)),
                        (int) visitor.evaluate(predicate.getTerm(1)),
                        (java.lang.String) visitor.evaluate(predicate.getTerm(2)),
                        (int) visitor.evaluate(predicate.getTerm(3))
                );
            };
        };}
        else if (rightTermType.equals(Type.BOOLEAN)) {
            return new LearningProcessFormulaAdaptor() {
                public Formula invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
                    return ((astra.learn.LearningProcess) visitor.agent().getLearningProcess(learningProcessNamespace)).evaluateListTerm(
                        (astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0)),
                        (int) visitor.evaluate(predicate.getTerm(1)),
                        (boolean) visitor.evaluate(predicate.getTerm(2))
                );
            };
        };}
        else {
            throw new LearningProcessException("Unsupported type " + rightTermType);
        }
        
    }

    
}
