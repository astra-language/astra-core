package astra.learn.library.DTModel;

import java.security.KeyStore.Entry;
import java.util.ArrayList;
import java.util.List;
import astra.type.Type;
import astra.formula.Formula;
import astra.term.ListTerm;
import astra.term.Term;

/*
 * What information needs encoding in the treenode?
 * 
 */
public class TreeNode {

    private int ID;

    private ListTerm examples;

    public Formula formula;

    // public Double impurity;
    
    private List<TreeNode> children;
    private TreeNode parent;
    
    //The value all the samples are supposed to have
    // Only relevant for leaf nodes
    private Term label;
    private Type labelType;
    private int attribute_index;

    private Double entropy;
    

    public TreeNode(int id, ListTerm examples, Term label, Type labelType) {
        this.ID = id;
        this.examples = examples;
        this.label = label;
        this.labelType = labelType;
        this.children = new ArrayList<>();
    }

    public TreeNode(int id, ListTerm examples) {
        this.ID = id;
        this.examples = examples;
        this.children = new ArrayList<>();
    }

    /*
     * Leaf node if no children
     */
    public boolean leaf() {
        return children.size() == 0;
    }

    public void setLabel(Term label) {
        this.label = label;
    }

    public void setEntropy(double ent) {
        this.entropy = ent;
    }

    public void setParent(TreeNode parent) {
        this.parent = parent;
    }

    public TreeNode getParent() {
        return this.parent;
    }

    public int ID() {
        return this.ID;
    }

    public Formula formula() {
        return formula;
    }

    public int depth() {
        int depth = 1;
        if (parent == null) {
            return 0;
        } else {
            depth += parent.depth();
        }
        return depth;
    }

    public List<TreeNode> children() {
        return children;
    }

    public int index() {
        return attribute_index;
    }

    public Term label() {
        return label;
    }

    public double entropy() {
        return entropy.doubleValue();
    }

    public Type labelType() {
        return labelType;
    }

    public void setFormula(Formula formula, int attribute) {
        this.attribute_index = attribute;
        this.formula = formula;
    }

    public void addChild(TreeNode child) {
        child.setParent(this);
        children.add(child);
    }

    @Override
    public String toString() {
        String retval = "ID: " + ID + " ";
        if (parent == null) {
            retval += "Root node ";
        } else {
            retval += "; Parent = " + parent.ID();
        }
        if (leaf()) {
            retval += "\n Leaf node " + label.toString();
            retval += "\n with " + examples.size() + " examples ";
        }
        retval += "\n Depth: " + depth();
        if (children().size() > 0) {
            retval += "\n with " + children.size() + " children ";
        }
        if (entropy != null) {
            retval += "\n entropy " + entropy.doubleValue();
        }
        
        if (formula != null) {
            retval += "\n Formula " + formula;
            retval += "\n   on index " + attribute_index;
        }
        return retval;
        
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TreeNode) {
            TreeNode tn = (TreeNode)o;
            if (!tn.toString().equals(this.toString())) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        int hashcode =  this.toString().hashCode();
        return hashcode;
    }
}
