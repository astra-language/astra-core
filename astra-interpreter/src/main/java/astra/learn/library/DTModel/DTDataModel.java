package astra.learn.library.DTModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import astra.core.Agent;
import astra.formula.Comparison;
import astra.formula.Formula;
import astra.learn.LearningProcessException;
import astra.learn.LearningProtectedPredicates;
import astra.learn.library.DataModel;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;
import astra.term.Variable;
import astra.type.Type;

public class DTDataModel extends DataModel {

    Object trainingData;
    boolean debug = false;
    String label;
    TreeNode root;
    Agent agent; //Need agent reasoner
    List<TreeNode> leafNodes;
    int depth = -1;
    int IDCOUNT = 0;

    //Important - this is a cache.
    // Assume knowledge_input(somepredicate(x,y)) 
    // When the algorithm splits features up, we want to remember te splits
    // and save them by index-of-feature, to the criteria of the split
    // always using some variable term, for example for a binary split the formula is going to be
    //
    // learningContextBuilder.booleanValueFor(x, featureToSplitOn) == true
    private HashMap<Integer, List<Formula>> featureSplitFormulas = new HashMap<>();

    public void setAgent(Agent a) {
        this.agent = a;
    }

    public void addInput(String term, Term[] input) {
        if (debug) System.out.println("[DTDataModel] Add inoput for term " + term);
        switch (term) {
            case LearningProtectedPredicates.KNOWLEDGE_TRAIN:
                trainingData = parseTrainingData(input);
                break;
            case LearningProtectedPredicates.KNOWLEDGE_INPUT:
                //Don't handle this
                break;
            case LearningProtectedPredicates.KNOWLEDGE_LABEL:
                if (debug) System.out.println("[DTDataModel] Add inoput for term " + input);
                label = parseOutputLabel(input);
                break;
            default:
                throw new LearningProcessException("Could not set ID3 configuration for " + term);
        }
    }

    private Object parseTrainingData(Term[] input) {
        //First term is input
        //Second term is output
        return null;
    }

    public String parseOutputLabel(Term[] terms) {
       
        if (terms.length == 1) {
            Term t = terms[0];
            if (t.type().equals(Type.STRING))  {
                return Type.stringValue(t);
            }
        }
        throw new LearningProcessException("Given input for the knowledge_label belief in RL process is incorrect: it should have one term, of type string");
    }

    public TreeNode getRoot() {
        return root;
    }

    public List<TreeNode> getLeafNodes() {
        List<TreeNode> leaves = new ArrayList<>();
        leaves = getLeafNodesRecursive(leaves, getRoot());
        return leaves;
    }

    public List<TreeNode> getLeafNodesRecursive(List<TreeNode> leaves, TreeNode currentNode) {
        if (currentNode.leaf()) {
            leaves.add(currentNode);
        } else {
            for (Object child: currentNode.children()) {
                getLeafNodesRecursive(leaves, (TreeNode)child);
            }
        }   
        return leaves;
    }

    public void buildTree(ListTerm X, ListTerm Y, TreeNode currentNode, int max_depth) {
        //Clear this in between
        featureSplitFormulas = new HashMap<>();

        if (debug) System.out.println("[ID3DecisionTree Model] Building tree for size " + Y.size());
        if (X.isEmpty()) {
            if (debug) System.out.println("[ID3DecisionTree Model] X is empty, return");
            return; //?
        }

        if (root == null && currentNode == null) {
            if (debug) System.out.println("[ID3DecisionTree Model] Adding root node");
            root = new TreeNode(IDCOUNT, X);
            IDCOUNT++;
            currentNode = root;
        } else {
            //Don't need to do this for root node
            //if (debug) System.out.println("[ID3DecisionTree Model] Checking entropy");
            depth = currentNode.depth();
    
            Term listTerm = Y.get(0);
            Type labelType = listTerm.type(); 

            if (entropy(X,Y) == 0) {
                if (debug) System.out.println("[ID3DecisionTree Model] Entropy of 0 for node with label " + Y.get(0));
                //Leaf node with label for first example (as they are the same)
                currentNode.setLabel(listTerm); 
                currentNode.setEntropy(0.0);
                return;
            }
            if (depth == max_depth) {
               
                //Leaf node with label for first example (as they are the same)
                double ent = entropy(X,Y);
                Term label = mostFrequentLabel(Y);
                currentNode.setLabel(label); 
                currentNode.setEntropy(ent);

                if (debug) System.out.println("[ID3DecisionTree Model] Reached max depth, for node with label " + label.toString() + ", entropy " + ent);

                return;
            }
        }

        ListTerm t = (ListTerm)X.get(0);
        int numberAttributes = t.size();
        
        //if (debug) System.out.println("[ID3DecisionTree Model] Number of attributes " + numberAttributes); 
        //How many features are in X?
        //Get the first one
        double highestInformationGain = 0;
        int featureToSplitOn = -1;
        for (int i = 0; i<numberAttributes; i++) {
            double ig = informationGain(X, Y, i);
            if (ig >= highestInformationGain) {
                highestInformationGain = ig;
                featureToSplitOn = i;
            }
        }
        
        //if (debug) System.out.println("[ID3DecisionTree Model] Feature to split on " + featureToSplitOn); 
        
        /* Now have feature to split on,
        * For each possible value vi of A:
        *           Add a new tree branch below Root for A = vi
        *           Let Xvi be the subset of X with value vi for A, and let Yvi be the corresponding subset of Y
        *           Build subtree by applying ID3 ( Xvi, Yvi, A − A) */
        List<Formula> splits = featureSplitFormulas.get(featureToSplitOn);
        for (Formula f: splits) {
            //1. Create new node per split
            ListTerm subListX = new ListTerm();
            ListTerm subListY = new ListTerm();
            for (int i = 0 ; i< X.size(); i++) {
                ListTerm row = (ListTerm)X.get(i);
                //Replace first term in formula with p
                Comparison comp = (Comparison)f;
                Comparison newComp = new Comparison(comp.operator(), (Primitive)row.get(featureToSplitOn), comp.right());
                //if (debug) System.out.println("[ID3DecisionTree Model] Comparing " + newComp.toString()); 
                HashMap<Integer, Term> bindings = new HashMap<>();
                List<Map<Integer, Term>> retval = agent.query(newComp, bindings);
                if (retval != null) {
                    subListX.add(X.get(i));
                    subListY.add(Y.get(i));
                }
            }
            TreeNode child = new TreeNode(IDCOUNT, subListX);
            IDCOUNT++;
            child.setFormula(f, featureToSplitOn);
            currentNode.addChild(child);
            buildTree(subListX, subListY, child, max_depth);
        }
    }

    /*
     * H(X) = − ∑ pi log2 pi
     * 
     * Entropy is a measure of impurity or uncertainty in a set, i.e. if a set contains
     * training examples with different labels it will have high impurity or entropy. 
     * In a set of examples X ∈ X with output values {C1, ..., Cc}, 
     * the entropy operates on the relative frequency (probability) of class Ci - pi
     *
     */
    public double entropy(ListTerm X, ListTerm Y) {
       
        int totalCount = Y.size();
        //if (debug) System.out.println("[ID3DecisionTree Model] Entropy for " + totalCount);
        if (totalCount == 0) {
            return 0.0; //Empty set? Purest of the pure
        }
        //For Y, what are the distinct values?
        //Get the first element to get the class
        Map<Object, Integer> uniquePrimitiveCount = new HashMap<>();
        
        for (Term t: Y) {
            Primitive p = (Primitive)t;
            Integer count = uniquePrimitiveCount.get(p.value());
            if (count == null) {
                count = 1;
            } else {
                count = count + 1;
            }
            uniquePrimitiveCount.put(p.value(), count);
        }
        //if (debug) System.out.println("[ID3DecisionTree Model] Number of types for Y " + totalCount);

        double h_s = 0.0;
        for (Object key: uniquePrimitiveCount.keySet()) {
            double frequency = (double)uniquePrimitiveCount.get(key) / totalCount;
            if (frequency == 0.0) {
                continue; //Then it will be h_s += 0.0, but with a NaN exception when try to divide log(0) by log(2)
            } else {
                double innerEnt = frequency * log2(frequency);
                innerEnt = -(innerEnt);
                h_s += innerEnt;
            }
        }

        return h_s;
    }

    public Term mostFrequentLabel(ListTerm Y) {
       
        Map<Object, Integer> uniquePrimitiveCount = new HashMap<>();
        Map<Object, Term> objectTermMap = new HashMap<>();
        
        for (Term t: Y) {
            Primitive p = (Primitive)t;
            Integer count = uniquePrimitiveCount.get(p.value());
            if (count == null) {
                count = 1;
            } else {
                count = count + 1;
            }
            uniquePrimitiveCount.put(p.value(), count);
            objectTermMap.put(p.value(), t);
        }

        Term label = null;
        int hightestCount = 0;
        for (Entry<Object, Integer> entrySet: uniquePrimitiveCount.entrySet()) {
            if (entrySet.getValue() > hightestCount) {
                hightestCount = entrySet.getValue();
                Object valuObject = entrySet.getKey();
                label = objectTermMap.get(valuObject);
            }
        }
        return label;
    }


    public static double log2(double n) {
        // calculate log2 N indirectly
        // using log() method
        return (Math.log(n) / Math.log(2));
    }
 
    /*
     * Information Gain measures the difference between the original entropy, and the 
     * entropy after a split case on some feature
     * For feature $A$, it splits a set of examples $X$ into $\{X_1,...,X_m\}$:

        IG(X,A) =  H(X) - \sum_{i=1}^m \abs*{\frac{X_i}{X}} H(X_i)

     * Inputs: 
     *         X: input dataset, size n(rows) x m(features)
     *         Y: labels, size n(rows) x 1
     *         index of attribute: indicates the index of the feature in x, i, where 0<=i<m
     * 
     * CACHE: one the algorithm has the best index to split on, if it is a numeric feature, 
     * need to remember what the best split is
     */
    public double informationGain(ListTerm X, ListTerm Y, int indexOfAttribute) {
        //For the attribute specified by indexOfAttribute, find the best split
        // If it is categorical, and there are c categories,
        // this is going to be a split into c groups, and a measure of how pure the groups are
        // If it is boolean, two groups (...and a measure of how pure the groups are)
        // The challenge is numerical values...

        //1. Check the type of information in the column (attribute...)
        //Get column, get data from column
        ListTerm singleRow = (ListTerm)X.get(0);
        Term singleExample = singleRow.get(indexOfAttribute);
        Primitive p = (Primitive)singleExample;
        
        //Or for binary, just entropy after split
        double entropyAfterBestSplit = 0.0;

        if (p.type().equals(Type.STRING) 
            || p.type().equals(Type.CHAR)) {
            entropyAfterBestSplit = entropyAfterSplitCategory(X,Y,indexOfAttribute);
        }
        else if (p.type().equals(Type.DOUBLE) 
                || p.type().equals(Type.INTEGER)
                || p.type().equals(Type.LONG)) {
            //Numeric. Aka, the difficult one
            //Cache the best split model
            // IF they are numerical, start with the second variable, and see if
            entropyAfterBestSplit = entropyAfterBestSplitContinousDouble(X,Y,indexOfAttribute);
        } else if (p.type().equals(Type.FLOAT)) {
            //Numeric. Aka, the difficult one
            //Cache the best split model
            // IF they are numerical, start with the second variable, and see if
            entropyAfterBestSplit = entropyAfterBestSplitContinousFloat(X,Y,indexOfAttribute);
        } else if (p.type().equals(Type.BOOLEAN)) {
            //Binary split
            entropyAfterBestSplit = entropyAfterSplitCategory(X,Y,indexOfAttribute);
        } else {
            throw new LearningProcessException("Expected the X dataset for the DT to be a list of a list of primitives (i.e. char/strings, boolean or numerical), like [ [1,'hi'], [2,'low'] ]");
        }

        double informationGain = entropy(X,Y) - entropyAfterBestSplit;
        return informationGain;
    }

    public double entropyAfterSplitCategory(ListTerm X, ListTerm Y, int indexOfAttribute) {
        Map<Object, List<Integer>> rowIndiciesForCategorySplit = new HashMap<>();

        for (int i = 0; i<X.size(); i++) {
            ListTerm dataRow = (ListTerm)X.get(i);
            Primitive p = (Primitive)dataRow.get(indexOfAttribute);
            //boolean categroy = (boolean)p.value();
            List<Integer> list = rowIndiciesForCategorySplit.get(p.value());
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(i);
            rowIndiciesForCategorySplit.put(p.value(), list);
        }

        createEqualsFormulas(rowIndiciesForCategorySplit.keySet(), indexOfAttribute);

        float totalSize = Y.size();
        double entropyAfterSplit = 0.0;
        for (Object key : rowIndiciesForCategorySplit.keySet()) {
            List<Integer> indexes = rowIndiciesForCategorySplit.get(key);
            ListTerm subCatX = buildSubList(X, indexes);
            ListTerm subCatY = buildSubList(Y, indexes);
            //Weight according to size
            float subCatSize = subCatY.size();
            float weight = Math.abs(subCatSize/totalSize);
            double subEnt = weight * entropy(subCatX, subCatY);
            entropyAfterSplit += subEnt;
        }
        return entropyAfterSplit;
    }

    private ListTerm buildSubList(ListTerm list, List<Integer> indexes) {
        int sizeSubList = indexes.size();
        Term[] subList = new Term[sizeSubList];
        for (int i = 0; i<sizeSubList; i++) {
            subList[i] = list.get(indexes.get(i));
        }
        return new ListTerm(subList);
    }

    private void createEqualsFormulas(Set<Object> keys, int index) {
        List<Formula> listF = featureSplitFormulas.get(index);
        if (listF == null) {
            listF = new ArrayList<>();
        }
        
        for (Object o: keys) {
            Formula f = new Comparison("==",
                new Variable(Type.BOOLEAN, "X"), 
                Primitive.newPrimitive(o)
            );
            listF.add(f);                
        }
        featureSplitFormulas.put(index, listF);
    }

    /*
     * For now this splits two ways - it works out the highest value in the data, and the lowest
     * Then it groups entries by >= midway point, and <  midway point
     */
    public double entropyAfterBestSplitContinousDouble(ListTerm X, ListTerm Y, int indexOfAttribute) {
        double max = -1;
        double min = -1;
        Map<Object, List<Integer>> rowIndiciesForSplit = new HashMap<>();

        for (int i = 0; i<X.size(); i++) {
            ListTerm dataRow = (ListTerm)X.get(i);
            Primitive p = (Primitive)dataRow.get(indexOfAttribute);
            
            double value = (double)p.value();
            if (i == 0) {
                min = value;
                max = value;
                continue;
            }

            if (value < min) {
                min = value;
            }
            if (value > max) {
                max = value;
            }
        }
        double halfWayPoint = min + ((max - min) / 2);
        createContinuousFormulasDouble(halfWayPoint, indexOfAttribute);
        
        for (int i = 0; i<X.size(); i++) {
            ListTerm dataRow = (ListTerm)X.get(i);
            Primitive p = (Primitive)dataRow.get(indexOfAttribute);
            
            double value = (double)p.value();
            int group = 0; //0 or 1
            if (value >= halfWayPoint) {
                group = 1;
            }
            List<Integer> list = rowIndiciesForSplit.get(group);
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(i);
            rowIndiciesForSplit.put(group, list);
        }

        //Try splitting on the mid way point
        float totalSize = Y.size();
        double entropyAfterSplit = 0.0;
        for (Object key : rowIndiciesForSplit.keySet()) {
            List<Integer> indexes = rowIndiciesForSplit.get(key);
            ListTerm subCatX = buildSubList(X, indexes);
            ListTerm subCatY = buildSubList(Y, indexes);
            //Weight according to size
            float subCatSize = subCatY.size();
            float weight = Math.abs(subCatSize/totalSize);
            double subEnt = weight * entropy(subCatX, subCatY);
            entropyAfterSplit += subEnt;
        }
        return entropyAfterSplit;
    }

    /*
     * For now this splits two ways - it works out the highest value in the data, and the lowest
     * Then it groups entries by >= midway point, and <  midway point
     */
    public double entropyAfterBestSplitContinousFloat(ListTerm X, ListTerm Y, int indexOfAttribute) {
        float max = -1f;
        float min = -1f;
        Map<Object, List<Integer>> rowIndiciesForSplit = new HashMap<>();

        for (int i = 0; i<X.size(); i++) {
            ListTerm dataRow = (ListTerm)X.get(i);
            Primitive p = (Primitive)dataRow.get(indexOfAttribute);
            
            float value = (float)p.value();
            if (i == 0) {
                min = value;
                max = value;
                continue;
            }

            if (value < min) {
                min = value;
            }
            if (value > max) {
                max = value;
            }
        }
        float halfWayPoint = min + ((max - min) / 2);
        createContinuousFormulasFloat(halfWayPoint, indexOfAttribute);
        
        for (int i = 0; i<X.size(); i++) {
            ListTerm dataRow = (ListTerm)X.get(i);
            Primitive p = (Primitive)dataRow.get(indexOfAttribute);
            
            float value = (float)p.value();
            int group = 0; //0 or 1
            if (value >= halfWayPoint) {
                group = 1;
            }
            List<Integer> list = rowIndiciesForSplit.get(group);
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(i);
            rowIndiciesForSplit.put(group, list);
        }

        //Try splitting on the mid way point
        float totalSize = Y.size();
        double entropyAfterSplit = 0.0;
        for (Object key : rowIndiciesForSplit.keySet()) {
            List<Integer> indexes = rowIndiciesForSplit.get(key);
            ListTerm subCatX = buildSubList(X, indexes);
            ListTerm subCatY = buildSubList(Y, indexes);
            //Weight according to size
            float subCatSize = subCatY.size();
            float weight = Math.abs(subCatSize/totalSize);
            double subEnt = weight * entropy(subCatX, subCatY);
            entropyAfterSplit += subEnt;
        }
        return entropyAfterSplit;
    }

    private void createContinuousFormulasDouble(double halfWayPoint, int index) {
        List<Formula> listF = featureSplitFormulas.get(index);
        if (listF == null) {
            listF = new ArrayList<>();
        }

        Formula f1 = new Comparison(">=",
            new Variable(Type.DOUBLE, "X"), 
            Primitive.newPrimitive(halfWayPoint)
        );
        Formula f2 = new Comparison("<",
            new Variable(Type.DOUBLE, "X"), 
            Primitive.newPrimitive(halfWayPoint)
        );
        listF.add(f1);          
        listF.add(f2);                
        featureSplitFormulas.put(index, listF);
    }

    private void createContinuousFormulasFloat(float halfWayPoint, int index) {
        List<Formula> listF = featureSplitFormulas.get(index);
        if (listF == null) {
            listF = new ArrayList<>();
        }

        Formula f1 = new Comparison(">=",
            new Variable(Type.FLOAT, "X"), 
            Primitive.newPrimitive(halfWayPoint)
        );
        Formula f2 = new Comparison("<",
            new Variable(Type.FLOAT, "X"), 
            Primitive.newPrimitive(halfWayPoint)
        );
        listF.add(f1);          
        listF.add(f2);                
        featureSplitFormulas.put(index, listF);
    }

    @Override
    public boolean mergeModel(DataModel other) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'mergeModel'");
    }

    /*
     * root - A, B
     * 
     * A - C, D
     * 
     * B - E, F
     * 
     */
    @Override
    public String print() {
        String retval = "";
        List<TreeNode> allNodes = flatten();
        boolean first = true;
        for (TreeNode n: allNodes) {
            if (first) {
                first = false;
            } else {
                retval += "\n";
            }
            retval += printNode(n);
        }
        return retval;
    }

    private List<TreeNode> flatten() {
        List<TreeNode> flatList = new ArrayList<>();
        flatList.add(root);
        recursiveAdd(flatList, root);
        return flatList; 
    }

    private List<TreeNode> recursiveAdd(List<TreeNode> list, TreeNode current) {
        for (Object child : current.children()) {
            TreeNode childNode = (TreeNode)child;
            if (!list.contains(childNode)) {
                list.add(childNode);
            }
        }
        //Now they are added, for each child, get their children
        for (Object child : current.children()) {
            TreeNode childNode = (TreeNode)child;
            recursiveAdd(list, childNode);
        }
        return list;
    }

    private String printNode(TreeNode node) {
        String sb = "\n*************************** ";
        sb += "\n" + node.toString();
        sb += "\n*************************** ";
        return sb;
    }
    
}

