package astra.learn.library;

import astra.term.Funct;

public class AlgorithmParameter {
    
    Funct parameter;

    public AlgorithmParameter(Funct parameter) {
        this.parameter = parameter;
    }

    public String getFunctor() {
        return parameter.functor();
    }
}
