package astra.learn.library;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import astra.core.Rule;
import astra.explanation.ExplanationEngine;
import astra.formula.AND;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.learn.LearningProcessException;
import astra.learn.LearningProtectedPredicates;
import astra.learn.library.RLModel.BeliefUpdateScoreTuple;
import astra.learn.library.TrustModel.BasicTrustModel;
import astra.statement.BeliefUpdate;
import astra.term.Term;

public class TrustAlgorithm extends Algorithm {

    private boolean trace = false;

    final String EPSILON = "epsilon";
    private double epsilon = 0.1; //Initialise to sensible values

    BasicTrustModel model;  //State:Action - value map, <Q(s,a), v>

    //Public for testing...
    public List<Formula> actions = new ArrayList<Formula>(); // A | s
    public List<Formula> reward = new ArrayList<Formula>(); // r
    
    public Random generator = new Random(12341); //This is the random number generator for the explore/ exploit decision

    public TrustAlgorithm() {
        model = new BasicTrustModel();
    }

    public void setConfiguration(String term, String value) throws Exception {
       // if (trace) System.out.println("Setting parameter configuration for " + term);
        switch (term) {
            case PAUSE:
                pause = Boolean.parseBoolean(value);
                break;
            case EVALUATE:
                boolean newValue = Boolean.parseBoolean(value);
                evaluate = newValue;
                break;
            case LEARNING_COMPLETE:
                learningComplete = Boolean.parseBoolean(value);
                break;
            case EPSILON:
                epsilon = Double.parseDouble(value);
                break;
            case EXPLAIN:
                explain = Boolean.parseBoolean(value);
                break;
            default:
                throw new LearningProcessException("Could not set TrustAlgorithm configuration for " + term);
        }
    }

    public Double getConfiguration(String term) throws Exception {
        switch (term) {
            default:
                throw new LearningProcessException("Could not get configuration for " + term);
        }
    }

    public void setInputBelief(String term, List<Formula> values) throws Exception {
        //if (trace) System.out.println("Setting input configuration for " + term);
        if (values.isEmpty()) {
            if (trace) System.out.println("No matching beliefs for " + term);
            return;
        }
        //Set this on the model
        switch (term) {
            case LearningProtectedPredicates.KNOWLEDGE_INPUT:
                //Do nothing
                break;
            case LearningProtectedPredicates.KNOWLEDGE_ACTION:
                actions.addAll(values);
                break;
            case LearningProtectedPredicates.KNOWLEDGE_REWARD:
                reward.addAll(values);
                break;
            default:
                throw new LearningProcessException("Could not set Trust intput belief for " + term);
        }
       
    }

    @Override
    public void setLabelBelief(String knowledgeLabel, Term[] t) {
        model.setOutputLabel(t);
        labelSet = true;
    }

    @Override
    public DataModel getModel() {
        return model;
    }

    @Override
    public void applyLearningFunction() throws Exception {
        if (pause || learningComplete) {
            if (trace) System.out.println("Learning is paused / complete");
            return;
        }

        model.update(actions, reward);

        if (actions.size() == 0) {
            if (trace) System.out.println("No actions, return");
            return;
        }
        
        if (trace) System.out.println("Getting next action");
        double randomN = Math.random();
        //double randomN = generator.nextDouble();   
        BeliefUpdateScoreTuple o_t_score;
        if ((randomN <= epsilon) && !evaluate) {
            //Picks random location (explore)
            o_t_score = model.nextActionRandom();
        } else {
            //Exploit
            //Of the AVAILABLE actions, which (if any) has the highest value?
            o_t_score = model.nextActionHighestValue();
        }

        if (o_t_score == null) {
            //This is fine
            if (trace) System.out.println("No rule to add ");
            actions = new ArrayList<>();
            reward = new ArrayList<>();
            return;
        }

        //if (trace) System.out.println("BU is " + o_t.toString());
        Formula context = generateContext(o_t_score.getBeliefUpdate());
        //if (trace) System.out.println("Context: " + context.toString());
        
        //Clear down state, action, reward info
        actions = new ArrayList<>();
        reward = new ArrayList<>();

        //Build rule
        Rule r = new Rule(event, context, o_t_score.getBeliefUpdate());
        if (trace) System.out.println("Adding rule: " + r.toString());
        agent.addOrReplaceRule(r);

        if (explain) {
            ExplanationEngine exEngine = agent.explanations();
            exEngine.addExplanations(exEngine.unitBuilder().build(r, learningProcessNamespace, o_t_score.getScore()));
        }
    }

    public Formula generateContext(BeliefUpdate bu) {
        Formula retval = Predicate.TRUE;
        boolean first = true;
        List<Formula> contexts = new ArrayList<>();
        contexts.addAll(actions);
        for (Formula c : contexts) {
            if (first) {
                first = false;
                retval = c;
            } else {
                retval = new AND(retval, c);
            }
        }
        return retval;
    }

}
