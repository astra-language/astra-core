package astra.learn.library.TrustModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import astra.formula.Formula;
import astra.formula.Predicate;
import astra.learn.library.DataModel;
import astra.learn.library.RLModel.BeliefUpdateScoreTuple;
import astra.learn.library.RLModel.RLDataObject;
import astra.term.Term;

import java.util.Random;

import astra.learn.LearningProcessException;

import astra.statement.BeliefUpdate;
import astra.type.Type;


public class BasicTrustModel extends DataModel {

    public Random generator = new Random();
    
    private List<RLDataObject> actions; 
    private RLDataObject action; //A_t, when chosen
    private String next_action; //Predicate to set A_t
    private RLDataObject previous_action; // A_(t-1)

    private double reward; // R_(t-1)
    private boolean trace = false;

    private HashMap<RLDataObject, Double> actionValueMap;

    public BasicTrustModel() {
        actionValueMap = new HashMap<>();
    }

    public RLDataObject parseBeliefTerms(Term[] terms) {
        //Convert to RLDataModel
        return new RLDataObject(terms);
    }

    public void setOutputLabel(Term[] terms) {
       if (terms.length == 1) {
            Term t = terms[0];
            if (t.type().equals(Type.STRING))  {
                next_action = Type.stringValue(t);
            }
        } else {
            throw new LearningProcessException("Given input for the knowledge_label belief in BasicTrustModel process is incorrect: it should have one term, of type string");
        }
    }

    public double parseReward(Term[] terms) {
        if (terms.length == 1) {
            Term t = terms[0];
            if (t.type().equals(Type.FLOAT) || t.type().equals(Type.INTEGER) || t.type().equals(Type.DOUBLE) || t.type().equals(Type.LONG))  {
                return Type.doubleValue(t);
            }
        }
        throw new LearningProcessException("Given input for the knowledge_reward belief in RL process is incorrect: it should have one term, of type float, int, double or long");
    }

    public BeliefUpdateScoreTuple nextActionRandom() {
        if (trace) System.out.println("Exploring");
        //Get from actions
        if (trace) System.out.println("Actions size is " + actions.size());
        if (actions.size() < 1) {
            return null;
        }
        int random_action = (int)(Math.random() * actions.size());
        //int random_action = generator.nextInt(actions.size());
        //Collections.shuffle(actions); //Help the randomness
        //Set the action
        action = actions.get(random_action);
        if (trace) System.out.println("Randomly selected action is " + action.toString());
        //Return a belief update for that action

        Double score = actionValueMap.get(action);
        return createBeliefUpdateForAction(score);
    }

    private BeliefUpdateScoreTuple createBeliefUpdateForAction(Double score) {
        Term[] newTerms = action.getTerms().clone();
        Predicate p = new Predicate(next_action, newTerms);
        if (score == null) {
            score = new Double(0.0);
        }
        return new BeliefUpdateScoreTuple(new BeliefUpdate('+', p), score);
    }
    
    public BeliefUpdateScoreTuple nextActionHighestValue() {
        if (trace) System.out.println("Exploiting");
        
        RLDataObject highestValueAction = getHighestValuedMatchingAction(actions);
            
        if (highestValueAction != null) {
            action = highestValueAction;
            Double score = actionValueMap.get(action);
            return createBeliefUpdateForAction(score);
        }
        if (trace) System.out.println("Don't have a highest valued action");
           
        return nextActionRandom();
    }

    public RLDataObject getHighestValuedMatchingAction(List<RLDataObject> nextPossibleActions) {
        Double highest = null;
        int size = nextPossibleActions.size();
        Double[] values = new Double[nextPossibleActions.size()];
        int highestValuePosition = 0;
        for (int i = 0; i<size; i++) {
            Double value = actionValueMap.get(nextPossibleActions.get(i));
            if (value != null) {
                values[i] = value;
                //Have to do >= because it may have equally valued actions
                if (highest == null || (value >= highest)) {
                    highest = value;
                    highestValuePosition = i;
                }
            }
        }
        if (highest == null) {
            return null;
        }
        //CHECK - for highest value, is there another equal one?
        for (int i = 0; i<size; i++) {
            if (i != highestValuePosition) {
                Double otherValue = values[i];
                if ((otherValue != null) && (Double.compare(highest, values[i]) == 0)) {
                        //Equal
                        return null;
                } 
            }  
        }
        return nextPossibleActions.get(highestValuePosition);
    }

    public void clearActions() {
        actions = new ArrayList<>(); //Clear actions at end
    }

    public void update(List<Formula> new_actions, List<Formula> new_reward) {
        
        parseActions(new_actions);
        parseReward(new_reward);
        
        if (previous_action == null) {
            //This is the first iteration of the learning process
            previous_action = action;
            return;
        }

        if (trace) System.out.println("Updating for previous action " + previous_action.toString());
        // Q(S,A)
        Double previousValue = actionValueMap.get(previous_action);
        if (previousValue == null) {
            previousValue = new Double(0.0);
        }

        //max Q(S',a) is the maximum score for moving from the current location, to another one.
        Double updatedValue = previousValue + reward;
        actionValueMap.put(previous_action, updatedValue);

        // Update Q(S,A) - UNLESS IN EVALUATION MODE
        if (trace) System.out.println("After update, new value is " + updatedValue);

        previous_action = action;
    }

    private void parseActions(List<Formula> new_actions) {
        actions = new ArrayList<>(); //Clear down previous
        for (Formula f: new_actions) {
            Predicate p = (Predicate)f;
            RLDataObject action = parseBeliefTerms(p.terms());
            actions.add(action);
        }
    }

    private void parseReward(List<Formula> new_reward) {
        if (new_reward.size() > 1) {
            throw new LearningProcessException("BasicTrustModel Only handles one reward belief at the moment");
        }
        for (Formula f: new_reward) {
            Predicate p = (Predicate)f;
            reward = parseReward(p.terms());
        }
    }

    @Override
    public boolean mergeModel(DataModel other) {
        if (other instanceof BasicTrustModel) {
            BasicTrustModel otherBT = (BasicTrustModel)other;
            
        }
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'mergeModel'");
    }

    @Override
    public String print() {
        StringBuffer sb = new StringBuffer();
        sb.append("BasicTrustModel ");
        for (RLDataObject key: actionValueMap.keySet()) {
            sb.append("\nKey: ");
            sb.append(key.toString());
           
            sb.append("\nValue:");
            sb.append(actionValueMap.get(key));
        }
        sb.append("\nEnd Model");
        return sb.toString();
    }
}
