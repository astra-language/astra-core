package astra.learn.library;

import astra.learn.LearningProcessException;
import astra.term.Term;
import astra.type.Type;

public abstract class DataModel {
    
    public String parseOutputLabel(Term[] terms) {
        if (terms.length == 1) {
            Term t = terms[0];
            if (t.type().equals(Type.STRING))  {
                return Type.stringValue(t);
            }
        }
        throw new LearningProcessException("Given input for the knowledge_label belief in RL process is incorrect: it should have one term, of type string");
    }

    public abstract boolean mergeModel(DataModel other);
    public abstract String print();

    @Override
    public String toString() {
        return print();
    }
}
