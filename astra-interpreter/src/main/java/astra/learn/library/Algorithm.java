package astra.learn.library;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import astra.core.Agent;
import astra.event.Event;
import astra.formula.Formula;
import astra.term.Term;

public abstract class Algorithm {
    
    public Algorithm() {
    }

    //Universal beliefs
    final String LEARNING_COMPLETE = "finish";
    final String PAUSE = "pause";
    final String EVALUATE = "evaluate";
    final String EXPLAIN = "explain";
    final String DEBUG = "debug";

    //Controls
    public boolean learningComplete = false;
    public boolean pause = false;
    public boolean evaluate = false;
    public boolean explain = false;
    public boolean debug = false;

    protected String learningProcessNamespace;
    public DataModel model;
    protected Event event;
    protected Agent agent;
    public HashMap<String, String> input_predicates; //Type to Predicate
    public boolean labelSet;
    public Map<String, Double> metrics = new HashMap<>();

    public void initialize(Agent agent, Event event, String learningProcessNamespace) {
        this.agent = agent;
        this.event = event;
        this.input_predicates = new HashMap<>();
        this.labelSet = false;
        this.learningProcessNamespace = learningProcessNamespace;
    }

    public abstract void setConfiguration(String term, String value) throws Exception; //This should handle pause, evaluate, learning complete
    public abstract Object getConfiguration(String term) throws Exception;
    public abstract void setInputBelief(String term, List<Formula> values) throws Exception;
    public abstract DataModel getModel();
    public abstract void applyLearningFunction() throws Exception;

    public void setLabelBelief(String knowledgeLabel, Term[] t) {
        labelSet = true;
    }
}
