package astra.learn.library;

import java.util.ArrayList;

import astra.core.Rule;
import astra.explanation.ExplanationEngine;
import astra.formula.Formula;
import astra.learn.library.RLModel.BeliefUpdateScoreTuple;
import astra.learn.library.RLModel.RLDataModelSARSA;

public class SARSA extends QLearning {
    
    public SARSA() {
        model = new RLDataModelSARSA();
        metrics.put(TOTAL_REWARD, totalUndiscountedReward);
        metrics.put(TOTAL_DISCOUNTED_REWARD, totalDiscountedReward);
    }

    @Override
    public void applyLearningFunction() throws Exception {
        if (pause || learningComplete) {
            if (debug) System.out.println("Learning is paused / complete");
            return;
        }

        if (actions.isEmpty()) {
            if (debug) System.out.println("No actions.");
            inputs = new ArrayList<>();
            actions = new ArrayList<>();
            reward = new ArrayList<>();
            return;
        }
        //Have S_t
        // Set of possible A - get A_t
        if (debug) System.out.println("Getting next action for current state");
        double randomN = Math.random();
        //double randomN = generator.nextDouble();   
        BeliefUpdateScoreTuple o_t_score;
        if (debug) System.out.println("epsilon: " + epsilon);
        if (debug) System.out.println("evaluate: " + evaluate);
        if (debug) System.out.println("model: " + model);

        //For Q-Learning, this happens when model.update is called
        // but for SARSA, we need to choose the next action before updating the model
        model.parseActions(actions);
        model.parseState(inputs);
        model.parseReward(reward);

        if ((randomN <= epsilon) && !evaluate) {
            //Picks random location (explore)
            o_t_score = model.nextActionRandom();
        } else {
            //Exploit
            //Of the AVAILABLE actions, which (if any) has the highest value?
            o_t_score = model.nextActionHighestValue();
        }

        // Have reward, for S_(t-1), A_(t-1) (which led to S_t)
        // As this is SARSA, we need A_t, so the update happens after the next
        // action is chosen
        model.update(inputs, actions, reward, gamma, alpha, metrics, evaluate);

        if (o_t_score == null) {
            //This is fine
            if (debug) System.out.println("No rule to add ");
            inputs = new ArrayList<>();
            actions = new ArrayList<>();
            reward = new ArrayList<>();
            return;
        }

        Formula context = generateContext(o_t_score.getBeliefUpdate());
        if (debug) System.out.println("BU Context: " + context.toString());
        
        //Clear down state, action, reward info
        inputs = new ArrayList<>();
        actions = new ArrayList<>();
        reward = new ArrayList<>();

        //Build rule
        Rule r = new Rule(event, context, o_t_score.getBeliefUpdate());
        if (debug) System.out.println("Adding rule: " + r.toString());
        agent.addOrReplaceRule(r);

        if (explain) {
            ExplanationEngine exEngine = agent.explanations();
            exEngine.addExplanations(exEngine.unitBuilder().build(r, learningProcessNamespace, o_t_score.getScore()));
        } 
    }
}
