package astra.learn;

import java.util.List;

import astra.core.Agent;
import astra.core.InternalAffordances;
import astra.core.Rule;
import astra.event.GoalEvent;
import astra.explanation.ExplanationEngine;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.learn.library.Algorithm;
import astra.learn.library.DataModel;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;
import astra.type.Type;

/**
 * This class is created per learning template
 */
public class LearningProcess extends InternalAffordances {
    
    public String processNamespace;
    String beliefSetNamespace;
    GoalEvent event;
    Algorithm algorithm;

    public LearningProcess(String process, String beliefSet, GoalEvent evt, Algorithm alg) {
        this.processNamespace = process;
        this.beliefSetNamespace = beliefSet;
        this.event = evt;
        this.algorithm = alg;        
    }

    @Override
    public void setAgent(Agent agent) {
        this.agent = agent;
        this.algorithm.initialize(agent, event, processNamespace);
        //Now can add explanation
        ExplanationEngine exEngine = agent.explanations();
        exEngine.addExplanations(exEngine.unitBuilder().build(this));
    
    }

    public String triggeringEventString() {
        return event.toString();
    }

    public void trigger() throws Exception {
        if (agent.trace()) System.out.println("[ " + agent.name() + "] LearningProcess: Setting config and input beliefs for " + processNamespace);
        extractConfigurationAndInputBeliefs();
        if (agent.trace()) System.out.println("[ " + agent.name() + "] LearningProcess: Applying learning function for " + processNamespace);
        //Now do step of process
        algorithm.applyLearningFunction();
    }

    public void extractConfigurationAndInputBeliefs() throws Exception {
        extractConfigBeliefs();
        extractInputBeliefs();
    }

    /*
     * Algorithm 7
     */
    public void extractConfigBeliefs() {
        // Configuration beliefs, i.e. +configuration(learningProcessName, configLabel, value);
        List<Formula> it = agent.beliefs().list(LearningProtectedPredicates.configBelief()); //This is a get on a HashMap - so O(1)
        for (Formula fd: it) { // O(n) if all agent beliefs are config beliefs
            Predicate b = (Predicate) fd;
            if (checkNamespace(b)) {
                //Handle someone adding a configuration belief manually, i.e. defining it as a type configuration(string, string)
                // with the first term matching the learning process namesapce
                if (b.size() != 3) {
                    throw new LearningProcessException("Configuration belief for " + processNamespace + " without three terms");
                }
                //Next two terms are config: term at index 1 is the config label, term at index 2 is the value. E.g. "alpha", 0.9
                try {
                    String configLabel = termAt(b, 1);
                    String value = termAt(b, 2);
                    algorithm.setConfiguration(configLabel, value);
                } catch (Exception e) {
                    throw new LearningProcessException("Exception building config beliefs for " + processNamespace + " " + e.getMessage());
                }
                
            }
        }
    }

    private boolean checkNamespace(Predicate b) {
        @SuppressWarnings("unchecked")
        String namespace = ((Primitive<String>)b.getTerm(0)).value();
        return namespace.equals(processNamespace);
    }

    @SuppressWarnings("unchecked")
    private String termAt(Predicate b, int i) {
        Primitive p = (Primitive)b.getTerm(i);
        if (p.type().equals(Type.STRING)) {
            return(String)p.value();
        }
        return p.toString();
    }


    /*
     * Algorithm 8
     */
    public void extractInputBeliefs() {
        //Default on every algorithm is NOT to cache input predicates
        if (algorithm.input_predicates.isEmpty()) { 
            //No input predicates are cached, retrieve them from belief set
            // Knowledge beliefs, i.e. +knowledge_input(learningProcessName, predicate);
            for (Integer id: LearningProtectedPredicates.knowledgeBeliefs()) { //O(4)
                List<Formula> it_knowledge = agent.beliefs().list(id); //O(1)
                for (Formula fd : it_knowledge) { //Worst case scenario? One of the 4 knowledge beliefs is only type of belief in agent belief base, size O(n). So this is O(n)
                    Predicate b = (Predicate) fd;
                     if (checkNamespace(b)) {
                        //Handle someone adding a belief with the matching predicate manually, i.e. defining it as a type
                        if (b.size() != 2) {
                            throw new LearningProcessException("Input belief for " + processNamespace + " has the incorrect number of terms");
                        }
                
                        try {
                            //Second term, at index 1, is the predicate for the matching belief.
                            // E.g. knowledge_input(<learning_process_namespace>, <other_belief>)
                            String relevantPredicate = termAt(b, 1);
                            //Now have the relevant predicate, get the matching beliefs
                            List<Formula> itRelevant = agent.beliefs().list(LearningProtectedPredicates.getID(relevantPredicate));
                            algorithm.setInputBelief(b.predicate(), itRelevant);                           
                        } catch (Exception e) {
                            throw new LearningProcessException("Exception building input beliefs for " + processNamespace + " " + e.getMessage());
                        }
                    }
                }
            }
        } else {
            for (String key: algorithm.input_predicates.keySet()) {
                //Input predicates are cached, no need to iterate belief set
                try {
                    Integer id = LearningProtectedPredicates.getID(algorithm.input_predicates.get(key));
                    List<Formula> itRelevant = agent.beliefs().list(id);
                    algorithm.setInputBelief(key, itRelevant);
                } catch (Exception e) {
                    throw new LearningProcessException("Exception building input beliefs for " + processNamespace + " " + e.getMessage());
                }
            }
        }
        if (!algorithm.labelSet) {
            // Special case: knowledge_label doesn't necessarily have a corresponding belief - so just get the label term
            // Constant time because - retrieve label belief (O(1)) and then if it is not size 1, throw exception
            Integer id = LearningProtectedPredicates.labelBelief();
            List<Formula> labelKnowledgeFormulas = agent.beliefs().list(id); 
            for (Formula fd: labelKnowledgeFormulas) { //O(1) as will throw exception if any bigger
                Predicate b = (Predicate) fd;
                if (checkNamespace(b)) {
                    Term[] t = {b.getTerm(1)};
                    algorithm.setLabelBelief(LearningProtectedPredicates.KNOWLEDGE_LABEL, t);
                }
            }
        }
    }

    /**
     * Universal actions over the learning process
	 */
    @TERM
    public DataModel model() {
        return algorithm.getModel();
    }

    @TERM
    public String namespace() {
        return this.processNamespace;
    }

    @TERM
    public String algorithmName() {
        return this.algorithm.getClass().getName();
    }

    @TERM
    public String configuration(String term) {
        try {
            Object o = this.algorithm.getConfiguration(term);
            if (o != null) {
                return o.toString(); 
            }
        } catch (Exception e) {
            if (agent.trace()) System.out.println("[ " + agent.name() + "] LearningProcess: Could not get config " + term);
        } 
        return "";
    }

    @TERM
    public double metric(String metric) {
        Double d = 0.0;
        try {
            d = this.algorithm.metrics.get(metric);
            if (d != null) {
                return d;
            }
        } catch (Exception e) {
            if (agent.trace()) System.out.println("[ " + agent.name() + "] LearningProcess: Could not get metric " + metric);
        } 
        return d;
    }

    @TERM
    public ListTerm availableMetrics() {
        ListTerm list = new ListTerm();
        for (String key: this.algorithm.metrics.keySet()) {
            list.add(Primitive.newPrimitive(key));
        }
        return list;
    }

    @TERM
    public ListTerm rules() {
        ListTerm list = new ListTerm();
        List<Rule> ruleList = agent.rules(event.signature()); 
		for (Rule r: ruleList) {
            list.add(Primitive.newPrimitive(r));
        }
        return list;
    }

    @ACTION
    public boolean mergeModel(DataModel incoming) {
        return algorithm.getModel().mergeModel(incoming);
    }

    @ACTION
    public boolean addRule(Rule r) {
        agent.addOrReplaceRule(r);
        return true;
    }

    /*
     * These are used to build the contexts for decision trees.
     * These are essentially the same as the Prelude methods, however don't want to force a module import
     * as this may interfere with other things in the agent.
     * 
     * ALSO - they need to handle null lists, because the triggering event for the learning process
     * may fire when there is no sample belief for decision trees
     */
    @FORMULA
	public Formula evaluateListTerm(ListTerm list, int index, boolean value) {
        boolean listValue = (Boolean) ((Primitive<?>) list.get(index)).value();
        return (listValue == value) ? Predicate.TRUE:Predicate.FALSE;
	}
    
	@TERM
	public String valueAsString(ListTerm list, int index) {
        if (list == null) {
            System.out.println("Null list, index is " + index);
            return "";
        }
		return ((Primitive<?>) list.get(index)).value().toString();
	}

    @FORMULA
	public Formula evaluateListTerm(ListTerm list, int index, String comparitor, float value) {
        float listValue = (Float) ((Primitive<?>) list.get(index)).value();
        if (comparitor.equals(">")) {
            return (listValue > value) ? Predicate.TRUE:Predicate.FALSE;
        }
        else if (comparitor.equals(">=")) {
            return (listValue >= value) ? Predicate.TRUE:Predicate.FALSE;
        }
        else if (comparitor.equals("<")) {
            return (listValue < value) ? Predicate.TRUE:Predicate.FALSE;
        }
        else if (comparitor.equals("==")) {
            return (listValue == value) ? Predicate.TRUE:Predicate.FALSE;
        } 
        else if (comparitor.equals("<=")) {
            return (listValue <= value) ? Predicate.TRUE:Predicate.FALSE;
        } else {
            throw new RuntimeException("Unsupported comparitor");
        }
	}

    @FORMULA
	public Formula evaluateListTerm(ListTerm list, int index, String comparitor, double value) {
        double listValue = (Double) ((Primitive<?>) list.get(index)).value();
        if (comparitor.equals(">")) {
            return (listValue > value) ? Predicate.TRUE:Predicate.FALSE;
        }
        else if (comparitor.equals(">=")) {
            return (listValue >= value) ? Predicate.TRUE:Predicate.FALSE;
        }
        else if (comparitor.equals("<")) {
            return (listValue < value) ? Predicate.TRUE:Predicate.FALSE;
        }
        else if (comparitor.equals("<=")) {
            return (listValue <= value) ? Predicate.TRUE:Predicate.FALSE;
        }  
        else if (comparitor.equals("==")) {
            return (listValue == value) ? Predicate.TRUE:Predicate.FALSE;
        } else {
            throw new RuntimeException("Unsupported comparitor");
        }
	}

    @FORMULA
	public Formula evaluateListTerm(ListTerm list, int index, String comparitor, String value) {
        String listValue = (String) ((Primitive<?>) list.get(index)).value();
        if (comparitor.equals("==")) {
            return (listValue.equals(value)) ? Predicate.TRUE:Predicate.FALSE;
        } else {
            throw new RuntimeException("Unsupported comparitor");
        }
	}

    @FORMULA
	public Formula evaluateListTerm(ListTerm list, int index, String comparitor, int value) {
        int listValue = (Integer) ((Primitive<?>) list.get(index)).value();
       //TODO link in comparison formula, look at AbstractEvaluateVisitor handler for Comparison
        if (comparitor.equals(">")) {
            return (listValue > value) ? Predicate.TRUE:Predicate.FALSE;
        }
        else if (comparitor.equals(">=")) {
            return (listValue >= value) ? Predicate.TRUE:Predicate.FALSE;
        }
        else if (comparitor.equals("<")) {
            return (listValue < value) ? Predicate.TRUE:Predicate.FALSE;
        }
        else if (comparitor.equals("<=")) {
            return (listValue <= value) ? Predicate.TRUE:Predicate.FALSE;
        }
        else if (comparitor.equals("==")) {
            return (listValue == value) ? Predicate.TRUE:Predicate.FALSE;
        } else {
            throw new RuntimeException("Unsupported comparitor");
        }
	}
}

