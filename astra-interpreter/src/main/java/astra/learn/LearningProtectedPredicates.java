package astra.learn;

import java.util.ArrayList;
import java.util.List;

import astra.reasoner.util.StringMapper;

public class LearningProtectedPredicates {

    private static StringMapper mapper = new StringMapper();;
	
    public static final String KNOWLEDGE_INPUT =  "knowledge_input";
    public static final String KNOWLEDGE_ACTION = "knowledge_action";
    public static final String KNOWLEDGE_LABEL = "knowledge_label";
	public static final String KNOWLEDGE_REWARD = "knowledge_reward";
    public static final String KNOWLEDGE_TRAIN = "knowledge_train";
	public static final String CONFIGURATION_BELIEFS = "configuration";

    public static Integer configBelief() {
        return mapper.toId(CONFIGURATION_BELIEFS);
    }

    public static List<Integer> knowledgeBeliefs() {
        List<Integer> formulaIDs = new ArrayList<>();
        formulaIDs.add(mapper.toId(KNOWLEDGE_INPUT));
        formulaIDs.add(mapper.toId(KNOWLEDGE_ACTION));
        formulaIDs.add(mapper.toId(KNOWLEDGE_REWARD));
        formulaIDs.add(mapper.toId(KNOWLEDGE_TRAIN));
        return formulaIDs;
    }

    public static Integer labelBelief() {
        return mapper.toId(KNOWLEDGE_LABEL);
    }

    public static Integer getID(String predicate) {
        return mapper.toId(predicate);
    }
    
}