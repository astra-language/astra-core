package astra.learn;

import java.util.HashMap;
import java.util.Map;
import astra.core.Agent;
import astra.event.GoalEvent;

public class LearningSelectionFunction {
    
    public Agent agent;
    public Map<String, LearningProcess> processes;
    public Map<String, String> nameToProcess;

    public LearningSelectionFunction(Agent agent) {
        this.agent = agent;
        this.processes = new HashMap<>();
        this.nameToProcess = new HashMap<>();
    }

    public void addLearningProcess(LearningProcess p) {
        p.setAgent(agent);
        this.processes.put(p.triggeringEventString(), p);
        this.nameToProcess.put(p.processNamespace, p.triggeringEventString());
    }

    public LearningProcess learningProcessForEvent(GoalEvent e) {
        return processes.get(e.toString());
    }

    public LearningProcess getLearningProcess(String namespace) {
        String trigString = nameToProcess.get(namespace);
        return processes.get(trigString);
    }

}
