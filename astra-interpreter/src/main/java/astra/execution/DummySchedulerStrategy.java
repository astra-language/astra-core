package astra.execution;

import astra.core.Agent;
import astra.core.Scheduler;
import astra.core.Task;


/**
 * This strategy was developed for the unit testing framework which uses its own control
 * layer instead of the scheduler.
 * 
 * @author Rem
 *
 */
public class DummySchedulerStrategy implements SchedulerStrategy {
	public void schedule(Agent agent) {}

	public void schedule(final Task task) {
		new Thread() {
			public void run() {
				task.doTask();
			}
		}.start();
	}

	public void setThreadPoolSize(int size) {}

	public void stop() {}

	public void setState(Agent agent, int state) {}

	public int getState(Agent agent) {
		return Scheduler.ACTIVE;
	}

	public void setSleepTime(long sleepTime) {}

	public void shutdown() {}
}
