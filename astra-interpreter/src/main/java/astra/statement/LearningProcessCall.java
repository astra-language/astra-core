package astra.statement;

import astra.core.AbstractTask;
import astra.core.Intention;
import astra.core.InternalAffordances.TERM;
import astra.formula.Predicate;
import astra.reasoner.util.ContextEvaluateVisitor;
import astra.reasoner.util.VariableVisitor;
import astra.term.Primitive;
import astra.term.Term;

public class LearningProcessCall extends AbstractStatement {
	
    String learningProcess;
	Predicate method;
	LearningProcessCallAdaptor adaptor;
	
	public LearningProcessCall(LearningProcessCallAdaptor adaptor) {
		this.adaptor = adaptor;
	}

	public LearningProcessCall(String learningProcess, String clazz, int[] data, Predicate method, LearningProcessCallAdaptor adaptor) {
		this.setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.learningProcess = learningProcess;
		this.method = method;
		this.adaptor = adaptor;
	}

	public LearningProcessCall(String learningProcess, Predicate method, LearningProcessCallAdaptor adaptor) {
		this.learningProcess = learningProcess;
		this.method = method;
		this.adaptor = adaptor;
	}

	@Override
	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			int state = 0;
			AbstractTask task;
			Predicate action;
			
			@Override
			public boolean execute(Intention context) {
				switch (state) {
				case 0:
					task= new AbstractTask() {
						/**
						 *
						 */
						private static final long serialVersionUID = -7588983545629284558L;

						public void doTask() {
							// action = method.clone();
							// System.out.println("method: " + method);
							// System.out.println("method.clone: " + method.clone());
							action = (Predicate) method.clone().accept(new ContextEvaluateVisitor(context));
							// System.out.println("[" + context.agent.name() + "-modulecall] Action: " + action);
							VariableVisitor visitor = new VariableVisitor();
							action.accept(visitor);
							// if (context.agent.trace())
							// System.out.println("["+context.agent.name()+"-modulecall] variables: " + visitor.variables());
							executor.addUnboundVariables(visitor.variables());
							context.resetActionParams();
							try {
								if (!adaptor.invoke(context, action)) {
									context.notifyDone("Failed Action: " + learningProcess + "#" + action);
								}
								
								context.applyActionParams();
								if (!adaptor.suppressNotification()) context.notifyDone(null);
							} catch (Throwable th) {
								context.notifyDone("Failed Action: " + learningProcess + "#" + action, th);
							}
						}

						@Override
						public Object source() {
							return null;
						}
					};
					
					context.suspend();
					if (adaptor.inline()) {
						task.doTask();
						task = null;
						return false;
					} else {
						context.schedule(task);
						state = 1;
						return true;
					}

					// ALL ACTIONS ARE INLINE - DEFAULT TO RETURNING
					// false TO INDICATE THAT THE STATEMENT IS COMPLETED...
				case 1:
					task = null;
				}
				return false;
			}

			@Override
			public boolean onFail(Intention context) {
				return false;
			}
			
			public String toString() {
				return method.toString();
			}
			
			@Override
			public Statement statement() {
				return LearningProcessCall.this;
			}
		};
	}
	
	public String toString() {
		return learningProcess+"#"+method.toString();
	}
	
}
