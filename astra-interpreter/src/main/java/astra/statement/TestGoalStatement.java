package astra.statement;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

import astra.core.Intention;
import astra.event.TestGoalEvent;
import astra.formula.Formula;
import astra.formula.Goal;
import astra.formula.TestGoal;
import astra.reasoner.util.ContextEvaluateVisitor;
import astra.reasoner.util.VariableVisitor;
import astra.term.Term;
import astra.term.Variable;

public class TestGoalStatement extends AbstractStatement {
	TestGoal goal;
	
	public TestGoalStatement(TestGoal goal) {
		this.goal = goal;
	}
	
	public TestGoalStatement(String clazz, int[] data, TestGoal goal) {
		setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.goal = goal;
	}
	
	@Override
	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			TestGoal gl;
			
			int index = 0;
			@Override
			public boolean execute(Intention intention) {
				switch (index) {
				case 0:
					// System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
					// System.out.println("goal: " + goal);

					// Try to match the goal to the state - if successfull, no need to raise events
					Map<Integer, Term> result = intention.query((Formula) goal.formula().accept(new ContextEvaluateVisitor(intention)));
					if (result != null) {
						// System.out.println("We got a match: " + result);
						for(Entry<Integer, Term> entry: result.entrySet()) {
							executor.addVariable(new Variable(entry.getValue().type(), entry.getKey()), entry.getValue());
						}
						return false;
					}
					executor = intention.executor();
					// System.out.println("all variables: " +executor.getAllBindings());
					// System.out.println("variables: " +executor.bindings());

					//gl = (Goal) goal.accept(new BindingsEvaluateVisitor(executor.getAllBindings(), intention.agent));
					gl = (TestGoal) goal.accept(new ContextEvaluateVisitor(intention));
					// System.out.println("Gl:" + gl);
				
					// Get Unbound Variables from Goal
					VariableVisitor visitor = new VariableVisitor();
					gl.accept(visitor);
					// System.out.println("Variables:" + visitor.variables());

					for (Variable variable : visitor.variables()) {
						executor.addVariable(variable);
					}

					TestGoalEvent goalEvent = new TestGoalEvent(TestGoalEvent.ADDITION, gl, executor);
				    if (intention.addEvent(goalEvent))
						intention.suspend();
					else
						intention.failed("Subgoal: " + goalEvent + " was not added to the agents event queue");

					index = 1;
					return true;
				case 1:
					// System.out.println("normal subgoal removal...");
					// System.out.println(intention.bindings());
					intention.addEvent(new TestGoalEvent(TestGoalEvent.REMOVAL, gl));
				}
				return false;
			}

			@Override
			public boolean onFail(Intention intention) {
				// System.out.println("onFail called...");
				intention.addEvent(new TestGoalEvent(TestGoalEvent.REMOVAL, gl));
				// Returns true if the failure event is added (there is a rule to handle it) or false otherwise
				return intention.addEvent(new TestGoalEvent(TestGoalEvent.FAILURE, gl, intention.executor()));
			}
			
			@Override
			public Statement statement() {
				return TestGoalStatement.this;
			}
			
			public String toString() {
				if (gl == null) return goal.toString();
				return gl.toString();
			}
			
			public void addGoals(Queue<Formula> list, Goal goal) {
				if (goal.formula().id() == gl.formula().id()) list.add(gl);
			}
		};
	}
	

}
