package astra.statement;

import astra.core.Intention;
import astra.explanation.store.ExplanationUnit;
import astra.reasoner.util.ContextEvaluateVisitor;
import astra.term.Primitive;
import astra.term.Term;

/** 
 * Support for following language level changes:
 * C = context (formula)
 * v = event (formula) - TODO OR PLAN 
 * t = triples (list)
 * 
 * explain( C, v, T );
 * 
 * explain( C, v );
 * 
 * explain( C, T );
 */
public class Explain extends AbstractStatement {
	
	Term ID;
	Term tag;
	Term value;
	
	public Explain(String clazz, int[] data, Term tag, Term value) {
		this.setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.tag = tag;
		this.value = value;
	}

	public Explain(String clazz, int[] data, Term ID, Term tag, Term value) {
		this.setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.ID = ID;
		this.tag = tag;
		this.value = value;
	}

	@Override
	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			
			@Override
			public boolean execute(Intention intention) {
				ContextEvaluateVisitor visitor = new ContextEvaluateVisitor(intention, true);

				Primitive tagVisited = (Primitive)Explain.this.tag.accept(visitor);
				ExplanationUnit ex = null;
				
				Primitive valueVisited = (Primitive)Explain.this.value.accept(visitor);
				ex = new ExplanationUnit((String)tagVisited.value(), valueVisited);
				
				if (Explain.this.ID != null) {
					Primitive IDVisited = (Primitive)Explain.this.ID.accept(visitor);
					int id = (Integer)IDVisited.value();
					ex.setID(id);
				}
				if (ex != null) {
					intention.addExplanation(ex);
				}
				
				return false;
			}

			@Override
			public boolean onFail(Intention context) {
				return false;
			}

			@Override
			public Statement statement() {
				return Explain.this;
			}
			
			public String toString() {
				return Explain.this.toString();
			}
			
		};
	}

	public String toString() {
		String retval = "explain( ";
		if (ID != null) {
			retval += ID + ", ";
		} 
		retval += tag + ", " + value + ")";
		return retval;
	}
}
