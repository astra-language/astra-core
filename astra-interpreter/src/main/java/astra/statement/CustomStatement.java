package astra.statement;

import java.util.Queue;

import astra.core.Intention;
import astra.core.RuleExecutor;
import astra.formula.Formula;
import astra.formula.Goal;

public class CustomStatement implements Statement {
    @Override
    public StatementHandler getStatementHandler() {
        return new StatementHandler() {

            @Override
            public boolean execute(Intention Intention) {
                System.out.println("Hello World!");
                return false;
            }

            @Override
            public boolean onFail(Intention intention) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public Statement statement() {
                return CustomStatement.this;
            }

            @Override
            public void addGoals(Queue<Formula> list, Goal goal) {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void setRuleExecutor(RuleExecutor executor) {
                // TODO Auto-generated method stub
                
            }
            
        };
    }

    @Override
    public boolean isLinkedToSource() {
        return false;
    }

    @Override
    public int beginLine() {
        return 0;
    }

    @Override
    public int endLine() {
        return 0;
    }

    @Override
    public String getASTRAClass() {
        return null;
    }
}
