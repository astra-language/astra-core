package astra.statement;

import astra.core.Intention;
import astra.formula.Formula;

public class Fail extends AbstractStatement {

	public Fail(String clazz, int[] data) {
		this.setLocation(clazz, data[0], data[1], data[2], data[3]);
	}

	public Fail(String clazz, int[] data, Formula guard, Statement ifStatement) {
		this.setLocation(clazz, data[0], data[1], data[2], data[3]);
	}

	@Override
	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			@Override
			public boolean execute(Intention intention) {
				// System.out.println("Starting Done statement...");
				intention.failed("Forced Failure of Intention");
				// System.out.println("Completed Done statement");
				return false;
			}

			@Override
			public boolean onFail(Intention context) {
				return false;
			}

			@Override
			public Statement statement() {
				return Fail.this;
			}
			
			public String toString() {
				return "fail()";
			}
		};
	}

}
