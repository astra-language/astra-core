package astra.statement;

import astra.core.Intention;
import astra.formula.Predicate;

/*
 * Copy of ModuleCallAdapter
 */
public interface LearningProcessCallAdaptor {
	public boolean suppressNotification();
	public boolean inline();
	public boolean invoke(Intention context, Predicate atom);
}

