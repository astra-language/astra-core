package astra.formula;

import astra.reasoner.util.LogicVisitor;
import astra.term.Term;
import astra.term.Variable;

public class Bind implements Formula {
	/**
	 *
	 */
	private static final long serialVersionUID = 2663348030164547848L;
	
	private Variable variable;
	private Term term;
	
	public Bind(Variable variable, Term term) {
		this.variable = variable;
		this.term = term;
	}

	public void reIndex() {
		variable.reIndex();
		term.reIndex();
	}

	public Variable variable() {
		return variable;
	}
	
	public Term term() {
		return term;
	}
	
	public String toString() {
		return "bind(" + variable + ", " + term + ")";
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Formula formula) {
		System.out.println("Bind.matches!!!");
		return false;
	}

}
