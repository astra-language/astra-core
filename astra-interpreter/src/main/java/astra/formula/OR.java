package astra.formula;

import astra.reasoner.util.LogicVisitor;

public class OR implements Formula {
	/**
	 *
	 */
	private static final long serialVersionUID = 2652979477428817051L;
	Formula left;
	Formula right;
	
	public OR (Formula left, Formula right) {
		this.left = left;
		this.right = right;
	}

	public void reIndex() {
		left.reIndex();
		right.reIndex();
	}
	
	public Formula left() {
		return left;
	}

	public Formula right() {
		return right;
	}

	@Override
	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public Formula[] formulae() {
		return new Formula[] { left, right };
	}

	@Override
	public boolean matches(Formula formula) {
		return (formula instanceof OR) && ((OR) formula).left.matches(left) && ((OR) formula).right.matches(right);
	}
	
	public String toString() {
		return left + " | " + right;
	}
}
