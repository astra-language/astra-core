package astra.formula;

import astra.reasoner.util.LogicVisitor;

public class Inference implements Formula {
	/**
	 *
	 */
	private static final long serialVersionUID = 4037757884576897405L;
	
	Predicate head;
	Formula body;
	
	public Inference (Predicate head, Formula body) {
		this.head = head;
		this.body = body;
	}

	public void reIndex() {
		head.reIndex();
		body.reIndex();
	}

	public Predicate head() {
		return head;
	}
	
	public Formula body() {
		return body;
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Formula formula) {
		if (formula instanceof Inference) {
			return ((Inference) formula).head.matches(head) && ((Inference) formula).body.matches(body);
		}
		return false;
	}
	
	public String toString() {
		return head + " :- " + body;
	}
}
