package astra.formula;

import astra.reasoner.util.LogicVisitor;
import astra.term.Term;

public class BooleanTermFormula implements Formula {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3531850681574434943L;
	private Term term;
	
	public BooleanTermFormula(Term term) {
		this.term = term;
	}
	
	public Term term() {
		return term;
	}

	public void reIndex() {
		term.reIndex();
	}

	@Override
	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public String toString() {
		return "(" + term + ")";
	}
	
	public BooleanTermFormula clone() {
		return new BooleanTermFormula(term.clone());
	}

	@Override
	public boolean matches(Formula formula) {
		return term.matches(((BooleanTermFormula) formula).term());
	}

}
