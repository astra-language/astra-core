package astra.formula;

import astra.reasoner.util.LogicVisitor;

public class LearningProcessFormula implements Formula {
	/**
	 *
	 */
	private static final long serialVersionUID = 5866231318251013321L;
	
	private String learningProcess;
	private Predicate predicate;
	private LearningProcessFormulaAdaptor adaptor;
	
	public LearningProcessFormula(String learningProcess, Predicate predicate, LearningProcessFormulaAdaptor adaptor) {
		this.learningProcess = learningProcess;
		this.predicate = predicate;
		this.adaptor = adaptor;
	}

	public void reIndex() {
		predicate.reIndex();
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Formula formula) {
		return false;
	}

	public String learningProcess() {
		return learningProcess;
	}
	
	public Predicate predicate() {
		return predicate;
	}
	
	public LearningProcessFormulaAdaptor adaptor() {
		return adaptor;
	}
	
	public String toString() {
		return learningProcess + "#" + predicate;
	}
}
