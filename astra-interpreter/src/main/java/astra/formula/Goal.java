package astra.formula;

import astra.reasoner.util.LogicVisitor;

public class Goal implements Formula {
	/**
	 *
	 */
	private static final long serialVersionUID = -4218559202716916656L;
	
	private Predicate predicate;
	
	public Goal(Predicate predicate) {
		this.predicate = predicate;
	}

	public void reIndex() {
		predicate.reIndex();
	}

	public Predicate formula() {
		return predicate;
	}
	
	public String toString() {
		return "!" + predicate.toString();
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Formula formula) {
		return (formula instanceof Goal) && ((Goal) formula).predicate.matches(predicate);
	}

}
