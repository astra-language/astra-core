package astra.formula;

import astra.reasoner.util.LogicVisitor;

public class IsDone implements Formula {
	/**
	 *
	 */
	private static final long serialVersionUID = 8127441265253220310L;

	public void reIndex() {
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Formula formula) {
		return false;
	}
}
