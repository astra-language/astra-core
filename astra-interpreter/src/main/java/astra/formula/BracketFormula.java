package astra.formula;

import astra.reasoner.util.LogicVisitor;

public class BracketFormula implements Formula {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8544417764206263107L;
	private Formula formula;
	
	public BracketFormula(Formula formula) {
		this.formula = formula;
	}

	public void reIndex() {
		formula.reIndex();
	}

	public Formula formula() {
		return formula;
	}
	
	public String toString() {
		return "(" + formula.toString() + ")";
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Formula formula) {
		return (formula instanceof BracketFormula) && ((BracketFormula) formula).formula.matches(formula);
	}

}
