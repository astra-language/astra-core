package astra.formula;

import astra.reasoner.util.LogicVisitor;

public class AND implements Formula {
	/**
	 *
	 */
	private static final long serialVersionUID = -7230172012647827352L;
	
	public Formula[] formulae = new Formula[2];
	
	public AND (Formula left, Formula right) {
		formulae[0] = left;
		formulae[1] = right;
	}
	
	public Formula left() {
		return formulae[0];
	}

	public Formula right() {
		return formulae[1];
	}

	public void reIndex() {
		formulae[0].reIndex();
		formulae[1].reIndex();
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Formula formula) {
		return (formula instanceof AND) && ((AND) formula).formulae[0].matches(formulae[0]) && ((AND) formula).formulae[1].matches(formulae[1]);
	}
	
	public String toString() {
		return formulae[0] + " & " + formulae[1];
	}

}
