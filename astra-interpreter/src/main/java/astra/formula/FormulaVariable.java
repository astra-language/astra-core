package astra.formula;

import astra.reasoner.util.LogicVisitor;
import astra.term.Variable;

public class FormulaVariable implements Formula {
	/**
	 *
	 */
	private static final long serialVersionUID = -4450140522918473556L;
	
	private Variable variable;
	private Formula value;
	
	public FormulaVariable(Variable variable) {
		this.variable = variable;
	}

	public void reIndex() {
		variable.reIndex();
		value.reIndex();
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public Variable variable() {
		return variable;
	}
	
	public Formula value() {
		return value;
	}
	
	public void value(Formula value) {
		this.value = value;
	}

	public boolean matches(Formula formula) {
		throw new UnsupportedOperationException("Should not get this");
	}
}
