package astra.type;

public class InvalidTypeException extends RuntimeException {

	public InvalidTypeException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1043617653662485337L;

}
