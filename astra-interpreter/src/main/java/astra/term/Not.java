package astra.term;

import astra.reasoner.util.LogicVisitor;
import astra.type.Type;

public class Not implements Term {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3531850681574434943L;
	Term term;
	
	public Not(Term term) {
		this.term = term;
	}

	@Override
	public Type type() {
		return Type.BOOLEAN;
	}

	public void reIndex() {
		term.reIndex();
	}
	
	public Term term() {
		return term;
	}

	@Override
	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean matches(Term term) {
		return (term instanceof Not) && (this.term.matches(((Not) term).term));
	}

	@Override
	public String signature() {
		return "not";
	}
	
	public String toString() {
		return "~" + term.toString();
	}

	public Not clone() {
		return new Not(term.clone());
	}
}
