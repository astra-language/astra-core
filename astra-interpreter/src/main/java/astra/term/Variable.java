package astra.term;

import astra.reasoner.util.LogicVisitor;
import astra.reasoner.util.StringMapper;
import astra.type.Type;

public class Variable implements Term {
	/**
	 *
	 */
	private static final long serialVersionUID = 8088567323537166255L;

	public static StringMapper mapper = new StringMapper();

	private Type type;
	private String identifier;
	private int id;
	private boolean returns;
	
	public Variable(Type type, String name) {
		this(type, name, false);
	}
	
	public Variable(Type type, String name, boolean returns) {
		this.identifier = name;
		this.id = mapper.toId(name);
		this.type = type;
		this.returns = returns;
	}
	
	public Variable(Type type, int key) {
		this.id = key;
		this.identifier = mapper.fromId(key);
		this.type = type;
	}

	public void reIndex() {
		id = mapper.toId(identifier);
		type.reIndex();
	}

	@Override
	public Type type() {
		return type;
	}
	
	public int id() {
		return id;
	}
	
	public String identifier() {
		return identifier;
	}

	@Override
	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}
	
	public String toString() {
		return identifier+":"+id; 
	}

	@Override
	public boolean matches(Term right) {
		return type.equals(right.type());
	}

	public boolean returns() {
		return returns;
	}
	
	@Override
	public String signature() {
		return null;
	}

	public Variable clone() {
		return this;
	}

	@SuppressWarnings("unchecked")
	public boolean equals(Object object) {
		if (object instanceof Variable) {
			Variable v = (Variable)object;
			return type.equals(v.type()) && identifier.equals(v.identifier) && (returns == v.returns);
		}
		
		return false;
	}
}
