package astra.term;

import astra.reasoner.util.LogicVisitor;
import astra.reasoner.util.StringMapper;
import astra.type.Type;

public class Performative implements Term {
	/**
	 *
	 */
	private static final long serialVersionUID = -5641472519856863137L;

	private static StringMapper mapper = new StringMapper();

	private int id;
	private String value;

	public Performative(String value) {
		this.value = value;
		this.id = mapper.toId(value);
	}
	
	public int id() {
		return id;
	}
	
	public String value() {
		return value;
	}

	@Override
	public Type type() {
		return Type.PERFORMATIVE;
	}

	public String toString() {
		return "\"" + value + "\"";
	}
	
	public boolean equals(Object obj) {
		if (Performative.class.isInstance(obj)) {
			return ((Performative) obj).id == id;
		}
		return false;
	}

	public void reIndex() {
		id = mapper.toId(value);
	}

	@Override
	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean matches(Term term) {
		if (term instanceof Performative) {
			return id == ((Performative) term).id;
		}
		return (term instanceof Variable);
	}

	@Override
	public String signature() {
		return "PE:"+id;
	}

	public Performative clone() {
		return this;
	}
}
