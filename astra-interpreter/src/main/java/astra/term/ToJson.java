package astra.term;

import astra.reasoner.util.LogicVisitor;
import astra.type.Type;

public class ToJson implements Term {
	/**
	 *
	 */
	private static final long serialVersionUID = 3084025495867606648L;

	private Term term;

	public ToJson(Term term) {
		this.term = term;
	}

	public void reIndex() {
		term.reIndex();
	}
	
	public Type type() {
		return Type.STRING;
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Term right) {
		if (!ToJson.class.isInstance(right)) return false;
		return term.matches(((ToJson) right).term);
	}

	public boolean equals(Object object) {
		return (object instanceof ToJson);
	}

	public String signature() {
		return "JS:"+term.signature();
	}
	
	public ToJson clone() {
		return this;
	}

	public Term term() {
		return term;
	}

	public String toString() {
		return "to_json("+term+")";
	}

}
