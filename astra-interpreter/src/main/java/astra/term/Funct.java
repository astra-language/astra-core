package astra.term;

import astra.reasoner.util.LogicVisitor;
import astra.reasoner.util.StringMapper;
import astra.type.Type;

public class Funct implements Term {
	/**
	 *
	 */
	private static final long serialVersionUID = -5262384951018591053L;

	private static StringMapper mapper = new StringMapper();
	
	private String functor;
	private int id;
	Term[] terms;
	
	public Funct(String functor, Term[] terms) {
		this.functor = functor;
		this.id = mapper.toId(functor);
		this.terms = terms;
	}

	public Term getTerm(int i) {
		return terms[i];
	}
	
	public int size() {
		return terms.length;
	}
	
	public int id() {
		return id;
	}

	public Term[] terms() {
		return terms;
	}
	
	public String toString() {
		String out = mapper.fromId(id);
		out += "(";
		if (terms.length > 0) {
			for (int i=0;i<terms.length; i++) {
				if (i > 0) out += ",";
				out += terms[i].toString();
			}
		}
		out += ")";
		return out;
	}

	@Override
	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public String functor() {
		return functor;
	}

	/**
	 * This should be called whenever a 
	 */
	public void reIndex() {
		id = mapper.toId(functor);
		for (Term term: terms) {
			term.reIndex();
		}
	}

	@Override
	public boolean matches(Term term) {
		if (term instanceof Funct) {
			Funct f = (Funct) term;
			if (f.id != id || f.size() != terms.length) return false;
			
			for (int i=0; i < terms.length; i++) {
				if (!terms[i].matches(f.getTerm(i))) {
					return false;
				}
			}
			
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Funct) {
			Funct p = (Funct) object;
			if (p.id != id || p.size() != terms.length) return false;
			
			for (int i=0; i < terms.length; i++) {
				if (!terms[i].equals(p.getTerm(i))) {
					return false;
				}
			}
			
			return true;
		}
		return false;
	}

	public Term termAt(int i) {
		return terms[i];
	}

	@Override
	public Type type() {
		return Type.FUNCTION;
	}

	@Override
	public String signature() {
		return this.id+":"+terms.length;
	}

	public void set(int i, Term term) {
		terms[i] = term;
	}

	public Funct clone() {
		Term[] values = new Term[terms.length];
		for (int i=0;i<terms.length; i++) {
			values[i] = terms[i].clone();
		}
		return new Funct(functor, terms);
	}
}
