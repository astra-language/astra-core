package astra.term;

import astra.reasoner.util.LogicVisitor;
import astra.type.Type;

public class Tail implements Term {
	/**
	 *
	 */
	private static final long serialVersionUID = -1102565278394169497L;
	Term term;
	
	public Tail(Term term) {
		this.term = term;
	}
	
	public void reIndex() {
		term.reIndex();
	}

	public Type type() {
		return Type.LIST;
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Term right) {
		if (!Tail.class.isInstance(right)) return false;
		return term.matches(((Tail) right).term);
	}

	public boolean equals(Object object) {
		return (object instanceof Tail);
	}

	public String signature() {
		return "TL:"+term.signature();
	}
	
	public Tail clone() {
		return this;
	}

	public Term term() {
		return term;
	}

	public String toString() {
		return "tail("+term+")";
	}
}
