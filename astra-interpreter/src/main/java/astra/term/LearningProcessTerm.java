package astra.term;

import astra.core.Intention;
import astra.formula.Predicate;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.reasoner.util.ContextEvaluateVisitor;
import astra.reasoner.util.LogicVisitor;
import astra.type.Type;


public class LearningProcessTerm implements Term {
	/**
	 * Basically the same as module term...
	 */
	private static final long serialVersionUID = 596832607121187737L;
	
	String learningNamespace;
	Predicate method;
	LearningProcessTermAdaptor adaptor;
	Type type;
	
	public LearningProcessTerm(LearningProcessTermAdaptor adaptor) {
		this.adaptor = adaptor;
	}

	public LearningProcessTerm(String learningNamespace, Type type, Predicate method, LearningProcessTermAdaptor adaptor) {
		this.learningNamespace = learningNamespace;
		this.type = type;
		this.method = method;
		this.adaptor = adaptor;
	}

	public void reIndex() {
		method.reIndex();
		type.reIndex();
	}

	@Override
	public Type type() {
		return type;
	}

	@Override
	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public Predicate method() {
		return method;
	}
	
	@Override
	public boolean matches(Term right) {
		return false;
	}

	public Object evaluate(Intention context) {
		try {
			return adaptor.invoke(context, (Predicate) method.accept(new ContextEvaluateVisitor(context)));
		} catch (Throwable th) {
			th.printStackTrace();
			throw th;
		}
	}

	public Object evaluate(BindingsEvaluateVisitor visitor) {
		System.out.println("adaptor " + adaptor);
		System.out.println("visitor " + visitor);
		System.out.println("method " + method); //ValueAsFloat
		System.out.println("type " + type);
		//FIXME: this is where issue is
		return adaptor.invoke(visitor, (Predicate) method.accept(visitor));
	}

	public boolean equals(Object object) {
		if (object instanceof LearningProcessTerm) {
			LearningProcessTerm term = (LearningProcessTerm) object;
			return term.learningNamespace.equals(learningNamespace) && term.method.equals(method);
		}
		return false;
	}

	public String toString() {
		return learningNamespace + "#"  + method;
	}

	@Override
	public String signature() {
		return null;
	}
	
	public LearningProcessTermAdaptor adaptor() {
		return adaptor;
	}

	public String learningNamespace() {
		return learningNamespace;
	}
	
	public LearningProcessTerm clone() {
		LearningProcessTerm clone = new LearningProcessTerm(adaptor);
		clone.method = method.clone();
		clone.learningNamespace = learningNamespace;
		clone.type = type;
		return clone;
	}
}
