package astra.term;

import astra.reasoner.util.LogicVisitor;
import astra.type.Type;

public class Head implements Term {
	/**
	 *
	 */
	private static final long serialVersionUID = 958926477039364094L;
	Term term;
	Type type;
	
	public Head(Term term, Type type) {
		this.term = term;
		this.type = type;
	}
	
	@Override
	public Type type() {
		return type;
	}

	public void reIndex() {
		term.reIndex();
		type.reIndex();
	}

	@Override
	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean matches(Term right) {
		if (!Head.class.isInstance(right)) return false;
		return term.matches(((Head) right).term);
	}

	public boolean equals(Object object) {
		return (object instanceof Head);
	}

	@Override
	public String signature() {
		return "HD:"+term.signature();
	}
	
	public Head clone() {
		return this;
	}

	public Term term() {
		return term;
	}
	
	public String toString() {
		return "head("+term+","+type+")";
	}

}
