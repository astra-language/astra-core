package astra.messaging;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import astra.core.Agent;


public abstract class MessageService {
	static Map<String, MessageService> services = new HashMap<String, MessageService>();
	static List<String> serviceIds = new LinkedList<String>();
	static String[] serviceIdArray = new String[0];
	
	public static boolean hasService(String id) {
		return services.containsKey(id);
	}

	public static void installService(String id, MessageService service) {
		if (!services.containsKey(id)) {
			services.put(id, service);
			serviceIds.add(id);
		}
	}
	
	public static MessageService getService(String id) {
		return services.get(id);
	}
	
	public static boolean send(AstraMessage message) {
		if (serviceIds.isEmpty()) {
			// Install LocalMQService as Default...
			System.out.println("[MessageService] No service installed - using LocalMQService");
			installService("local", new LocalMQService());
		}

		// Defensive copy of the service ids to stop concurrent modification errors
		// The installService() method has also been modified to stop duplicate
		// message services being added.
		if (serviceIdArray.length < serviceIds.size()) {
			serviceIdArray = serviceIds.toArray(new String[serviceIds.size()]);
		}

		for (String id : serviceIdArray) {
			if (services.get(id).sendMessage(message)) {
				return true;
			}
		}
		
		return false;
	}
	
	public abstract boolean sendMessage(AstraMessage message);
	public abstract void configure(String key, String value);
	public abstract void start();

	public boolean receiveMessage(AstraMessage message) {
		boolean received = false;
		for (String name : message.receivers) {
            Agent receiver = Agent.getAgent(name);
            if ( receiver != null ) {
               receiver.receive(message);
               received = true;
            }
		}
		return received;
	}

}