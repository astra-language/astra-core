package astra.explanation;

import java.util.List;

import astra.explanation.store.ExplanationUnit;

public class ExplanationBuilder {
    
    public Explanation build(List<ExplanationUnit> units) {
        return new Explanation(units);
    }
}
