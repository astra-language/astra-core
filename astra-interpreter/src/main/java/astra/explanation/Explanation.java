package astra.explanation;

import java.util.List;

import astra.explanation.store.ExplanationUnit;

public class Explanation {
    
    List<ExplanationUnit> units;

    public Explanation(List<ExplanationUnit> units) {
        this.units = units;
    }

    public List<ExplanationUnit> getUnits() {
        return units;
    }

    /*
     * 
     */
    @Override
    public String toString() {
        String retval = "";
        boolean first = true;
        for (ExplanationUnit eu : units) {
            if (first) {
                first = false;
            } else {
                retval += "\n";
            }
            retval += eu.toString();
        }
        return retval;
    }
}
