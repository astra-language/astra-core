package astra.explanation;

import java.util.ArrayList;
import java.util.List;

import astra.core.Agent;
import astra.core.Rule;
import astra.explanation.store.BaseExplanationStack;
import astra.explanation.store.BaseExplanationUnitBuilder;
import astra.explanation.store.ExplanationStack;
import astra.explanation.store.ExplanationUnit;
import astra.explanation.store.ExplanationUnitBuilder;
import astra.term.Primitive;

public class ExplanationEngine {
	
	ExplanationStack stack;

	ExplanationUnitBuilder unitBuilder;

	ExplanationBuilder explanationBuilder;

	List<Config> config;

	Agent agent;

	public ExplanationEngine(Agent agent) {
		this.agent = agent;
		//Depending on config, create stack and builder
		this.stack = new BaseExplanationStack();
		this.unitBuilder = new BaseExplanationUnitBuilder();
		this.explanationBuilder = new ExplanationBuilder();
		this.config = new ArrayList<Config>();
	}

	public void addConfig(Config config) {
		this.config.add(config);
		if (config.getType().equals(ConfigTypes.UNIT_BUILDER)) {
			//Adding new generator 
			String generatorClassStr = config.firstParamAsString();
			try {
				Class newGenerator = Class.forName(generatorClassStr);
				System.out.println("Setting builder " + generatorClassStr);
				this.unitBuilder = (ExplanationUnitBuilder)newGenerator.newInstance();
			} catch (ClassNotFoundException ex) {
				ex.printStackTrace();
				//Hacky hack hack
				this.unitBuilder = new BaseExplanationUnitBuilder();
			} catch (InstantiationException e) {
				e.printStackTrace();
				//Hacky hack hack
				this.unitBuilder = new BaseExplanationUnitBuilder();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				//Hacky hack hack
				this.unitBuilder = new BaseExplanationUnitBuilder();
			}	
		}
		if (config.getType().equals(ConfigTypes.EXPLANATION_BUILDER)) {
			//Adding new generator 
			String generatorClassStr = config.firstParamAsString();
			try {
				Class newGenerator = Class.forName(generatorClassStr);
				System.out.println("Setting explanation builder " + generatorClassStr);
				this.explanationBuilder = (ExplanationBuilder)newGenerator.newInstance();
			} catch (ClassNotFoundException ex) {
				ex.printStackTrace();
				//Hacky hack hack
				this.explanationBuilder = new ExplanationBuilder();
			} catch (InstantiationException e) {
				e.printStackTrace();
				//Hacky hack hack
				this.explanationBuilder = new ExplanationBuilder();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				//Hacky hack hack
				this.explanationBuilder = new ExplanationBuilder();
			}	
		}
	}

	public void addExplanation(ExplanationUnit explanation) {
		stack.addExplanation(explanation);
	}

	public void addExplanations(List<ExplanationUnit> exs) {
		stack.addExplanations(exs);
	}

	public Explanation retrieveExplanation(Rule r) {
		return explanationBuilder.build(stack.retrieveExplanation(r));
	}

	public Explanation retrieveExplanation(Primitive p) {
		return explanationBuilder.build(stack.retrieveExplanation(p));
	}

	public Explanation retrieveExplanation(String tag) {
		return explanationBuilder.build(stack.retrieveExplanation(tag));
	}

	public Explanation retrieveLastExplanationFor(Rule r) {
        return explanationBuilder.build(stack.retrieveLastExplanationFor(r));
    }
	
	public Explanation retrieveLastExplanationFor(String tag){
        return explanationBuilder.build(stack.retrieveLastExplanationFor(tag));
    }

	public Explanation retrieveLastExplanationFor(Rule r, String tag) {
        return explanationBuilder.build(stack.retrieveLastExplanationFor(r, tag));
    }

	public Explanation retrieveLastExplanationFor(String tag, String value) {
        return explanationBuilder.build(stack.retrieveLastExplanationFor(tag, value));
    }

	public Explanation retrieveExplanationFor(String tag, int ID) {
		return explanationBuilder.build(stack.retrieveExplanationFor(tag, ID));
	}

	public Explanation retrieveLastFullExplanationFor(Rule r) {
		return explanationBuilder.build(stack.retrieveLastFullExplanationFor(r));
	}

	public Explanation retrieveLastFullExplanationFor(String tag, String value) {
		return explanationBuilder.build(stack.retrieveLastFullExplanationFor(tag, value));
	}

	public int newID() {
		return stack.reserveID();
	}

	public int size() {
		return stack.size();
	}

	public List<ExplanationUnit> explanations() {
		return stack.explanations();
	}

	public ExplanationStack stack() {
		return this.stack;
	}

	public ExplanationUnitBuilder unitBuilder() {
		return this.unitBuilder;
	}

	public ExplanationBuilder explanationBuilder() {
		return this.explanationBuilder;
	}

}
