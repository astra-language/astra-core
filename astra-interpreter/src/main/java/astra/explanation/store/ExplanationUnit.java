package astra.explanation.store;

import astra.core.Rule;
import astra.formula.Formula;
import astra.term.Primitive;

public class ExplanationUnit<T> {
    
    //Main components of all explanations
    private Integer ID;
    private long timestamp;
    private String tag;
   
    //Now for types:
    private Rule rule;
    private Primitive detail;

    public ExplanationUnit(String tag, T obj) {
        if (obj instanceof Rule) {
            this.rule = (Rule)obj;
        }
        if (obj instanceof Primitive) {
            this.detail = (Primitive)obj;
        }

        this.tag = tag;
        this.timestamp = System.currentTimeMillis();
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Integer getID() {
        return ID;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Primitive getDetail() {
        return detail;
    }

    public Rule getRule() {
        return rule;
    }

    public String getTag() {
        return tag;
    }

    public String toString() {
		
        String retval = "ID: " + ID + ", tag: " + tag + ", ";
        if (rule != null) {
            retval += "rule body: " + rule.statement.toString() + ", rule: " + rule.toString();
		} 
        if (detail !=null) {
            retval += "detail: " + detail.toString();
        }
        return retval;
	}
}
