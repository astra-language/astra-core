package astra.explanation.store;

import java.util.List;

import astra.core.Rule;
import astra.learn.LearningProcess;

public abstract class ExplanationUnitBuilder  {
	
    public abstract List<ExplanationUnit> build(LearningProcess l);

    public abstract List<ExplanationUnit> build(Rule r, String source, double score);

}
