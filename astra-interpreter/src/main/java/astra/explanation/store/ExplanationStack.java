package astra.explanation.store;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import astra.core.Rule;
import astra.term.Primitive;

/**
 * Container for storing explanations.
 * 
 * 
 *  
 * @author Katharine
 *
 */
public abstract class ExplanationStack implements Iterable {

	//Stack itself
	public LinkedList<ExplanationUnit> stack = new LinkedList<ExplanationUnit>();

	//Indexes - ID, to stack location 
	public Map<Integer, List<Integer>> IDtoIndex = new HashMap<Integer, List<Integer>>(); 

	//Indexes - value, ID 
	public Map<Primitive, List<Integer>> valueIDIndex = new HashMap<Primitive, List<Integer>>();
	
	//Indexes - Rule, ID 
	public Map<String, List<Integer>> ruleSignatureIDIndex = new HashMap<String, List<Integer>>();
	
	//Indexes - tag, ID
	public Map<String, List<Integer>> tagIDIndex = new HashMap<String, List<Integer>>();
	
	public Integer ID = stack.size();

	public synchronized void increment() {
        ID++;
    }

	public int reserveID() {
		int i = ID;
		increment();
		return i;
	}

	public synchronized boolean addExplanations(List<ExplanationUnit> explanations) { 
		//Set all with same ID
		for(ExplanationUnit e: explanations) {
			e.setID(ID);
			addExplanation(e);
		}
		increment();
		return true;
	}

	public synchronized boolean addExplanation(ExplanationUnit ex) {
		//Set the index
		Integer exID = ex.getID();
		if (exID == null) {
			exID = ID;
			ex.setID(ID); //Stack is 0, ID is 0.
			increment();
		}

		stack.addLast(ex);
		updateIndexes(ex, exID);
		return true;
	}

	private void updateIndexes(ExplanationUnit ex, Integer exID) {
		
		List<Integer> indexes = IDtoIndex.get(exID);
		if (indexes == null) {
			indexes = new ArrayList<Integer>();
		}
		indexes.add(stack.size()-1);
		IDtoIndex.put(exID, indexes);

		List<Integer> occurances = tagIDIndex.get(ex.getTag());
		if (occurances == null) {
			occurances = new ArrayList<Integer>();
		}
		occurances.add(exID);
		tagIDIndex.put(ex.getTag(), occurances);
		
		if (ex.getRule() != null) {
			occurances = ruleSignatureIDIndex.get(ex.getRule().event.signature());
			if (occurances == null) {
				occurances = new ArrayList<Integer>();
			}
			occurances.add(exID);
			ruleSignatureIDIndex.put(ex.getRule().event.signature(), occurances);
		}

		if (ex.getDetail() != null) {
			occurances = valueIDIndex.get(ex.getDetail());
			if (occurances == null) {
				occurances = new ArrayList<Integer>();
			}
			occurances.add(exID);
			valueIDIndex.put(ex.getDetail(), occurances);
		}
	}

	public List<ExplanationUnit> explanations() {
		List<ExplanationUnit> list = new LinkedList<ExplanationUnit>();
		list.addAll(stack);
		return list;
	}

	public void clear() {
		stack.clear();
	}

	public int size() {
		return stack.size();
	}

	@Override
	public Iterator<ExplanationUnit> iterator() {
		Iterator<ExplanationUnit> it = new Iterator<ExplanationUnit>() {

            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size() && stack.get(currentIndex) != null;
            }

            @Override
            public ExplanationUnit next() {
                return stack.get(currentIndex++);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
        return it;
	}

	public abstract List<ExplanationUnit> retrieveExplanation(Rule r);

	public abstract List<ExplanationUnit> retrieveExplanation(Primitive value);

	public abstract List<ExplanationUnit> retrieveExplanation(String tag);

	public abstract List<ExplanationUnit> retrieveLastExplanationFor(Rule r);

	public abstract List<ExplanationUnit> retrieveLastExplanationFor(String tag);

	public abstract List<ExplanationUnit> retrieveLastExplanationFor(Rule r, String tag);

	public abstract List<ExplanationUnit> retrieveLastExplanationFor(String tag, String value);

	public abstract List<ExplanationUnit> retrieveExplanationFor(String tag, int ID);

	public abstract List<ExplanationUnit> retrieveLastFullExplanationFor(String tag, String value);

	public abstract List<ExplanationUnit> retrieveLastFullExplanationFor(Rule r);
	
}

