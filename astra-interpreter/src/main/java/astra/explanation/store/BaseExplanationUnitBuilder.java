package astra.explanation.store;

import java.util.Arrays;
import java.util.List;

import astra.core.Rule;
import astra.explanation.ExplanationTag;
import astra.learn.LearningProcess;
import astra.term.Primitive;

public class BaseExplanationUnitBuilder extends ExplanationUnitBuilder {
	
	@Override
	public List<ExplanationUnit> build(LearningProcess l) {
		ExplanationUnit lpEx = new ExplanationUnit<Primitive>(ExplanationTag.KEY, Primitive.newPrimitive(l.processNamespace));
		ExplanationUnit detailEx = new ExplanationUnit<Primitive>(ExplanationTag.ALGORITHM, Primitive.newPrimitive(l.algorithmName()));
		return Arrays.asList(lpEx, detailEx);
	}
	
	@Override
	public List<ExplanationUnit> build(Rule r, String source, double score) {
		ExplanationUnit ruleEx = new ExplanationUnit<Rule>(ExplanationTag.KEY, r);
		ExplanationUnit scoreEx = new ExplanationUnit<Primitive>(ExplanationTag.VALUE, Primitive.newPrimitive(score));
		ExplanationUnit sourceEx = new ExplanationUnit<Primitive>(ExplanationTag.SOURCE, Primitive.newPrimitive(source));
		ExplanationUnit contextEx = new ExplanationUnit<Primitive>(ExplanationTag.CONTEXT, Primitive.newPrimitive(r.context.toString()));
		ExplanationUnit sigEx = new ExplanationUnit<Primitive>(ExplanationTag.SIGNATURE, Primitive.newPrimitive(r.event.signature().toString()));
		return Arrays.asList(ruleEx, scoreEx, sourceEx, contextEx, sigEx);
	}
}
