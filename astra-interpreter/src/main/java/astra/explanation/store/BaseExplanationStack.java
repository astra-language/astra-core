package astra.explanation.store;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import astra.core.Rule;
import astra.reasoner.Unifier;
import astra.term.Primitive;
import astra.term.Term;

/**
 * Container for storing explanations.
 * 
 * FIXME: many, many, many improvements needed
 * 
 *  
 * @author Katharine
 *
 */
public class BaseExplanationStack extends ExplanationStack {

	boolean trace = false;

	@Override
	public List<ExplanationUnit> retrieveExplanation(Rule rule) {
		//Return an empty list if there are no explanations
		List<ExplanationUnit> result = new ArrayList<ExplanationUnit>(); 
		
		List<Integer> IDList = ruleSignatureIDIndex.get(rule.event.signature()); // O(1)
		if (IDList != null) {
			for (int id : IDList) {
				List<Integer> stackLocations = IDtoIndex.get(id);
				for (int index : stackLocations) {
					ExplanationUnit eu = stack.get(index); //O(n)

					Rule r = eu.getRule();
					if (r == null || r.context == null) {
						continue;
					}
				
					Map<Integer, Term> bindings = Unifier.unify(r.context, rule.context, new HashMap<Integer, Term>(), null); //Complexity?
					if (bindings != null) {
						result.add(eu);
					}
				}
			}
		}

		return result;
	}

	@Override
	public List<ExplanationUnit> retrieveExplanation(Primitive value) {
		//Return an empty list if there are no explanations
		List<ExplanationUnit> result = new ArrayList<ExplanationUnit>(); 
		
		List<Integer> IDList = valueIDIndex.get(value); // O(1)
		if (IDList != null) {
			for (int id : IDList) { //Size of ID list.
				List<Integer> stackLocations = IDtoIndex.get(id); // O(1)
				for (int index : stackLocations) {
					result.add(stack.get(index)); //O(1) to add, O(n) to get.
				}
			}
		}

		return result;
	}

	@Override
	public List<ExplanationUnit> retrieveExplanation(String tag) {
		if (trace) System.out.println("Searching for " + tag);
		List<ExplanationUnit> result = new ArrayList<ExplanationUnit>(); 
		
		List<Integer> IDList = tagIDIndex.get(tag); // O(1)
		
		if (trace) System.out.println("Tag ID index size " + tagIDIndex.size());
		if (trace) System.out.println("Results for tag " + IDList.size());

		if (IDList != null) {
			for (int id : IDList) { //Size of ID list.
				if (trace) System.out.println("ID is " + id);
				List<Integer> stackLocations = IDtoIndex.get(id); // O(1)

				if (trace) System.out.println("Occurs " + stackLocations.size() +  " times in stack");
				for (int index : stackLocations) {
					if (trace) System.out.println("Stack index " + index);
					result.add(stack.get(index)); //O(1) to add, O(n) to get.
				}
			}
		}

		return result;
	}

	@Override
	public List<ExplanationUnit> retrieveLastExplanationFor(Rule rule) {
		//Return an empty list if there are no explanations
		List<ExplanationUnit> result = new ArrayList<ExplanationUnit>(); 
		
		List<Integer> IDList = getIDsForRule(rule);
		if (IDList == null) return result;
		
		Collections.sort(IDList); //Inbuilt method, sorts inascending order
		int id = IDList.get(IDList.size() - 1); //Get last item 
		List<Integer> stackLocations = IDtoIndex.get(id);
		for (int index : stackLocations) {
			ExplanationUnit eu = stack.get(index);
			Rule r = eu.getRule();
			if (r == null || r.context == null) {
				continue;
			}
			result.add(eu);
		}
		return result;
	}

	@Override
	public List<ExplanationUnit> retrieveLastFullExplanationFor(Rule rule) {
		//Return an empty list if there are no explanations
		List<ExplanationUnit> result = new ArrayList<ExplanationUnit>(); 
		
		List<Integer> IDList = getIDsForRule(rule);
		if (IDList == null) return result;
		
		Collections.sort(IDList); //Inbuilt method, sorts inascending order
		int id = IDList.get(IDList.size() - 1); //Get last item 
		List<Integer> stackLocations = IDtoIndex.get(id);
		for (int index : stackLocations) {
			result.add(stack.get(index));
		}
		return result;
	}

	@Override
	public List<ExplanationUnit> retrieveLastExplanationFor(String tag) {
		if (trace) System.out.println("Searching for the last explanation for " + tag);
		List<ExplanationUnit> result = new ArrayList<ExplanationUnit>(); 
		
		List<Integer> IDList = tagIDIndex.get(tag); // O(1)
		if (IDList == null) return result;

		if (trace) System.out.println("Tag ID key set size " + tagIDIndex.keySet().size());
		if (trace) System.out.println("Results for tag " + IDList.size());

		Collections.sort(IDList); //Inbuilt method, sorts inascending order
		int id = IDList.get(IDList.size() - 1); //Get last item 
		
		if (trace) System.out.println("Getting ID " + id);
		List<Integer> stackLocations = IDtoIndex.get(id); // O(1)
		for (int index : stackLocations) {
			result.add(stack.get(index)); //O(1) to add, O(n) to get.
		}

		return result;
	}

	@Override
	public List<ExplanationUnit> retrieveLastExplanationFor(Rule rule, String tag) {
		//Return an empty list if there are no explanations
		List<ExplanationUnit> result = new ArrayList<ExplanationUnit>(); 
		
		List<Integer> IDList = getIDsForRule(rule);
		if (IDList == null) return result;
		
		Collections.sort(IDList); //Inbuilt method, sorts inascending order
		int id = IDList.get(IDList.size() - 1); //Get last item 
		
		List<Integer> stackLocations = IDtoIndex.get(id);
		for (int index : stackLocations) {
			ExplanationUnit eu = stack.get(index);
			if (eu.getTag().equals(tag)) {
				result.add(eu);
			}
		}
		return result;
	}

	private List<Integer> getIDsForRule(Rule rule) {
		List<Integer> result = new ArrayList<Integer>(); 
		
		List<Integer> IDList = ruleSignatureIDIndex.get(rule.event.signature()); // O(1)
		
		if (IDList == null) return result;
		
		for (Integer ID: IDList) {
			List<Integer> stackLocations = IDtoIndex.get(ID);
			for (int index : stackLocations) {
				ExplanationUnit eu = stack.get(index);

				Rule r = eu.getRule();
				if (r == null || r.context == null) {
					continue;
				}
			
				Map<Integer, Term> bindings = Unifier.unify(r.context, rule.context, new HashMap<Integer, Term>(), null); //Complexity?
				if (bindings != null) {
					//THEN WE KNOW THAT THIS ID IS FOR THE RULE
					result.add(ID);
				}
			}
		}
		return result;
	}

	@Override
	public List<ExplanationUnit> retrieveLastExplanationFor(String tag, String detail) {
		//Return an empty list if there are no explanations
		List<ExplanationUnit> result = new ArrayList<ExplanationUnit>(); 
		
		List<Integer> IDList = tagIDIndex.get(tag); // O(1)
		if (IDList == null) return result;

		Collections.sort(IDList); //Inbuilt method, sorts inascending order
		Collections.reverse(IDList); //Inbuilt method, sorts descending order

		for (int i = 0; i < IDList.size(); i++) {
			if (trace) System.out.println("Tring to find " + tag + ", value " + detail + " for index " + i);
			List<Integer> stackLocations = IDtoIndex.get(IDList.get(i)); // O(1)
			for (int index : stackLocations) {
				ExplanationUnit eu = stack.get(index);
				if (trace) System.out.println("Checking" + eu);
				if (eu.getDetail() == null) {
					if (trace) System.out.println("No detail");
					continue;
				}

				//May have to trim trailing quotation marks
				if (eu.getTag().equals(tag)) {
					if (trace) System.out.println("Tags match " + tag);
					if (trace) System.out.println(eu.getDetail());
					if (trace) System.out.println(Primitive.newPrimitive(detail));
					if(eu.getDetail().equals(Primitive.newPrimitive(detail))) {
						if (trace) System.out.println("MATCH!");
						result.add(eu); //O(1) to add, O(n) to get.
						return result;
					}
					if(eu.getDetail().equals(detail)) {
						if (trace) System.out.println("Covering all bases MATCH!");
						result.add(eu); //O(1) to add, O(n) to get.
						return result;
					}
				}
			}
		}
		return result;
	}

	@Override
	public List<ExplanationUnit> retrieveExplanationFor(String tag, int ID) {
		//Return an empty list if there are no explanations
		List<ExplanationUnit> result = new ArrayList<ExplanationUnit>(); 
		
		List<Integer> stackLocations = IDtoIndex.get(ID); // O(1)
		for (int index : stackLocations) {
			ExplanationUnit eu = stack.get(index);
			if (eu.getTag().equals(tag)) {
				result.add(eu); //O(1) to add, O(n) to get.
			}
			
		}

		return result;
	}

	@Override
	public List<ExplanationUnit> retrieveLastFullExplanationFor(String tag, String detail) {
		//Return an empty list if there are no explanations
		List<ExplanationUnit> result = new ArrayList<ExplanationUnit>(); 
		
		List<Integer> IDList = tagIDIndex.get(tag); // O(1)
		if (IDList == null) return result;

		Collections.sort(IDList); //Inbuilt method, sorts inascending order
		Collections.reverse(IDList); //Inbuilt method, sorts descending order

		for (int i = 0; i < IDList.size(); i++) {
			if (trace) System.out.println("Tring to find " + tag + ", detail " + detail + " for index " + i);
			List<Integer> stackLocations = IDtoIndex.get(IDList.get(i)); // O(1)
			boolean match = false;
			for (int index : stackLocations) {
				ExplanationUnit eu = stack.get(index);
				if (trace) System.out.println("Checking" + eu);
				if (eu.getDetail() == null) {
					continue;
				}

				//May have to trim trailing quotation marks
				if (eu.getTag().equals(tag) && eu.getDetail().equals(Primitive.newPrimitive(detail))) {
					if (trace) System.out.println("MATCH!");
					//This is the ID we want. So want to go through this again
					match = true;
					break; //Break out of this loop
				}
			}
			if (match) {
				for (int index : stackLocations) {
					result.add(stack.get(index));
				}
				break; //No need to keep iterating
			}
		}
		return result;
	}

}

