package astra.explanation;

public class ExplanationTag {
    
    //General
    public static final String KEY = "key";
    public static final String DETAIL = "detail";
    
    //Leanring process specific
    public static final String ALGORITHM = "algorithm";
    //Rule specific
    public static final String VALUE = "value";
    public static final String SIGNATURE = "signature";
    public static final String CONTEXT = "context";
    public static final String SOURCE = "source";
}
