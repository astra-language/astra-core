package astra.explanation;

import java.util.List;

/*
 * Configuration Element for the Explanation Engine
 * 
 * e.g.
 *   stack("explanation.KnowledgeGraphExplanationStack"); type="stack"
 *   generator("explanation.DomainGraphExplanationGenerator");
 *   map("ContextPrefix","My");
 *   map("TriplePrefix","#failure","I have a");
 */
public class ConfigTypes {
    
    public static final String STACK = "stack";
    public static final String UNIT_BUILDER = "unitBuilder";
    public static final String EXPLANATION_BUILDER = "explanationBuilder";
    public static final String MAP = "map";
}
