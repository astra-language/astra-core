package astra.explanation;

import java.util.ArrayList;
import java.util.List;

/*
 * Configuration Element for the Explanation Engine
 * 
 * e.g.
 *   stack("explanation.KnowledgeGraphExplanationStack"); type="stack"
 *   generator("explanation.DomainGraphExplanationGenerator");
 *   map("ContextPrefix","My");
 *   map("TriplePrefix","#failure","I have a");
 */
public class Config {
    
    private String type;
    private Object[] params;

    public Config(String type, Object ... params) {
        this.type = type;
        this.params = params;
    }

    public String getType() {
        return type;
    }

    public Object[] getParams() {
        return params;
    }

    public String firstParamAsString() {
        return (String)params[0];
    }
}
