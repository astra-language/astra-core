package astra.event;

import astra.formula.TestGoal;
import astra.reasoner.util.LogicVisitor;

public class TestGoalEvent implements Event {
	public char type;
	public TestGoal goal;
	public Object source;

	public TestGoalEvent(char type, TestGoal goal) {
		this(type, goal, null);
	}

	public TestGoalEvent(char type, TestGoal goal, Object source) {
		this.type = type;
		this.goal = goal;
		this.source = source;
	}

	public char type() {
		return type;
	}

	public TestGoal goal() {
		return goal;
	}

	public String toString() {
		return type + goal.toString();
	}

	public Object getSource() {
		return source;
	}

	public String signature() {
		return "TGE:" + type + ":" + goal.formula().id() + ":" + goal.formula().terms().length;
	}

	@Override
	public Event accept(LogicVisitor visitor) {
		return new TestGoalEvent(type, (TestGoal) goal.accept(visitor), source);
	}
}
