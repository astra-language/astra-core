package astra.docker;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import astra.core.Module;
import astra.formula.Formula;
import astra.formula.Predicate;

/**
 * Manage credentials for Docker.
 * 
 * File Name: *.cred
 */
public class Credentials extends Module {
    Map<String, Map<String, String>> credentials = new HashMap<>();

    /**
     * Load a Credentials file
     * @param file
     * @return
     */
    @ACTION
    public boolean load(String file) {
        credentials.clear();
        try {
            String line = null;
            BufferedReader in = new BufferedReader(new FileReader(file));
            while ((line = in.readLine()) != null) {
                String[] bits = line.split(":");
                if (bits.length != 3) {
                    System.out.println("WARNING: Invalid Entry in credentials file: " + line);
                    in.close();
                    return false;
                }

                Map<String, String> usernames = credentials.get(bits[0]);
                if (usernames == null) {
                    usernames = new HashMap<>();
                    credentials.put(bits[0],usernames);
                }
                usernames.put(bits[1],bits[2]);
            }
            in.close();
        } catch (FileNotFoundException fnfe) {
            System.out.println("WARNING: Could not file file: " + file);
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }

    @FORMULA
    public Formula hasCredentials(String repositoryUrl, String username) {
        Map<String,String> usernames = credentials.get(repositoryUrl);
        if (usernames == null) return Predicate.FALSE;
        return usernames.containsKey(username) ? Predicate.TRUE:Predicate.FALSE;
    }

    @TERM
    public String getPasskey(String repositoryUrl, String username) {
        Map<String,String> usernames = credentials.get(repositoryUrl);
        if (usernames == null) return null;
        return usernames.get(username);
    }

    @ACTION
    public boolean add(String repositoryUrl, String username, String passkey) {
        Map<String,String> usernames = credentials.get(repositoryUrl);
        if (usernames == null) {
            usernames = new HashMap<>();
            credentials.put(repositoryUrl, usernames);
        }
        usernames.put(username, passkey);
        return true;
    }

}
