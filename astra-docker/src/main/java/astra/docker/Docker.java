package astra.docker;

import java.time.Duration;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.async.ResultCallback;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.AuthConfig;
import com.github.dockerjava.api.model.AuthResponse;
import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.HostConfig;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.api.model.Volume;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.DockerClientImpl;
import com.github.dockerjava.httpclient5.ApacheDockerHttpClient;
import com.github.dockerjava.transport.DockerHttpClient;

import astra.core.Module;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;


public class Docker extends Module { 
    private DockerClient client;

    public Docker() {
        this(DefaultDockerClientConfig.createDefaultConfigBuilder().build());
    }

    public Docker(DockerClientConfig config) {
        DockerHttpClient httpClient = createDockerHttpClient(config);
        client = DockerClientImpl.getInstance(config, httpClient);   
    }

    private DockerHttpClient createDockerHttpClient(DockerClientConfig config) {
        return new ApacheDockerHttpClient.Builder()
            .dockerHost(config.getDockerHost())
            .sslConfig(config.getSSLConfig())
            .maxConnections(100)
            .connectionTimeout(Duration.ofSeconds(30))
            .responseTimeout(Duration.ofSeconds(45))
            .build();        
    }

    @FORMULA
    public Formula isLocal(String imageUrl) {
        try {
            client.inspectImageCmd(imageUrl).exec();
            return Predicate.TRUE;
        } catch (RuntimeException e) {
            // e.printStackTrace();
            return Predicate.FALSE;
        }
    }

    @ACTION
    public boolean pull(String imageUrl) {
        try {
            client
                .pullImageCmd(imageUrl)
                .start()
                .awaitCompletion();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @ACTION
    public boolean pull(AuthConfig authConfig, String imageUrl) {
        try {
            client
                .pullImageCmd(imageUrl)
                .withAuthConfig(authConfig)
                .start()
                .awaitCompletion();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @TERM
    public String createContainer(String imageUrl, String name) {
        CreateContainerResponse response = client
            .createContainerCmd(imageUrl)
            .withName(name)
            .exec();
        return response.getId();
    }

    @ACTION
    public boolean addPortBinding(HostConfig hostConfig, String binding) {
        hostConfig.withPortBindings((PortBinding.parse(binding)));
        return true;
    }

    @ACTION
    public boolean addVolume(HostConfig hostConfig, String volumePath, String bindPath) {
        hostConfig.withBinds(new Bind(System.getProperty("user.dir") + bindPath, new Volume(volumePath)));
        return true;
    }

    @TERM
    public HostConfig createHostConfig() {
        return HostConfig.newHostConfig();
    }

    @TERM
    public String createContainer(String imageUrl, HostConfig hostConfig) {
        return createContainer(imageUrl, hostConfig, new ListTerm());
    }

    @SuppressWarnings("unchecked")
    @TERM
    public String createContainer(String imageUrl, HostConfig hostConfig, ListTerm envs) {
        List<String> list = new LinkedList<>();
        for (Term term : envs) {
            list.add(((Primitive<String>) term).value());
        }

        CreateContainerResponse response = client
            .createContainerCmd(imageUrl)
            .withHostConfig(hostConfig)
            .withEnv(list)
            .exec();
        return response.getId();
    }

    @SuppressWarnings("unchecked")
    @TERM
    public String createContainer(String imageUrl, HostConfig hostConfig, ListTerm envs, ListTerm cmd) {
        List<String> list = new LinkedList<>();
        for (Term term : envs) {
            list.add(((Primitive<String>) term).value());
        }
        
        List<String> list2 = new LinkedList<>();
        for (Term term : cmd) {
            list2.add(((Primitive<String>) term).value());
        }
        CreateContainerResponse response = client
            .createContainerCmd(imageUrl)
            .withTty(true)
            .withStdinOpen(true)
            .withHostConfig(hostConfig)
            .withEnv(list)
            .withWorkingDir("/home/airflow/")
            .withEntrypoint(list2)
            .exec();
        
        return response.getId();
    }

    @TERM
    public String createContainer(String imageUrl) {
        CreateContainerResponse response = client
            .createContainerCmd(imageUrl)
            .exec();
        return response.getId();
    }

    @ACTION
    public boolean watchContainer(String id) {
        // Attach to container to see output (optional)
        client.attachContainerCmd(id)
            .withStdErr(true)
            .withStdOut(true)
            .withFollowStream(true)
            .withLogs(true)
            .exec(new ResultCallback.Adapter<>());

        // Wait for container to finish
        client.waitContainerCmd(id).exec(new ResultCallback.Adapter<>());     
        return true;   
    }

    @ACTION
    public boolean startContainer(String id) {
        client
            .startContainerCmd(id)
            .exec();
        return true;
    }

    private ObjectMapper mapper = new ObjectMapper();

    @TERM
    public ListTerm listImages(){
        ListTerm imagesList = new ListTerm();
        try {
            List<Image> images = client.listImagesCmd().exec();
            for (Image image : images) {
                imagesList.add(Primitive.newPrimitive(mapper.writeValueAsString(image)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imagesList;
    }

    @TERM
    public ListTerm listImages(int offset, int limit){
        ListTerm imagesList = new ListTerm();

        int i = 0;
        try {
            List<Image> images = client.listImagesCmd().exec();
            for (Image image : images) {
                if (i >= offset && i < offset+limit)
                    imagesList.add(Primitive.newPrimitive(mapper.writeValueAsString(image)));
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imagesList;
    }

    @TERM
    public AuthConfig createAuthConfig(String registryUrl, String username, String accessToken) {
        return new AuthConfig()
            .withUsername(username)
            .withPassword(accessToken)
            .withRegistryAddress("https://" + registryUrl);
    }

    @ACTION
    public boolean auth(AuthConfig authConfig) {
        try {
            AuthResponse response = client.authCmd().withAuthConfig(authConfig).exec();
            System.out.println("Auth Response: " + response.getStatus());
        } catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @FORMULA
    public Formula hasContainer(String image) {
        try {
            List<Container> containers = client.listContainersCmd().exec();
            for (Container container: containers) {
                if (container.getImage().equals(image)) return Predicate.TRUE;
            }
        } catch (RuntimeException e) {
        }
        return Predicate.FALSE;
    }

    @FORMULA
    public Formula hasContainer(String image, String id) {
        try {
            List<Container> containers = client.listContainersCmd().exec();
            for (Container container: containers) {
                if (container.getImage().equals(image) && container.getId().equals(id)) return Predicate.TRUE;
            }
        } catch (RuntimeException e) {
        }
        return Predicate.FALSE;
    }

    @ACTION
    public boolean close() {
        try{
            client.close();
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

//     /*
//     * Image Code
//     */


//    @ACTION
//    public boolean searchImages(String term){
//        try {
//            List<SearchItem> items = dockerClient.searchImagesCmd(term).exec();
//            // Print out the IDs of the images
//            if(!items.isEmpty()){
//                for (SearchItem item : items) {
//                    System.out.println("SearchItem Name: " + item.getName());
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }

//        return true;
//    }


//    @TERM 
//    public String getImageId(Image image){
//        return image.getId();
//    }

//    /*
//     * Container Code
//     */

//     @TERM
//     public ListTerm listContainers(){
//         ListTerm containersList = new ListTerm();
//         try {
//             List<Container> containers = dockerClient.listContainersCmd().exec();
//             // Print out the IDs of the images
//             for (Container container : containers) {
//                 // System.out.println("Container ID: " + container.getId());
//                 containersList.add(Primitive.newPrimitive(container));
//             }
//         } catch (Exception e) {
//             e.printStackTrace();
//         }
//         return containersList;
//     }

//     @TERM
//     public String createContainer(String imageID){
//         try {
//             CreateContainerResponse response = dockerClient.createContainerCmd(imageID).exec();
//             System.out.println("Container ID: "+response.getId());
//             return response.getId();
//             // Print out the IDs of the images
//         } catch (Exception e) {
//             e.printStackTrace();
//         }
//         return "No ID";
//     }

//     @TERM
//     public String createContainerWithArgs(String imageID, ListTerm args){
//         try {

//             // CreateContainerResponse response = dockerClient.createContainerCmd(imageID)
//             //                                     .withCmd("bash", args.get(0).toString(),
//             //                                     args.get(1).toString(),
//             //                                     args.get(2).toString(), 
//             //                                     args.get(3).toString(),
//             //                                     args.get(4).toString(),
//             //                                     args.get(5).toString(),
//             //                                     args.get(6).toString(),
//             //                                     args.get(7).toString(),
//             //                                     args.get(8).toString(),
//             //                                     args.get(9).toString())
//             //                                     .withTty(true)
//             //                                     .exec();


//             CreateContainerResponse container = dockerClient.createContainerCmd("sentinel_download")
//             .withName("download_sentinel_container")
//             .withTty(true)
//             .withStdinOpen(true)
//             .withHostConfig(HostConfig.newHostConfig()
//                 .withAutoRemove(true)
//                 .withBinds(new Bind(System.getProperty("user.dir") + "/write_storage", 
//                                     new Volume("/home/airflow/tasks/write_storage"))))
//             .withCmd("bash", "/scripts/download_sentinel.sh", 
//                      "/home/airflow/tasks/write_storage", "SENTINEL-1", "S1A_IW_GRDH_1SDV", 
//                      "COG", "creds.json", "aoi.geojson", "2018-01-05T00:00:00Z", 
//                      "2018-01-05T23:59:59Z", "out")
//             .exec();

//         // Start container
//         dockerClient.startContainerCmd(container.getId()).exec();

//         // Attach to container to see output (optional)
//         dockerClient.attachContainerCmd(container.getId())
//             .withStdErr(true)
//             .withStdOut(true)
//             .withFollowStream(true)
//             .withLogs(true)
//             .exec(new ResultCallback.Adapter<>());

//         // Wait for container to finish
//         dockerClient.waitContainerCmd(container.getId()).exec(new ResultCallback.Adapter<>());


//             System.out.println("Container ID: "+container.getId());
//             return container.getId();
//             // Print out the IDs of the images
//         } catch (Exception e) {
//             e.printStackTrace();
//         }
//         return "No ID";
//     }

//     @ACTION
//     public boolean stopContainer(String containerID){
//         try {
//             dockerClient.stopContainerCmd(containerID).exec();
//             // Print out the IDs of the images
//         } catch (Exception e) {
//             e.printStackTrace();
//             return false;
//         }

//         return true;
//     }
}
