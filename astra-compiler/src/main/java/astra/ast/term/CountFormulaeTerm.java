package astra.ast.term;

import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.IFormula;
import astra.ast.core.ITerm;
import astra.ast.core.IType;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.type.BasicType;

public class CountFormulaeTerm extends AbstractElement implements ITerm {
	IFormula formula;
	
	public CountFormulaeTerm(IFormula formula, Token start, Token end, String source) {
		super(start, end, source);
		this.formula = formula;
	}

	public IFormula formula() {
		return formula;
	}

	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public String toString() {
		return formula.toString();
	}

	public IType type() {
		return new BasicType(Token.INTEGER);
	}
}
