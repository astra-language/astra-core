package astra.ast.term;

import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.ITerm;
import astra.ast.core.IType;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.type.BasicType;

public class NotTerm extends AbstractElement implements ITerm {
	ITerm term;
	IType type = new BasicType(Token.BOOLEAN);
	
	public NotTerm(ITerm term, Token start, Token end, String source) {
		super(start, end, source);
		this.term = term;
	}

	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public ITerm term() {
		return term;
	}

	public IType type() {
		return type;
	}
	
	public String toString() {
		return "~" + term;
	}
}
