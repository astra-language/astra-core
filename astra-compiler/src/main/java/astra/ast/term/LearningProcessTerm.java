package astra.ast.term;

import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.ITerm;
import astra.ast.core.IType;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.formula.PredicateFormula;

public class LearningProcessTerm extends AbstractElement implements ITerm {
	PredicateFormula method;
	String learningProcessNamespace;
	IType type;
	
	public LearningProcessTerm(String learningProcessNamespace, PredicateFormula method, Token start, Token end, String source) {
    	super(start, end, source);
        this.learningProcessNamespace = learningProcessNamespace;
		this.method = method;
	}

	public PredicateFormula method() {
		return method;
	}

	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public String learningProcessNamespace() {
		return learningProcessNamespace;
	}
	
	public String toString() {
		return learningProcessNamespace + "#" + method;
	}

	public void setType(IType type) {
		this.type = type;
	}
	
	public IType type() {
		return type;
	}
}