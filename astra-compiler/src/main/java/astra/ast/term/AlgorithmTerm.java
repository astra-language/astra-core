package astra.ast.term;

import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.ITerm;
import astra.ast.core.IType;
import astra.ast.core.ParseException;
import astra.ast.core.Token;

public class AlgorithmTerm extends AbstractElement implements ITerm {
	
    Function parameter;
	String algorithmName;
	IType type;
	
	public AlgorithmTerm(String algorithmName, Function parameter, Token start, Token end, String source) {
		super(start, end, source);
		this.algorithmName = algorithmName;
		this.parameter = parameter;
	}

	public Function parameter() {
		return parameter;
	}

	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public String algorithm() {
		return algorithmName;
	}

	public Function getParamter() {
		return parameter;
	}
	
	
	public String toString() {
		return algorithmName + "(" + parameter + ")";
	}

	public void setType(IType type) {
		this.type = type;
	}
	
	public IType type() {
		return type;
	}
}