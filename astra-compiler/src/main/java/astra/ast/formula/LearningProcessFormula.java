package astra.ast.formula;

import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.IFormula;
import astra.ast.core.ParseException;
import astra.ast.core.Token;

public class LearningProcessFormula extends AbstractElement implements IFormula {
	PredicateFormula formula;
	String learningProcess;
	
	public LearningProcessFormula(String learningProcess, PredicateFormula formula, Token start, Token end, String source) {
		super(start, end, source);
		this.learningProcess = learningProcess;
		this.formula = formula;
	}

	@Override
	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public PredicateFormula method() {
		return formula;
	}
	
	public String learningProcess() {
		return learningProcess;
	}
	
	public String toString() {
		return learningProcess + "#" + formula;
	}
	
	public String toSignature() {
		return "";
	}
}
