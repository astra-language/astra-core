package astra.ast.formula;

import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.IFormula;
import astra.ast.core.ITerm;
import astra.ast.core.ParseException;
import astra.ast.core.Token;

public class BooleanTermElement extends AbstractElement implements IFormula {
	ITerm term;
	
	public BooleanTermElement(ITerm term, Token start, Token end, String source) {
		super(start, end, source);
		
		this.term = term;
	}
    
    @Override
    public Object accept(IElementVisitor visitor, Object data) throws ParseException {
        return visitor.visit(this, data);
    }

    public ITerm term() {
        return term;
    }

    @Override
    public String toSignature() {
        return "$bte";
    }
    
}
