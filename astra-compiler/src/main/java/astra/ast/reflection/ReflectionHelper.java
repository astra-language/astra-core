package astra.ast.reflection;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import astra.ast.core.ASTRAClassElement;
import astra.ast.core.AbstractHelper;
import astra.ast.core.BuildContext;
import astra.ast.core.IJavaHelper;
import astra.ast.core.ITerm;
import astra.ast.core.ImportElement;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.element.PackageElement;
import astra.ast.formula.MethodSignature;
import astra.ast.formula.MethodType;
import astra.ast.formula.PredicateFormula;
import astra.ast.term.VariableElement;
import astra.ast.type.ObjectType;
import astra.core.ActionParam;

public class ReflectionHelper extends AbstractHelper {
	// private static Logger log = LoggerFactory.getLogger(ReflectionHelper.class);

	private static Map<Integer, String> annotations = new HashMap<Integer, String>();

	static {
		annotations.put(ACTION, "astra.core.InternalAffordances.ACTION");
		annotations.put(TERM, "astra.core.InternalAffordances.TERM");
		annotations.put(FORMULA, "astra.core.InternalAffordances.FORMULA");
		annotations.put(SENSOR, "astra.core.Module.SENSOR");
		annotations.put(EVENT, "astra.core.Module.EVENT");
	}

	PackageElement packageElement;
	ImportElement[] imports;
	BuildContext context = new BuildContext();
	private File source;
	private File target;

	public ReflectionHelper(File source, File target) {
		this.source = source;
		this.target = target;
	}

	public BuildContext getBuildContext() {
		return context;
	}

	public String resolveModule(String className) {
		Class<?> clazz = resolveClass(className);
		return clazz == null ? null : clazz.getCanonicalName();
	}

	public Class<?> resolveClass(String clazz) {
		// Start by checking if it is a fully qualified class name
		// The loop is used in case the class is an inner class (hence '.' replaced by '$')
		String im = clazz;
		// System.out.println("["+source.getAbsolutePath()+"] Checking: " + im);
		Class<?> cls = recursiveClassSearch(im);
		if (cls != null) return cls;

		// Try with the package name prefixed...
		try {
			if (packageElement != null)
				return Class.forName(packageElement.packageName() + "." + clazz);
		} catch (ClassNotFoundException e0) {
		}

		// Try astra.lang.*
		try {
			return Class.forName("astra.lang." + clazz);
		} catch (ClassNotFoundException e0) {
		}

		// Try java.lang.*
		try {
			return Class.forName("java.lang." + clazz);
		} catch (ClassNotFoundException e0) {
		}

		// Try to match against an import...
		for (ImportElement imp : imports) {
			// System.out.println("\tOption: " + imp.name());
			im = imp.name();
			if (im.endsWith(clazz)) {
				cls = recursiveClassSearch(im);
				if (cls != null) return cls;

			}

			// Extra check for imported packages...
			if (im.endsWith("*")) {
				// Remote the * and add the classname...
				im = im.substring(0, im.length() - 1) + clazz;
				cls = recursiveClassSearch(im);
				if (cls != null) return cls;
			}
		}
		try {
			return Class.forName("astra.lang." + clazz);
		} catch (ClassNotFoundException e2) {
		}

		//System.out.println("Package: " + packageElement.packageName());
		throw new RuntimeException("Could not match class: " + clazz);
		// return null;
	}

	private Class<?> recursiveClassSearch(String im) {
		boolean finished = false;
		while (!finished) {
			try {
				return Class.forName(im);
			} catch (ClassNotFoundException e1) {
			}
			int index = im.lastIndexOf('.');
			if (index > 0) {
				im = im.substring(0, index)+"$"+im.substring(index+1);
			} else {
				finished = true;
			}
		}
		return null;
	}
	public void setup(PackageElement packageElement, ImportElement[] imports) {
		this.packageElement = packageElement;
		this.imports = imports;
	}

	public String getFullClassName(String className) {
		return resolveClass(className).getCanonicalName();
	}

	public String getQualifiedName(String parent, String packageName, ImportElement[] imports) {
		// System.out.println("[ReflectionHelper] parent: "  + parent);
		// System.out.println("[ReflectionHelper] packageName: "  + packageName);

		if (parent.contains(".")) return parent;

		if (!packageName.isEmpty()) {
			String qname = "/" + packageName.replace(".", "/") + "/" + parent + ".astra";
			// System.out.println("Possible package: " + qname);
			if (classExists(qname)) return packageName + "." + parent;
			// System.out.println("FAILED");
		} else {
			String qname = "/" + parent + ".astra";
			// System.out.println("In same package: " + qname);
			if (classExists(qname)) return parent;
		}
		String qname = "/astra/lang/" + parent.replace(".", "/") + ".astra";
		if (classExists(qname)) return "astra.lang." + parent;


		for (ImportElement imp : imports) {
			String im = imp.name();
			// System.out.println("Testing: " + im);;
			if (im.endsWith("*")) {
				im = im.substring(0, im.length() - 1) + parent;
				// System.out.println("Refined: " + im);;
			}

			if (im.endsWith(parent)) {
				if (classExists("/" + im.replace(".", "/") + ".astra")) return im;
				// System.out.println("Failed");
			}
		}
		return parent;
	}

	private boolean classExists(String qname) {
		// File file = getSourceFileReference(qname);
		// System.out.println("file: " +file);
		return getSourceFileReference(qname).exists() || getClass().getResourceAsStream(qname) != null;
	}

	public ASTRAClassElement loadAST(String clazz) throws ParseException {		
		File file = getSourceFileReference(clazz.replace(".", "/") + ".astra");
		return file.exists() ? loadASTFromFile(clazz, file):loadASTFromJar(clazz);
	}

	private ASTRAClassElement loadASTFromFile(String clazz, File file) throws ParseException {
		try {
			InputStream in = new FileInputStream(file);
			ASTRAClassElement element = new ASTRAClassElement(clazz, in, true);
			in.close();
			return element;
		} catch (FileNotFoundException e) {
			return loadASTFromJar(clazz);
		} catch (IOException e) {
			return loadASTFromJar(clazz);
		} catch (RuntimeException rte) {
			System.out.println("ERROR Parsing Class: " + clazz);
			throw rte;
		}
	}

	private ASTRAClassElement loadASTFromJar(String clazz) throws ParseException {
		InputStream in = getClass().getResourceAsStream("/" + clazz.replace(".", "/") + ".astra");

		if (in == null) return null;
		return new ASTRAClassElement(clazz, in, true);
	}

	private boolean matchMethodSignature(Method mthd, MethodSignature signature) {
		return mthd.getName().equals(signature.name()) &&
				 signature.termCount() == (mthd.getParameterTypes().length-(signature.symbol() ? 1:0));
	}

	private Method getMatchingMethod(String moduleClass, MethodSignature signature) {
		Class<?> cls = resolveClass(moduleClass);
		//System.out.println("Resolved class " + cls);
		if (cls == null) {
			return null;
		}
		while (cls.getSuperclass() != null) {
			for (Method mthd : cls.getMethods()) {
				if (matchMethodSignature(mthd, signature)) {
					//System.out.println("Match method sig for mthd  " + mthd);
					//System.out.println(" sig  " + signature);
					if (signature.type() == -1) {
						//System.out.println("Type is -1  ");
						if (validateSignature(signature, mthd, null)) return mthd;
					} else {
						//Annotation[] ans = mthd.getAnnotations();
						//System.out.println("Have " + ans.length);
						for (Annotation ann : mthd.getAnnotations()) {
							//System.out.println(" Annotation  " + ann.annotationType().getCanonicalName());
							//System.out.println(" Map of annotations  " + annotations.get(signature.type()));
							if (ann.annotationType().getCanonicalName().equals(annotations.get(signature.type()))
								&& validateSignature(signature, mthd, ann)) {
									//System.out.println("Found match!  " + mthd);
									return mthd;
							}
						}
					}
				}
			}
			cls = cls.getSuperclass();
		}
		// System.out.println("FAILED");
		return null;
	}

	public boolean validate(String moduleClass, MethodSignature signature) {
		return getMatchingMethod(moduleClass, signature) != null;
	}

	public boolean isInline(String moduleClass, MethodSignature signature) {
		Method method = getMatchingMethod(moduleClass, signature);
		if (method == null) return true;
		for (Annotation ann : method.getAnnotations()) {
			String cannonicalName = ann.annotationType().getCanonicalName();
			if (cannonicalName.equals(annotations.get(signature.type())))
			{
				if (signature.type() == IJavaHelper.ACTION) {
					return ((astra.core.InternalAffordances.ACTION) ann).inline();
				}
			}
		}
		return false;
	}

	private boolean validateSignature(MethodSignature signature, Method mthd, Annotation ann) {
		//System.out.println("in validateSignature " + signature);
		//System.out.println(" method " + mthd);
		//System.out.println(" params " + ann);
		//System.out.println(" signature.type() " + signature.type());
		switch (signature.type()) {
			
			case IJavaHelper.EVENT:
				return validateEventSignature(signature, mthd, ann);
			case IJavaHelper.FORMULA:
				// Check for new Formula model (queryable==true)
				if (isNewFormulaModel(signature, mthd, ann)) {
					return validateFormulaSignature(signature, mthd, ann);
				}
				// If not, revert to the old Formula model...
			default:
				//System.out.println("default: ");
				Type[] params = mthd.getGenericParameterTypes();
				int i = 0;
				boolean match = true;
				while (match && i < params.length) {
					match = matchType(params[i], signature.type(i));
					i++;
				}
		
				if (match) {
					String retType = mthd.getReturnType().getCanonicalName();
					String t = MethodType.resolveType(retType);
					if (t == null) {
						signature.returnType(retType);
					} else {
						signature.returnType(t);
					}
					return true;
				}
				return false;
		}
	}

	private boolean validateEventSignature(MethodSignature signature, Method mthd, Annotation ann) {
		String[] params = ((astra.core.Module.EVENT) ann).types();
		int i = 0;
		boolean match = true;
		while (match && i < params.length) {
			match = params[i].equals(signature.type(i).type());
			if (!match) {
				// Try class match
				match = resolveClass(params[i]).equals(resolveClass(signature.type(i).type()));
			}
			i++;
		}

		if (match) {
			signature.signature(((astra.core.Module.EVENT) ann).signature());
			String retType = mthd.getReturnType().getCanonicalName();
			String t = MethodType.resolveType(retType);
			if (t == null)
				signature.returnType(retType);
			else
				signature.returnType(t);
		}

		return match;
	}

	private boolean isNewFormulaModel(MethodSignature signature, Method mthd, Annotation ann) {
		return ((astra.core.InternalAffordances.FORMULA) ann).types().length == mthd.getParameterTypes().length;

	}
	private boolean validateFormulaSignature(MethodSignature signature, Method mthd, Annotation ann) {
		String[] params = ((astra.core.InternalAffordances.FORMULA) ann).types();
		int i = 0;
		boolean match = true;
		while (match && i < params.length) {
			match = params[i].equals(signature.type(i).type());
			signature.type(i).primitiveType("astra.term.Term");
			i++;
		}

		if (match) {
			// signature.signature(((FORMULA) ann).signature());
			String retType = mthd.getReturnType().getCanonicalName();
			String t = MethodType.resolveType(retType);
			if (t == null)
				signature.returnType(retType);
			else
				signature.returnType(t);
		}

		return match;
	}

	private boolean matchType(Type cls, MethodType methodType) {
    //    System.out.println("Matching: " + cls + " and: " + methodType);
		if (cls instanceof ParameterizedType) {
			if (methodType.variable()) {
				ParameterizedType t = (ParameterizedType) cls;
				if (t.getRawType().equals(ActionParam.class)) {
					boolean result = matchType(t.getActualTypeArguments()[0], methodType);
					if (result) {
						methodType.actionParam(true);
					}
					return result;
				}
			} else if (methodType.isFunct()) {
				ParameterizedType t = (ParameterizedType) cls;
				if (t.getRawType().equals(ActionParam.class)) {
					boolean result = matchType(t.getActualTypeArguments()[0], methodType);
					if (result) {
						methodType.actionParam(true);
					}
					return result;
				}

			}
		}

		// handle primitives
		if (methodType.isPrimitive()) {
			// System.out.println(">>>>>>> PRIMITIVE");
			return methodType.validatePrimitive(((Class<?>) cls).getCanonicalName());
		}

		// validate raw types...
		// System.out.println("method.type="+methodType.type());
		Class<?> cl = resolveClass(methodType.type());
		// System.out.println("cl: " + cl);
		if ((cl != null) && ((Class<?>) cls).isAssignableFrom(cl)) {
			methodType.primitiveType(cl.getCanonicalName());
			return true;
		}
		return false;
	}

	public List<String> getSensors(String name) {
		List<String> list = new LinkedList<String>();

		Class<?> cls = resolveClass(name);
		// System.out.println("class: " + name + " / cls=" + cls);
		for (Method mthd : cls.getMethods()) {
			// System.out.println("\tmethod: " + mthd);
			for (Annotation ann : mthd.getAnnotations()) {
				// System.out.println("\t\tannotation: " + ann);
				if (mthd.getParameterTypes().length == 0 && ann.toString().startsWith("@astra.core.Module$SENSOR")) {

					list.add(mthd.getName());
				}
			}
		}

		return list;
	}

	public IJavaHelper spawn() {
		return new ReflectionHelper(source, target);
	}

	public long lastModified(String clazz, String type) {
		String filename = clazz.replace(".", "/") + type;
		
		// Try to check if the file is local (source) first
		File file = getSourceFileReference(filename);
		if (file.exists()) {
			// System.out.println("LOCAL FILE: " + filename + " / Last Modified: "  + file.lastModified());
			return file.lastModified();
		}

		// Try to check if the file is local (target) second
		file = getTargetFileReference(filename);
		if (file.exists()) {
			// System.out.println("LOCAL FILE: " + filename + " / Last Modified: "  + file.lastModified());
			return file.lastModified();
		}

		// Okay, so it isn't local, so search the classpath...
		URL url = getClass().getResource("/" + filename);
		// url will be null if there is not resource with the given name...
		if (url == null) {
//			log.warn("NOT IN LOCAL CLASSPATH: " + filename + " / Last Modified: 0");
			return 0;
		}
		
		// okay, get the resources last modified date...
		try {
			long lastModified = url.openConnection().getLastModified();
//			log.warn("IN LOCAL CLASSPATH: " + filename + " / Last Modified: " + lastModified);
			return lastModified;
		} catch (IOException e) {
			return 0;
		}
	}

	private File getSourceFileReference(String filename) {
		return new File(source, filename);
	}

	private File getTargetFileReference(String filename) {
		return new File(target, filename);
	}

	public boolean hasAutoAction(String className) {
		return validate(className, getAutoMethodSignature());
	}

	public boolean hasTRAutoAction(String className) {
		return validate(className, getAutoTRMethodSignature()); 
	}

	private MethodSignature getAutoMethodSignature() {
		VariableElement V = new VariableElement("X", null, null, null);
		V.setType(new ObjectType(Token.OBJECT_TYPE, "astra.formula.Predicate"));
		VariableElement V2 = new VariableElement("Y", null, null, null);
		V2.setType(new ObjectType(Token.OBJECT_TYPE, "astra.core.Intention"));
		return new MethodSignature(
				new PredicateFormula("auto_action", Arrays.asList((ITerm) V2, (ITerm) V), null, null, null), -1);
	}
	
	private MethodSignature getAutoTRMethodSignature() {
		VariableElement V = new VariableElement("X",null,null,null);
		V.setType(new ObjectType(Token.OBJECT_TYPE, "astra.formula.Predicate"));
		VariableElement V2 = new VariableElement("Y",null,null,null);
		V2.setType(new ObjectType(Token.OBJECT_TYPE, "astra.tr.TRContext"));
		return new MethodSignature(
				new PredicateFormula("auto_action", 
						Arrays.asList((ITerm) V2, (ITerm) V),
						null, null, null
				),
				-1
		);
	}
	
	public boolean hasAutoFormula(String className) {
		VariableElement V = new VariableElement("X", null, null, null);
		V.setType(new ObjectType(Token.OBJECT_TYPE, "astra.formula.Predicate"));
		return validate(className, new MethodSignature(
				new PredicateFormula("auto_formula", Arrays.asList((ITerm) V), null, null, null), -1));
	}

	public boolean hasAutoEvent(String className) {
		VariableElement V = new VariableElement("X", null, null, null);
		V.setType(new ObjectType(Token.OBJECT_TYPE, "String"));
		VariableElement V2 = new VariableElement("Y", null, null, null);
		V2.setType(new ObjectType(Token.OBJECT_TYPE, "astra.formula.Predicate"));
		return validate(className, new MethodSignature(
				new PredicateFormula("auto_event", Arrays.asList((ITerm) V, (ITerm) V2), null, null, null), -1));
	}

	public boolean hasField(String className, String property, String type, boolean isStatic) {
		Class<?> cls = resolveClass(className);
		for (Field field : cls.getFields()) {
			if (field.getName().equals(property) && matchType(field.getType(), new MethodType(type)) && 
					java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
				return true;
			}
		}
		return false;
	}

	public boolean getEventSymbols(String className, MethodSignature signature, String symbol) {
		Class<?> cls = resolveClass(className);

		while (cls.getSuperclass() != null) {
			for (Method mthd : cls.getMethods()) {
				if (mthd.getName().equals(signature.name())) {
					if (signature.termCount() != (mthd.getParameterTypes().length - (signature.symbol() ? 1 : 0)))
						continue;

					for (Annotation ann : mthd.getAnnotations()) {
						if (ann.annotationType().getCanonicalName().equals(annotations.get(signature.type()))) {
							String[] params = ((astra.core.Module.EVENT) ann).symbols();
							for (int i = 0; i < params.length; i++) {
								if (params[i].equals(symbol))
									return true;
							}
						}
					}
				}
			}
			cls = cls.getSuperclass();
		}
		return false;
	}

	public boolean suppressAutoActionNotifications(String className) {
		Method mthd = getMatchingMethod(className, getAutoMethodSignature());
		for (Annotation ann : mthd.getAnnotations()) {
			if (ann.annotationType().getCanonicalName().equals("astra.core.InternalAffordances.SUPPRESS_NOTIFICATIONS"))
				return true;
		}
		return false;
	}

	public void createTarget(ASTRAClassElement element, String code) {
		try {
			File file = getTargetFileReference(element.getFilename());
			
			// Create the folder structure (if it does not already exist).
			if (!file.getParentFile().exists()) file.getParentFile().mkdirs();
			
			// Create the file
			file.createNewFile();
			
			// Write the generated code...
			FileWriter out = new FileWriter(file);
			out.write(code);
			out.close();
			// System.out.println("[ASTRACompiler] Generated Target File: " + element.getFilename() + " / " + file.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
