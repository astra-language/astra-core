package astra.ast.element;


import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.IEvent;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.definition.FormulaDefinition;

public class LearningElement extends AbstractElement {
    
  	String namespace;
	AlgorithmElement algorithm;
    RuleElement rule;
    IEvent event;
    FormulaDefinition beliefFormula;

    public LearningElement(String namespace, AlgorithmElement algorithm, RuleElement rule, 
                            FormulaDefinition beliefFormula, Token start, Token end, String source) {
		super(start, end, source);
		this.namespace = namespace;
        this.algorithm = algorithm;
        this.rule = rule;
        this.event = rule.event;
		this.beliefFormula = beliefFormula;
		
	}

    public String getNamespace() {
        return namespace;
    }

    public AlgorithmElement getAlgorithmElement() {
        return algorithm;
    }

    public RuleElement getRule() {
        return rule;
    }

    public IEvent getEvent() {
        return event;
    }

    public FormulaDefinition getBeliefFormulae() {
        return beliefFormula;
    }

    @Override
	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

    public String toString() {
		return "learn " + namespace + " { " + algorithm.toString() + " \n " + rule.event().toString() + "; \n formula  " + beliefFormula.toString() + "; } ";
	}
}
