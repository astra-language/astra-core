package astra.ast.element;

import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.ParseException;
import astra.ast.core.Token;

import java.util.List;

public class AlgorithmElement extends AbstractElement {

    String algorithm;
    List<InitialElement> parameters;

    public AlgorithmElement(String algorithm, List<InitialElement> parameters, Token start, Token end, String source) {
		super(start, end, source);

        this.algorithm = algorithm;
        this.parameters = parameters;
	}

    public String getAlgorithm() {
        return algorithm;
    }

    public List<InitialElement> getParameters() {
        return parameters;
    }

    @Override
	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}
    
}
