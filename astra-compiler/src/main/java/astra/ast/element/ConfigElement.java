package astra.ast.element;

import java.util.List;

import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.ITerm;
import astra.ast.core.ParseException;
import astra.ast.core.Token;

public class ConfigElement extends AbstractElement {

	String name;
	List<ITerm> terms;
	
	public ConfigElement(String name, List<ITerm> terms, Token start, Token end, String source) {
		super(start, end, source);
		this.name = name;
		this.terms = terms;
	}

	@Override
	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public String name() {
		return name;
	}

	public List<ITerm> terms() {
		return terms;
	}

	
	public String toString() {
		return name + "( " + terms.toString() +  " ) ";
	}
}
