package astra.ast.element;

import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.ITerm;
import astra.ast.core.IType;
import astra.ast.core.ParseException;
import astra.ast.core.Token;

public class ConstantElement extends AbstractElement {
	String constant;
	IType type;
	ITerm term;
	
	public ConstantElement(String constant, IType type, ITerm term, Token start, Token end, String source) {
		super(start, end, source);
		this.constant = constant;
		this.type = type;
		this.term = term;
	}

	@Override
	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public String constant() {
		return constant;
	}

	public IType type() {
		return type;
	}
	
	public ITerm term() {
		return term;
	}

	public String toString() {
		return constant + " = " + term;
	}		
}
