package astra.ast.visitor;

import java.util.ArrayList;
import java.util.List;

import astra.ast.core.ASTRAClassElement;
import astra.ast.core.IFormula;
import astra.ast.core.IJavaHelper;
import astra.ast.core.ILanguageDefinition;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.definition.FormulaDefinition;
import astra.ast.definition.TypeDefinition;
import astra.ast.element.AlgorithmElement;
import astra.ast.element.InitialElement;
import astra.ast.element.LearningElement;
import astra.ast.element.TypesElement;
import astra.ast.formula.PredicateFormula;
import astra.ast.term.Literal;


public class LearningProcessVisitor extends AbstractVisitor {

	protected IJavaHelper helper;
	
	public LearningProcessVisitor(IJavaHelper helper) {
		this.helper = helper;
	}

	//FIXME
	private static String knowledge_input = "knowledge_input";
	private static String knowledge_action = "knowledge_action";
	private static String knowledge_label = "knowledge_label";
	private static String knowledge_reward = "knowledge_reward";
	private static String knowledge_train = "knowledge_train";
	private static String[] INPUT_BELIEFS = {knowledge_input, knowledge_action, knowledge_label, knowledge_reward, knowledge_train};
	private static String CONFIGURATION_BELIEFS = "configuration";
	
	private List<ILanguageDefinition> createDefinitions(String learningElementSource, Token start, Token end) {
		List<ILanguageDefinition> definitions = new ArrayList<>();
		TypeDefinition td = new TypeDefinition("string", Token.STRING_TYPE);
		List<TypeDefinition> types = new ArrayList<>();
		types.add(td);
		types.add(td);
		for (String pred: INPUT_BELIEFS) {
			definitions.add(new FormulaDefinition(pred, types.toArray(new TypeDefinition[types.size()]), start, end, learningElementSource));
		}
		
		types.add(new TypeDefinition("double", Token.DOUBLE_TYPE));
		//Congiguration with string, string, double
		definitions.add(new FormulaDefinition(CONFIGURATION_BELIEFS, types.toArray(new TypeDefinition[types.size()]), start, end, learningElementSource));
		//Configuration with string, string, boolean
		td = new TypeDefinition("string", Token.STRING_TYPE);
		types = new ArrayList<>();
		types.add(td);
		types.add(td);
		types.add(new TypeDefinition("boolean", Token.BOOLEAN));
		definitions.add(new FormulaDefinition(CONFIGURATION_BELIEFS, types.toArray(new TypeDefinition[types.size()]), start, end, learningElementSource));
		
		types = new ArrayList<>();
		types.add(td);
		types.add(td);
		types.add(new TypeDefinition("int", Token.INTEGER));
		definitions.add(new FormulaDefinition(CONFIGURATION_BELIEFS, types.toArray(new TypeDefinition[types.size()]), start, end, learningElementSource));
		
		return definitions;
	}
	
	public Object visit(ASTRAClassElement element, Object data) throws ParseException {
		for (LearningElement le : element.getLearningElements()) {
			le.accept(this, data);
		}
		
		return null;
	}

	public Object visit(LearningElement learningElement, Object data) throws ParseException {
		//helper.setup(element.packageElement(), element.imports());
		ComponentStore store = (ComponentStore) data;
		
		//First, check the learning algorithm exists
		AlgorithmElement algorithmElement = learningElement.getAlgorithmElement();
		String algorithmName = algorithmElement.getAlgorithm();
		try {
			Class.forName("astra.learn.library." + algorithmName);
		} catch (ClassNotFoundException ex) {
			//One more go
			try {
				helper.resolveModule(algorithmName);
			} catch(Exception e) {
				throw new ParseException("Could not load class for the algorithm " + algorithmName + " for learning process " + learningElement.getNamespace(), 
				algorithmElement.start, algorithmElement.end);
			}
		}
		
		//Initial check: for any input beliefs elements, are there matching formulas
		//FIXME: this will break if Formula signatures change - also would need to change LearningProtectectedPredicates in interpreter
		AlgorithmElement ae = learningElement.getAlgorithmElement();
		for (InitialElement ie : ae.getParameters()) {
			IFormula f = ie.formula();
			if (!(f instanceof PredicateFormula)) {
				throw new ParseException("Algorithm parameter is wrong type, expected a belief but got : " + f.toString(),
				ae.start, ae.end);
			} else {
				PredicateFormula p = (PredicateFormula)f;
				if (isInputBelief(p)) {
					//Get the second term
					Literal t = (Literal)p.terms().get(1);
					String v = t.value().replaceAll("\"", "");
					boolean matchingPredicate = false;
					for (String s: store.signatures) {
						//Get name of predicate
						int index = s.indexOf(":") + 1;
						int index2 = s.indexOf(":", index);
						if (index2 == -1) {
							continue;
						}
						String term = s.substring(index, index2);
						if (term.equals(v)) {
							matchingPredicate = true;
						}
					}
					if (!matchingPredicate) 
						throw new ParseException("Undefined input belief to algorithm, no formula with the predicate: " + t.value() + " for input belief: " + p.predicate(), 
						ae.start, ae.end);
				}
				
			}
			
		}
		//FIXME: add another check - knowledge_action has to match knowledge_label. 
		//Possibly also check that knowledge_input and knowledge_label match is SL?

		// Add the belief set namespace language definition (as for explicitly declared formulae in the types section)
		List<TypesElement> ontologies = new ArrayList<TypesElement>();
		FormulaDefinition fd = learningElement.getBeliefFormulae();
		List<ILanguageDefinition> definitions = new ArrayList<>();
		definitions.add(fd);

		List<ILanguageDefinition> protectedDefinitions = createDefinitions(learningElement.getSource(), learningElement.start, learningElement.end);
		//FIXME: there will be a nicer way to do this...
		// if there is another learning process, the protected definitions will have been added already
		if (!store.signatures.contains(protectedDefinitions.get(0).toSignature())) {
			definitions.addAll(protectedDefinitions);
		}
		
		TypesElement ontology = new TypesElement(learningElement.getNamespace() + "_ont", definitions.toArray(new ILanguageDefinition[definitions.size()]), 
								learningElement.start, learningElement.end, learningElement.getSource());
		ontologies.add(ontology);
		
		if (store.types.contains(ontology.name())) 
			throw new ParseException("Duplicate Ontology: " + ontology.name(), 
					ontology.start, ontology.end);

		for (ILanguageDefinition definition : ontology.definitions()) {
			if (store.signatures.contains(definition.toSignature())) {
				throw new ParseException("Conflict in ontology: " + ontology.name() + " for term: " + definition, 
						ontology.start, ontology.end);
			}
			store.signatures.add(definition.toSignature());
		}

		// Ontology was loaded without conflict so we are okay...
		store.types.add(ontology.name());
		return null;
	}

	private boolean isInputBelief(PredicateFormula predicateFormula) {
		for (String s: INPUT_BELIEFS) {
			if (predicateFormula.predicate().equals(s)) {
				return true;
			}
		}
		return false;
	}
}
