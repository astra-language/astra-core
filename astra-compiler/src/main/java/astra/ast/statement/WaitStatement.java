package astra.ast.statement;

import astra.ast.core.*;

public class WaitStatement extends AbstractElement implements IStatement {
	private IFormula guard;
	private ITerm timeout;

	public WaitStatement(IFormula guard, Token start, Token end, String source) {
		this(guard, null, start, end, source);
	}

	public WaitStatement(IFormula guard, ITerm timeout, Token start, Token end, String source) {
		super(start, end, source);
		this.guard = guard;
		this.timeout = timeout;
	}

	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public IFormula guard() {
		return guard;
	}

	public ITerm timeout() {
		return timeout;
	}
}
