package astra.ast.statement;

import astra.ast.core.*;

public class DoneStatement extends AbstractElement implements IStatement {
	public DoneStatement(Token first, Token last, String source) {
		super(first, last, source);
	}

	@Override
	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}
	public String toString() {
		return "done()";
	}
}
