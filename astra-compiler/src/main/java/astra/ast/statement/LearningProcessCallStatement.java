package astra.ast.statement;

import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.IStatement;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.formula.PredicateFormula;

public class LearningProcessCallStatement extends AbstractElement implements IStatement {

    PredicateFormula actionPredicate;
	String learningProcessNamespace;
	
	public LearningProcessCallStatement(String learningProcessNamespace, PredicateFormula actionPredicate, Token start, Token end, String source) {
		super(start, end, source);
		this.learningProcessNamespace = learningProcessNamespace;
		this.actionPredicate = actionPredicate;
	}

	public String learningProcessNamespace() {
		return learningProcessNamespace;
	}

	public PredicateFormula actionPredicate() {
		return actionPredicate;
	}

	@Override
	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}
	
	public String toString() {
		return learningProcessNamespace + "#" + actionPredicate;
	}
    
}
