package astra.ast.statement;

import astra.ast.core.*;

public class FailStatement extends AbstractElement implements IStatement {
	public FailStatement(Token first, Token last, String source) {
		super(first, last, source);
	}

	@Override
	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}
	public String toString() {
		return "done()";
	}
}
