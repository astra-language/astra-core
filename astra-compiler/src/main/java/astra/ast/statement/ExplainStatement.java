package astra.ast.statement;

import astra.ast.core.AbstractElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.IStatement;
import astra.ast.core.ITerm;
import astra.ast.core.ParseException;
import astra.ast.core.Token;


/** 
 * Support for following language level changes:
 * 
 * explain( ID, tag, value );
 * 
 * explain( tag, value );
 * 
 */
public class ExplainStatement extends AbstractElement implements IStatement {

	ITerm tag;
	ITerm ID;
	ITerm value;

	public ExplainStatement(ITerm ID, ITerm tag, ITerm value, Token start, Token end, String source) {
		super(start, end, source);
		this.ID = ID;
		this.tag = tag;
		this.value = value;
	}

	public ExplainStatement(ITerm tag, ITerm value, Token start, Token end, String source) {
		super(start, end, source);
		this.tag = tag;
		this.value = value;
	}

	@Override
	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public ITerm tag() {
		return tag;
	}

	public ITerm value() {
		return value;
	}

	public ITerm ID() {
		return ID;
	}

	public String toString() {
		String retval = "explain( ";
		if (ID != null) {
			retval += ID + ", ";
		}
		retval += tag + ", " + value + " )";
		
		return retval;
	}
}
