import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import astra.ast.core.ADTTokenizer;
import astra.ast.core.ASTRAParser;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.element.AlgorithmElement;
import astra.ast.element.LearningElement;

public class TestLearningElement {
	ASTRAParser parser;
	ADTTokenizer tokenizer;

	public List<Token> setup(String input) throws ParseException {
		tokenizer = new ADTTokenizer(new ByteArrayInputStream(input.getBytes()));
		parser = new ASTRAParser(tokenizer);
		List<Token> list = new ArrayList<Token>();
		Token token = tokenizer.nextToken();
		while (token != Token.EOF_TOKEN) {
			list.add(token);
			token = tokenizer.nextToken();
		}

		return list;
	}

	@Test
	public void testParseAlgorithm() throws ParseException {
		List<Token> tokens = setup("algorithm QLearning{+epsilon(0.1);+alpha(0.8);}");
		assertEquals(Token.ALGORITHM, tokens.get(0).type);
		AlgorithmElement algorithmElement = parser.createAlgorithm("test", tokens);
		System.out.println(algorithmElement.toString());
		assertEquals(AlgorithmElement.class, algorithmElement.getClass());
	}

	@Test
	public void testParseLearn() throws ParseException {
		StringBuffer sb = new StringBuffer();
		sb.append(" qlearningNamespace{");
		sb.append("\n algorithm QLearning{ ");
		sb.append("\n			+epsilon(0.1);");
		sb.append("\n }");
		sb.append("\n rule +!test(){}");
		sb.append("}");

		List<Token> tokens = setup(sb.toString());
		//CreateLearningElement expects the Learn token to have already been removed
		LearningElement learningElement = parser.createLearningElement(tokens);
		System.out.println(learningElement.toString());
		assertEquals(LearningElement.class, learningElement.getClass());
	}

	@Test
	public void testParseLearnNoRule() throws ParseException {
		StringBuffer sb = new StringBuffer();
		sb.append(" nothing{");
		sb.append("\n algorithm QLearning{ ");
		sb.append("\n			+epsilon(0.1);");
		sb.append("\n }");
		sb.append("\n} ");

		List<Token> tokens = setup(sb.toString());
		assertThrows(ParseException.class, () -> parser.createLearningElement(tokens));
	}


}
