import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import astra.ast.core.ADTTokenizer;
import astra.ast.core.ASTRAParser;
import astra.ast.core.ITerm;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.term.CountFormulaeTerm;
import astra.ast.term.Function;
import astra.ast.term.Literal;
import astra.ast.term.ModuleTerm;

public class TestBasicTerms {
	ASTRAParser parser;
	ADTTokenizer tokenizer;

	public List<Token> setup(String input) throws ParseException {
		tokenizer = new ADTTokenizer(new ByteArrayInputStream(input.getBytes()));
		parser = new ASTRAParser(tokenizer);
		List<Token> list = new ArrayList<Token>();
		Token token = tokenizer.nextToken();
		while (token != Token.EOF_TOKEN) {
			list.add(token);
			token = tokenizer.nextToken();
		}

		return list;
	}

	@Test
	public void stringTest() throws ParseException {
		List<Token> tokens = setup("\"name\"");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test
	public void missingQuoteStringTest() throws ParseException {
    	assertThrows(
           ParseException.class,
           () -> setup("\"name"),
           "Expected setup() to throw, but it didn't"
	    );		
	}

	@Test
	public void characterTest() throws ParseException {
		List<Token> tokens = setup("'c'");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test
	public void missingQuoteCharacterTest() throws ParseException {
    	assertThrows(
           ParseException.class,
           () -> setup("'c"),
           "Expected setup() to throw, but it didn't"
	    );		
	}

	@Test
	public void intTest() throws ParseException {
		List<Token> tokens = setup("12");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test
	public void longTest() throws ParseException {
		List<Token> tokens = setup("12l");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test
	public void floatTest() throws ParseException {
		List<Token> tokens = setup("12.0f");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test
	public void doubleTest() throws ParseException {
		List<Token> tokens = setup("12.0");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test
	public void functionTest() throws ParseException {
		List<Token> tokens = setup("fatherOf(\"rem\")");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Function.class, term.getClass());
	}

	@Test
	public void failedFunctionTest() throws ParseException {
		List<Token> tokens = setup("fatherOf(\"rem\"");
		assertThrows(
           ParseException.class,
           () -> parser.createTerm(tokens),
           "Expected createTerm() to throw, but it didn't"
	    );		
		// assertEquals(Function.class, term.getClass());
	}

	@Test
	public void moduleTermTest() throws ParseException {
		List<Token> tokens = setup("mod.fatherOf(\"rem\")");
		ITerm term = parser.createTerm(tokens);
		assertEquals(ModuleTerm.class, term.getClass());
	}
	
	@Test
	public void countFormulaeTest() throws ParseException {
		List<Token> tokens = setup("count_formulae(init(happy))");
		ITerm term = parser.createTerm(tokens);
		assertEquals(CountFormulaeTerm.class, term.getClass());
	}

	// Cannot run this test because 'init' could now be a variable - this will be caught later
	// when variables are checked.
	// @Test(expected = ParseException.class)
	// public void invalidCountFormulaeTest() throws ParseException {
	// 	List<Token> tokens = setup("count_formulae(init)");
	// 	ITerm term = parser.createTerm(tokens);
	// 	assertEquals(CountFormulaeTerm.class, term.getClass());
	// }

}
