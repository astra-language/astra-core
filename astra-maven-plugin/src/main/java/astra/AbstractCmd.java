package astra;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

public abstract class AbstractCmd {
	private BufferedReader inputReader;
	private BufferedReader errorReader;
	protected File baseDir;
	
	public AbstractCmd(File baseDir) {
		this.baseDir = baseDir;
	}
	
	public void execute() throws IOException, InterruptedException {
		Map<String, String> allEnv = System.getenv();
		ArrayList<String> envp = new ArrayList<>();
		for(String key:allEnv.keySet()) {
			envp.add(key + "=" + allEnv.get(key));
		}
		Process process = Runtime.getRuntime().exec(getCommand(), envp.toArray(new String[0]), baseDir);
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				process.destroy();
			}
		}));		
		
		inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		displayStream(inputReader);
		displayStream(errorReader);
		if (process.waitFor() > 0) {
			Thread.sleep(1000);
			System.exit(1);
		}
	}
	
	public BufferedReader getInputReader() {
		return inputReader;
	}
	
	public BufferedReader getErrorReader() {
		return errorReader;
	}
	
	public String[] getCommand() {
		return new String[] {"skip"};
	}
	
	private void displayStream(final BufferedReader inputReader) {
		new Thread() {
			public void run() {
				try {
					String line = null;
					while ((line = inputReader.readLine()) != null) {
						System.out.println(line);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
}
