# astra-archetype

This is a template for a basic ASTRA project. It creates a correctly tooled
maven project with a single ASTRA file that displayed "Hello World, ASTRA!"

To use this archetype, you must first add the ASTRA repository to your
maven settings.xml file:

	<repository>
		<id>astra-repo</id>
		<name>ASTRA Maven Repository</name>
		<url>https://gitlab.com/astra-language/astra-mvn-repo/raw/master</url>
	</repository>

Following this, you can create an ASTRA project by entering the following
command:

    mvn archetype:generate 
        -DarchetypeGroupId=astra 
        -DarchetypeArtifactId=astra-archetype 
        -DarchetypeVersion=0.1.0 
        -DgroupId=test 
        -DartifactId=tester 
        -Dversion=0.1.0

Executing the above command creates a basic ASTRA project with a sample Hello
World agent.

The agent file is called Main.astra and it is located in the src/main/astra folder.