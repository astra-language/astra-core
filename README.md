ASTRA: AgentSpeak(TeleoReactive) Agent Programming Language
==========

This repository contains the source code for ASTRA, an Agent-Oriented Programming (AOP) language that is based on the combination of AgentSpeak(L) and Teleo-Reactive Programming.

The repository contains a number of subprojects that implement various aspects of ASTRA and its associated tooling.

ASTRA comes with built in support for both the [Environment Interface Standard](https://github.com/eishub) and [CArtAgO](http://cartago.sourceforge.net/).

For information about our architectural style for deploying ASTRA within a microservices architecture, please visit [here](https://gitlab.com/mams-ucd/mams-cartago).

Release Notes
------

Release notes for the different verions of ASTRA are now available [here](RELEASE_NOTES.md).

Getting Started
------

To learn more about ASTRA, please goto the [ASTRA Programming Language Guide](http://guide.astralanguage.com/en/latest/).  You can also explore some sample projects [here](https://gitlab.com/astra-language/examples).

Installation Instructions
------

ASTRA is designed to be deployed using the [Apache Maven](https://maven.apache.org/) build system. All versions of ASTRA are released through [Maven Central](https://search.maven.org/). To see the latest release information, please visit [here](https://mvnrepository.com/artifact/com.astralanguage).

About
------

ASTRA is an ongoing research project that is maintained by [Associate Prof. Rem Collier](https://people.ucd.ie/rem.collier), an academic in the [School of Computer Science] (https://www.cs.ucd.ie), [University College Dublin](http://www.ucd.ie), Ireland.

It is used within a number of larger research projects:

* [Crop OptimisatioN though Sensing, Understanding and viSualisation (CONSUS)](https://www.consus.ie)
* [Creating an Architecture for Manipulating Earth Observation data (CAMEO)](https://www.ucd.ie/innovation/news-and-events/latest-news/ucd-dtif-round-three/name,550189,en.html)

