package astra.reasoner;

import java.util.Map;

import astra.cartago.CartagoProperty;
import astra.reasoner.node.ReasonerNode;
import astra.reasoner.node.ReasonerNodeFactory;
import astra.term.Term;

public class CartagoPropertyNodeFactory implements ReasonerNodeFactory<CartagoProperty> {

    @Override
    public ReasonerNode create(CartagoProperty formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new CartagoPropertyNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, CartagoProperty formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new CartagoPropertyNode(parent, formula, bindings, singleResult);
    }
    
}
