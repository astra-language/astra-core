package astra.reasoner;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import astra.cartago.CartagoProperty;
import astra.formula.Formula;
import astra.reasoner.node.ReasonerNode;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Term;

public class CartagoPropertyNode extends ReasonerNode {
    CartagoProperty property;
	Queue<Formula> options = new LinkedList<>();
    int state = 0;

    public CartagoPropertyNode(ReasonerNode parent, CartagoProperty property, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.property = property;
        this.initial = initial;
    }

    @Override
    public ReasonerNode initialize(Reasoner reasoner) {
        visitor = new BindingsEvaluateVisitor(initial, reasoner.agent());
        property = (CartagoProperty) property.accept(visitor);
        for (Queryable source : reasoner.sources()) {
            source.addMatchingFormulae(options, property);
        }
        return super.initialize(reasoner);
    }

    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        while (!options.isEmpty()) {
            Formula formula = options.poll();
            Map<Integer, Term> bindings = Unifier.unify(property, (CartagoProperty) formula.accept(visitor), new HashMap<Integer, Term>(initial), reasoner.agent());
            if (bindings != null) {
                solutions.add(bindings);
                if (singleResult) {
                    finished = true;
                }
                return true;
            }
        }

        finished = true;
        return !(failed = solutions.isEmpty());
    }

    
}
