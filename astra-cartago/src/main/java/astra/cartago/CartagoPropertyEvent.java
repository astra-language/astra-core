package astra.cartago;

import astra.event.Event;
import astra.reasoner.util.LogicVisitor;
import astra.term.Primitive;
import astra.term.Term;

public class CartagoPropertyEvent implements Event {
	public static final Primitive<String> ADDED = Primitive.newPrimitive("+");
	public static final Primitive<String> REMOVED = Primitive.newPrimitive("-");

	private Term type;
	private Term id;
	private Term formula;

	public CartagoPropertyEvent(Term type, Term id, Term formula) {
		this.type = type;
		this.id = id;
		this.formula = formula;
	}

	public String signature() {
		return "$cpe";
	}

	public String toString() {
		return type + "$cartago.event(" + id + "," + formula + ")";
	}

	public Term type() {
		return type;
	}

	public Term id() {
		return id;
	}

	public Term content() {
		return formula;
	}

	@Override
	public Object getSource() {
		return null;
	}

	@Override
	public Event accept(LogicVisitor visitor) {
		return new CartagoPropertyEvent((Term) type.accept(visitor), (Term) id.accept(visitor), (Term) formula.accept(visitor));
	}
}
