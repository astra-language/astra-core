package astra.cartago;

import astra.event.Event;
import astra.reasoner.util.LogicVisitor;
import astra.term.Term;

public class CartagoSignalEvent implements Event {
	private Term id;
	private Term content;

	public CartagoSignalEvent(Term id, Term content) {
		this.id = id;
		this.content = content;
	}

	public String signature() {
		return "$cse";
	}

	public String toString() {
		return "$cartago.signal(" + id + "," + content + ")";
	}

	public Term id() {
		return id;
	}

	public Term content() {
		return content;
	}

	@Override
	public Object getSource() {
		return null;
	}

	@Override
	public Event accept(LogicVisitor visitor) {
		return new CartagoSignalEvent((Term) id.accept(visitor), (Term) content.accept(visitor));
	}
}
