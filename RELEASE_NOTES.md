# RELEASE NOTES

Please find release notes for each new version of ASTRA, starting with version 2.0.1.

You can go back to the main page by clicking [here](.).

## 2.0.1
This release includes the following bug fixes:

* Issue 68: Update to latest version of EIS
* Issue 91: Constants do not work in initial beliefs...

This release also includes the following enhancements:

* Issue 92: Integration of astra-docker library into main codebase

## 2.0.1
This release includes the following bug fixes:

* Issue 79: Issue with unbound variable in Cartago operation
* Issue 80: Splitting of EIS support into eis-0.5 and eis-0.7 as the two versions are incompatible
* Issue 82: Test goal should check the state first and then generate the event
* Issue 83: Removal of read-only parameter warnings when running ASTRA code
* Issue 84: Possible solution to kill the running ASTRA process when the Maven process is killed.

This release also includes the following enhancements:

* Issue 85: Introduction of a "fail" statement as a first class entity
* Issue 86: Added method to get the agent object via the System API (necessary for MAMS FIPA Messaging Integration)

## 1.4.3
This release includes the following bug fixes

* Issue 71: Updating of maven package versions across ASTRA
* Issue 74: Add a test goal (?) to ASTRA
* Issue 78: astra-eis-base does not copy dependencies by default

This release also includes the following enhancements:

* Issue 72: Creation of an astra-cartago-archtype
* Issue 73: Add a "contains" methods for the Strings library
* Issue 76: Implementation of atomic keyword for rules
* Issue 77: Vickrey Auction in astra-protocols is actually a First Price Sealed Bid Auction

## 1.4.2
This release includes the following bug fixes

* Issue 70: Archtype issue fixed

Special Updates:

* BSPL support removed from codebase and transfered to the [ASTRA Tools Repository](../../astra-tools/astra-bspl)

## 1.4.1
This release includes the following bug fixes

* Issue 65: Support for boolean terms being interpreted as logical formulae.

## 1.4.0
This release includes the following enhancements:

* Issue 8: A warning is reported if an agent type without a +!main(...) rule is used as the main agent type.
* Issue 46: Creation of a failed goal event
* Issue 50: Introduction of Constants
* Issue 61/62: Introduction of a NOT (~) operator for boolean values in term expressions

This release includes the following bug fixes

* Issue 49: Matching of object types in @EVENT methods needs to be improved
* Issue 51: forall(...) does not work with @TERM methods that return a ListTerm

## 1.3.7
This release includes the following bug fixes:

* Subgoals that have no associated rules no longer fail silently
* CArtAgO codebase has been copied into astra-language and released as a Maven artifact on Gitlab to remove dependency on the CArtAgO artifact remotely hosted with JaCaMo (which was recently deleted).

## 1.3.6
This release include the following bug fixes:

* Unbound variables created before an action can now be passed directly to the action (previously an inline variable declaration + an assignment had to be used)
* Brackets can now be used in logical formulae (an exception occurred previously)
* The `Prelude` module had a number of `addAt(...)` methods that were meant to be actions, but were not annotated correctly.

The following enhancements have been made:

* The `Prelude` module now has a `shuffle` action that will shuffle a list
* The `System` module now has a `env(...)` term that provide access to environment variables.